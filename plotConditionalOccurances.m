function plotConditionalOccurances(condition_for_prob, delayTime_for_prob)
    
    ll = [0,150,300];
    ul = [150,300,450];
    for tB = 1:length(ll)
        for cnd = 1:length(unique(condition_for_prob))
        
            
            id_tb = find(delayTime_for_prob > ll(tB) & delayTime_for_prob <= ul(tB));
            c_tb = condition_for_prob(id_tb);
            
            c_cnd = find(c_tb == cnd-1);
            figure(1)
            subplot(1,3,tB)
            [no,xo] = hist(c_tb(c_cnd), [0:1:3]);
            bar([0:1:3],no/length(id_tb))
            xticklabels({'B-Left','B-Right','S-Left','S-Right'})
            hold on
            
            if tB == 1
                title(sprintf('0-150ms(N=%d)', length(id_tb)))
            elseif tB == 2
                title(sprintf('150-300ms (N=%d)', length(id_tb)))
            elseif tB == 3
                title(sprintf('300-450ms (N=%d)', length(id_tb)))
            end
            
        end
    end
    
    
end