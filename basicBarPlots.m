close all

directory = 'C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\priorityMaps2.0\scriptsAndData\data\SK\';
folder = '01182021\';

fullpath = strcat(directory,folder);

S = dir(fullpath);
numSessions = sum([S(~ismember({S.name},{'.','..'})).isdir]);

for eachSess = 4%1:numSessions
    
    [changeLocs,delayTimeChoices,....
    orUp, orDown, orLeft, orRight,....
    pL_up, pL_down, pL_left, pL_right,...
    responses,...
    nhits_top, nhits_bottom, nhits_left, n_hits_right,...
    idtrials_top, idtrials_bottom, idtrials_left, idtrials_right] = deal([]);
    
    sessFolder = strcat(fullpath,string(eachSess),'\');
    
    load([strcat(sessFolder,'pptrials.mat')])
    dataHolder = pptrials.user;
%     dataHolder = pptrials;
    
    for all = 1:length(dataHolder)
        changeLocs(1,end+1) = dataHolder{all}.changeGaborLoc;
        delayTimeChoices(1,end+1) = round(dataHolder{all}.TimeDelayOff - dataHolder{all}.TimeDelayOn);
        responses(1,end+1) = dataHolder{all}.Response;
    
        if (dataHolder{all}.changeGaborLoc == 5)
            idtrials_top(1,end+1) = all;
            pL_up(1,end+1) = dataHolder{all}.pestLevel_top;
            orUp(1,end+1) = dataHolder{all}.orUp;
        elseif (dataHolder{all}.changeGaborLoc == 6)
            idtrials_bottom(1,end+1) = all;
            pL_down(1,end+1) = dataHolder{all}.pestLevel_bottom;
            orDown(1,end+1) = dataHolder{all}.orDown;
        elseif (dataHolder{all}.changeGaborLoc == 7)
            idtrials_left(1,end+1) = all;
            pL_left(1,end+1) = dataHolder{all}.pestLevel_left;
            orLeft(1,end+1) = dataHolder{all}.orLeft;
        elseif (dataHolder{all}.changeGaborLoc == 8)
            idtrials_right(1,end+1) = all;
            pL_right(1,end+1) = dataHolder{all}.pestLevel_right;
            orRight(1,end+1) = dataHolder{all}.orRight;
        end
    end
%     [pL_Left_new,resp_new_left] = deal([]);
%     all_left_resp = responses(idtrials_left(67:95));%responses(idtrials_left);
%     all_pest_left = pL_left(67:95);%pL_left;
%     for all = 1:responses(idtrials_left(67:95))
%         resp_new_left(1,end+1) = all_left_resp(all);
%         pL_Left_new(1,end+1) = all_pest_left(all);
%     end
%     [thresh_left ~, ~] = psyfit(pL_left, responses(idtrials_left),'Boots',1)
% %     figure();
%     [thresh_right ~, ~] = psyfit(pL_right, responses(idtrials_right),'Boots',1)
    figure()
%     plot([1:1:length(pL_up)],pL_up)
%     hold on
%     plot(find(responses(idtrials_top) == 1),pL_up(responses(1,idtrials_top) == 1),'go')
%     hold on
%     plot(find(responses(idtrials_top) == 0),pL_up(responses(1,idtrials_top) == 0),'ro')
%     hold on
% %     title('pest level top gabor')
% %     figure()
%     plot([1:1:length(pL_down)],pL_down)
%     hold on
%     plot(find(responses(idtrials_bottom) == 1),pL_down(responses(1,idtrials_bottom) == 1),'go')
%     hold on
%     plot(find(responses(idtrials_bottom) == 0),pL_down(responses(1,idtrials_bottom) == 0),'ro')
%     hold on
%     title('pest level bottom gabor')
%     figure()
    subplot(1,2,1)
    plot([1:1:length(pL_left)],pL_left)
    hold on
    plot(find(responses(idtrials_left) == 1),pL_left(responses(1,idtrials_left) == 1),'go')
    hold on
    plot(find(responses(idtrials_left) == 0),pL_left(responses(1,idtrials_left) == 0),'ro')
    hold on
    xlabel('No. of trials')
    ylabel('Pest Level left gabor')
    ylim([8 45]);
    ax = gca;
    ax.FontSize = 25;

%     figure()
    subplot(1,2,2)
    plot([1:1:length(pL_right)],pL_right)
    hold on
    plot(find(responses(idtrials_right) == 1),pL_right(responses(1,idtrials_right) == 1),'go')
    hold on
    plot(find(responses(idtrials_right) == 0),pL_right(responses(1,idtrials_right) == 0),'ro')
    xlabel('No. of trials')
    ylabel('Pest Level right gabor')
    ylim([8 45]);
    ax = gca;
    ax.FontSize = 25;

    a = axes;
    dataHolder{1}.bgContrast = 0.2;
    dataHolder{1}.gaborSpatialFrequency = 10;
    t1 = title(strcat('session: ',string( eachSess),';', ' sp freq: ', string(dataHolder{1}.gaborSpatialFrequency),';',...
                       ' bg contrast: ', string(  dataHolder{1}.bgContrast),';', ' gabor contrast: ', string( dataHolder{1}.contrast)));
    a.Visible = 'off'; % set(a,'Visible','off');
    t1.Visible = 'on'; %
   
%     figure();
    

end









%[thresh, par, threshB, xi, yi, chiSq] = psyfit(orUp(idtrials_top), responses(idtrials_top));
% [thresh_up ~, ~] = psyfit(pL_up, responses(idtrials_top),'Boots',1)
% [thresh_down ~, ~] = psyfit(pL_down, responses(idtrials_bottom),'Boots',1)
[thresh_left ~, ~] = psyfit(pL_left, responses(idtrials_left),'Boots',1)
%% set the threshold to 62 and chance to 25 when 4 gabors are used%
[thresh_right ~, ~] = psyfit(pL_right, responses(idtrials_right),'Boots',1)


%PEST PLOTS
figure()
%EVERYTIME RESPONSE IS CORRECT PUT AN ASTERISK OR CIRCLE....
plot([1:1:length(pL_up)],pL_up)
hold on
plot(find(responses(idtrials_top) == 1),pL_up(responses(1,idtrials_top) == 1),'go')
hold on
plot(find(responses(idtrials_top) == 0),pL_up(responses(1,idtrials_top) == 0),'ro')
title('pest level top gabor')
figure()
plot([1:1:length(pL_down)],pL_down)
hold on
plot(find(responses(idtrials_bottom) == 1),pL_down(responses(1,idtrials_bottom) == 1),'go')
hold on
plot(find(responses(idtrials_bottom) == 0),pL_down(responses(1,idtrials_bottom) == 0),'ro')
title('pest level bottom gabor')
figure()
plot([1:1:length(pL_left)],pL_left)
hold on
plot(find(responses(idtrials_left) == 1),pL_left(responses(1,idtrials_left) == 1),'go')
hold on
plot(find(responses(idtrials_left) == 0),pL_left(responses(1,idtrials_left) == 0),'ro')
title('pest level left gabor')
figure()
plot([1:1:length(pL_right)],pL_right)
hold on
plot(find(responses(idtrials_right) == 1),pL_right(responses(1,idtrials_right) == 1),'go')
hold on
plot(find(responses(idtrials_right) == 0),pL_right(responses(1,idtrials_right) == 0),'ro')
title('pest level right gabor')



propCorrect_top = length(find(responses(idtrials_top) == 1))/length(idtrials_top);
propCorrect_bottom = length(find(responses(idtrials_bottom) == 1))/length(idtrials_bottom);
propCorrect_left = length(find(responses(idtrials_left) == 1))/length(idtrials_left);
propCorrect_right = length(find(responses(idtrials_right) == 1))/length(idtrials_right);





%prob of change
figure()
[no,xo] = hist(changeLocs, [0,5,6,7,8]);
bar([1:1:5],no/length(changeLocs))
title('Probability of Change at each location')
set(gca, 'FontSize', 15, ...
         'XTick', [1:1:5], 'XTickLabel',{'catch trials','Top', 'Bottom','Left','Right'});
xlabel('Locations')
ylabel('Probability');
box off

figure()
[no,xo] = hist(delayTimeChoices, [0,50,70,90,100]);
bar([1:1:5],no/length(delayTimeChoices))
title('Probability of each delay time')
set(gca, 'FontSize', 15, ...
         'XTick', [1:1:5], 'XTickLabel',{'catch','30ms', '50ms','70ms','90ms'});
xlabel('Delay Times')
ylabel('Probability');
box off

%prob of orientation
figure()
subplot(2,2,1)
[no_orUp,xo_orUp] = hist(orUp,[30,35,45,55,60,65,70,80,115,120,135,155]);
bar([1:1:12],no_orUp/length(orUp))
title('Probability of diff orientations (Top)')
set(gca, 'FontSize', 15, ...
         'XTick', [1:1:12], 'XTickLabel',{'30','35','45','55','60','65','70','80','115','120','135','155'});
xlabel('Orientations')
ylabel('Probability');
box off

subplot(2,2,2)
[no_orDown,xo_orDown] = hist(orDown,[30,35,45,55,60,65,70,80,115,120,135,155]);
bar([1:1:12],no_orDown/length(orDown))
title('Probability of diff orientations (Bottom)')
set(gca, 'FontSize', 15, ...
         'XTick', [1:1:12], 'XTickLabel',{'30','35','45','55','60','65','70','80','115','120','135','155'});
xlabel('Orientations')
ylabel('Probability');
box off

subplot(2,2,3)
[no_orLeft,xo_orLeft] = hist(orLeft,[30,35,45,55,60,65,70,80,115,120,135,155]);
bar([1:1:12],no_orLeft/length(orLeft))
title('Probability of diff orientations (Left)')
set(gca, 'FontSize', 15, ...
         'XTick', [1:1:12], 'XTickLabel',{'30','35','45','55','60','65','70','80','115','120','135','155'});
xlabel('Orientations')
ylabel('Probability');
box off

subplot(2,2,4)
[no_orRight,xo_orRight] = hist(orRight,[30,35,45,55,60,65,70,80,115,120,135,155]);
bar([1:1:12],no_orRight/length(orRight))
title('Probability of diff orientations (Right)')
set(gca, 'FontSize', 15, ...
         'XTick', [1:1:12], 'XTickLabel',{'30','35','45','55','60','65','70','80','115','120','135','155'});
xlabel('Orientations')
ylabel('Probability');
box off