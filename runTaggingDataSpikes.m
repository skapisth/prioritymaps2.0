%% runTaggingData
clc
clear all
close all

filesTemp = dir(fullfile(cd,'*.mat'));

for ii = 1:length(filesTemp)
    fprintf('loading %s files \n', filesTemp(ii).name);
    filesAll{ii} = load(fullfile(cd,filesTemp(ii).name));
    nameStruct = string(fieldnames(filesAll{ii}));
    
    if isfile(fullfile(cd,'Processed',sprintf('Processed_%s',filesTemp(ii).name)))
        filesAllTemp{ii} = load(fullfile(cd,'Processed',sprintf('Processed_%s',filesTemp(ii).name)));
        nameLoad = filesAllTemp{ii}.pptrials;
    else
        temp = filesAll{ii}.(nameStruct);
        tempFileName = (fullfile(cd,'Processed',sprintf('Processed_%s',filesTemp(ii).name)));
        save(tempFileName,'temp');
        filesAllTemp{ii}.temp = temp;
        nameLoad = filesAllTemp{ii}.temp;
    end
    
    newPPtrials{ii} = cleaningTrialsVisually(nameLoad,...
        'NewFileName',fullfile('Processed',sprintf('Processed_%s',filesTemp(ii).name)));
end


