imagelength=100;
middle=(imagelength+1)/2;
radius=20;
dimmerRadius=15;
brightness=256;
dimmerBrightness=150;
image=ones(imagelength);
for i=1:imagelength
    x=-imagelength+middle+i;  %change y coordinate by adding term
    for j=1:imagelength
        y=-imagelength+middle+j; %change x coordinate by adding term
        if(x^2+y^2<=radius^2)  %equation of circle
            image(i,j)=brightness;
        end
        if(x^2+y^2<=dimmerRadius^2)
            image(i,j)=dimmerBrightness;
        end
    end
end
imagesc(image)
axis square
axis off
colormap gray
caxis([1 256])