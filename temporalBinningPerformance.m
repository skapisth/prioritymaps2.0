function [TimeBins, TimeBins_std, DataPoints, n ,PC ,PC_se, Dprime ,Dprime_CI, index, HR, FA,RespTime, RespTime_se] = TemporalBinningPerformance(time,perf,resp,resp_time,targetO,nBins)


%% First Check that inputs are of equal length
if isequal(length(time),length(perf),length(resp),length(targetO))
    
    %% Preallocating outputs
    TimeBins = (1:nBins);
    DataPoints = {1:nBins};
    PC = (1:nBins);
    PC_se = (1:nBins);
    Dprime = (1:nBins);
    Dprime_CI = (1:nBins);
    
    %% Bin time data by grouping equal groups
    [~, ~, thresh, ~] = PartitionEqualGroups(time', nBins);
    

            for i = 1:length(thresh)-1
                lowerThresh = thresh(i);
                upperThresh = thresh(i + 1);

                % Index into trial
                IDX = find( time > lowerThresh & time <= upperThresh);
                index{i} = IDX;

                % Save average of data to create bin
                TimeBins(i) = mean(time(IDX));
                TimeBins_std(i) = std(time(IDX));
                
                % Save each averaged data point
                DataPoints{i} = time(IDX);
                n(i) = length(time(IDX));
                
                % Response Time
                RespTime(i) = mean(resp_time(IDX));
                RespTime_se(i) = std(resp_time(IDX))/length(IDX);
                
                
                % Percent correct
                PC(i) = sum(perf(index{i}))/length(IDX);
                p = 0.5; % probability of guessing correct/incorrect
                PC_se(i) = sqrt(p*(1-p)/(length(IDX)));

                % Sensitivity (dprime)
                [Dprime(i),~ ,Dprime_CI(i),~ ,~,HR(i), FA(i)] = CalculateDprime_2(resp(IDX), targetO(IDX));

            end


    end
end