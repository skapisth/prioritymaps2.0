subjects = {'z001','z029','z072','PJ'};
% subjects = {'SK','LG','z096','z001','z145','z029','z072'};
conversionFactor = 1000/340;

[TimeBins_B_all, TimeBins_std_B_all, DataPoints_B_all,...
    n_B_all,PC_B_all,PC_se_B_all, PC_CI_B_all,Dprime_B_all,Dprime_CI_B_all,...
    IDX_B_all,overlap_B_all, HR_B_all, FA_B_all,...
    RespTime_B_all,RespTime_se_B_all] = deal([]);

[TimeBins_S_all, TimeBins_std_S_all, DataPoints_S_all,...
    n_S_all,PC_S_all,PC_se_S_all, PC_CI_S_all,Dprime_S_all,Dprime_CI_S_all,...
    IDX_S_all,overlap_S_all, HR_S_all, FA_S_all,...
    RespTime_S_all,RespTime_se_S_all] = deal([]);

for si = 1:length(subjects)
    
    [timesBasedOnSaccade,condition,responses,reactionTimes] = deal([]);
    
    userVariables.allCombined_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\priorityMaps2.0\scriptsAndData\data\',subjects{si},'\eyeTracking\disc\noFace\saccade','\','allCombined\');
    load([strcat(userVariables.allCombined_path,'pptrials.mat')])
    dataHolder = pptrials;
    dataHolder = convertSamplesToMs(dataHolder, conversionFactor);
    
     for all = 1:length(dataHolder)
         timeOn = dataHolder{all}.TimeFixationOff;
         timeOff = dataHolder{all}.TimeResponseOn;
         changeOff = dataHolder{all}.TimeChangeOff;
         [output, validTrials,...
          countNoTracks, countBlinks,countInvalid,...
          countMicroSaccades, countSaccades,ms_trials] = discardTrials(dataHolder, all, timeOn, timeOff,changeOff);
      
         RT = dataHolder{all}.TimeResponseOff - dataHolder{all}.TimeResponseOn;
         if (output == 1) 
            if ( RT > 700 ||RT < 100)
                output = 0;
            end
            if isnan(dataHolder{all}.Response)
                output = 0;
            end
         end
         
         if (output == 1)
            responses(1,end+1)        = dataHolder{all}.Response;
            reactionTimes(1,end+1) = dataHolder{all}.TimeResponseOff - dataHolder{all}.TimeResponseOn;
            saccToChange = dataHolder{all}.TimeDelayOff - dataHolder{all}.TimeFixationOff;
            timesBasedOnSaccade(1,end+1) = saccToChange;
            condition(1,end+1)        = dataHolder{all}.groundTruthCond_id;
            
         end
      
     end
    
    all_conditionLabels  = unique(condition);
    idx_baseline             = find(condition == all_conditionLabels(1));
    idx_baseline_catch       = find(condition == all_conditionLabels(2));
    idx_salient              = find(condition == all_conditionLabels(3));
    idx_salient_catch        = find(condition == all_conditionLabels(4));
    
    
    timingBaseline = [idx_baseline,idx_baseline_catch];
    timingSalient = [idx_salient,idx_salient_catch];
    timeline = [0:1:450];
    window = 50;
    
    
    [TimeBins_B, TimeBins_std_B, DataPoints_B,...
    n_B ,PC_B ,PC_se_B, PC_CI_B ,Dprime_B ,Dprime_CI_B, IDX_B,overlap_B, HR_B, FA_B,...
    RespTime_B,RespTime_se_B] = smoothedTemporalBinningPerformance(timesBasedOnSaccade(timingBaseline),...
                                                                   timeline,responses(timingBaseline),responses(timingBaseline),...
                                                                   reactionTimes(timingBaseline),condition(timingBaseline),window);
                                                               
                                                            
    PC_B_all(si,:) = PC_B;
    PC_se_B_all(si,:) = PC_se_B;
    
    [TimeBins_S, TimeBins_std_S, DataPoints_S,...
    n_S ,PC_S ,PC_se_S, PC_CI_S ,Dprime_S ,Dprime_CI_S, IDX_S,overlap_S, HR_S, FA_S,...
    RespTime_S,RespTime_se_S] = smoothedTemporalBinningPerformance(timesBasedOnSaccade(timingSalient),...
                                                                   timeline,responses(timingSalient),responses(timingSalient),...
                                                                   reactionTimes(timingSalient),condition(timingSalient),window);
                                                               
    PC_S_all(si,:) = PC_S;
    PC_se_S_all(si,:) = PC_se_S;
    
end

pc_B_all = mean(PC_B_all);
pc_B_se_all = mean(PC_se_B_all);

pc_S_all = mean(PC_S_all);
pc_S_se_all = mean(PC_se_S_all);

figure()
plt = plot(TimeBins_B(~isnan(pc_B_all)),pc_B_all(~isnan(pc_B_all)),'LineStyle','-','Color',[0 0 0])
pltHandles = plt;
hold on
shadedErrorBar(TimeBins_B(~isnan(pc_B_all)),pc_B_all(~isnan(pc_B_all)),pc_B_se_all(~isnan(pc_B_all)),{'-r','color',[0 0 0]},0)
hold on
plt = plot(TimeBins_S(~isnan(pc_S_all)),pc_S_all(~isnan(pc_S_all)),'LineStyle','-','Color',	[0, 1,0])
pltHandles(2) = plt
hold on
shadedErrorBar(TimeBins_S(~isnan(pc_S_all)),pc_S_all(~isnan(pc_S_all)),pc_S_se_all(~isnan(pc_S_all)),{'-r','color',[0, 1, 0]},0)
xlabel('Delay Times (ms)')
ylabel('Proportion Correct')
set(gca,'fontsize',27,'FontWeight','Bold')
% ylim([0.5 1])
% yline(0.5,'--k',{'Chance'})
legend(pltHandles,{'Baseline','Salient'})
legend box off
set(gca,'fontsize',27,'FontWeight','Bold')

cond_label = condition;
cond_label(cond_label == 1) = 0; %baseline
cond_label(cond_label == 2) = 1; %salient
cond_label(cond_label == 3) = 1; %salient

lowL = 0;
upL = 150;

id_baseline_ST = find(timesBasedOnSaccade>=lowL & timesBasedOnSaccade<upL & cond_label == 0);
id_salient_ST = find(timesBasedOnSaccade>=lowL & timesBasedOnSaccade<upL & cond_label == 1);

[E1, E2, z,hh_100, p_100]  = Z_Test(length(find(responses(id_baseline_ST) == 1)),length(responses(id_baseline_ST)),...
                           length(find(responses(id_salient_ST)==1)),length(responses(id_salient_ST)),0.05,...
                            'two sided',1);

% strfName = 'propC_shaded_all';
% strPath_svg = 'C:/Users/sanjana/OneDrive - University of Rochester/Documents/Sanjana/Projects/priorityMaps2.0/latex';
% saveas(gcf,sprintf(strPath_svg, strfName), 'eps')
    