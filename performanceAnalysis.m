close all

directory = 'C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\priorityMaps2.0\scriptsAndData\data\SK\';
folder = '01262021\';

fullpath = strcat(directory,folder);

S = dir(fullpath);
numSessions = sum([S(~ismember({S.name},{'.','..'})).isdir]);

for eachSess = 2
    
    [responses,id_T_change,id_T_noChange,id_T_salient_change,id_T_salient_noChange,...
        id_T_baseline,id_T_salient,...
        delayTimeChoices,condition,...
        id_baseline_D50,id_baseline_D70,id_baseline_D90,id_baseline_D100,id_baseline_D200,id_baseline_D300,...
        id_salient_D50,id_salient_D70,id_salient_D90,id_salient_D100,id_salient_D200,id_salient_D300...
        id_D50,id_D70,id_D90,id_D100,...
        cond_dPrime,...
        cond_dPrime_50B,cond_dPrime_70B,cond_dPrime_90B,cond_dPrime_100B,cond_dPrime_200B,cond_dPrime_300B,...
         cond_dPrime_50S,cond_dPrime_70S,cond_dPrime_90S,cond_dPrime_100S,cond_dPrime_200S,cond_dPrime_300S] = deal([]);
    
    sessFolder = strcat(fullpath,string(eachSess),'\');
    
    load([strcat(sessFolder,'pptrials.mat')])
    dataHolder = pptrials.user;
%     dataHolder = pptrials;
    
    for all = 1:length(dataHolder)
        condition(1,end+1) = dataHolder{all}.groundTruthCond_id;
        responses(1,end+1) = dataHolder{all}.Response;
        delayTimeChoices(1,end+1) =  dataHolder{all}.delayTime;%round(dataHolder{all}.TimeDelayOff - dataHolder{all}.TimeDelayOn);
        if (dataHolder{all}.groundTruthCond_id == 0)
            id_T_change(1,end+1) = all;
            id_T_baseline(1,end+1) = all;
            if (dataHolder{all}.delayTime == 50)
                id_baseline_D50(1,end+1) = all;
                cond_dPrime_50B(1,end+1) = 1;
                cond_dPrime(1,end+1) = 1;
            elseif (dataHolder{all}.delayTime == 70)
                id_baseline_D70(1,end+1) = all;
                cond_dPrime_70B(1,end+1) = 1;
                cond_dPrime(1,end+1) = 1;
            elseif (dataHolder{all}.delayTime == 90)
                id_baseline_D90(1,end+1) = all;
                cond_dPrime_90B(1,end+1) = 1;
                cond_dPrime(1,end+1) = 1;
            elseif (dataHolder{all}.delayTime == 100)
                id_baseline_D100(1,end+1) = all;
                cond_dPrime_100B(1,end+1) = 1;
                cond_dPrime(1,end+1) = 1;
            elseif (dataHolder{all}.delayTime == 200)
                id_baseline_D200(1,end+1) = all;
                cond_dPrime_200B(1,end+1) = 1;
                cond_dPrime(1,end+1) = 1;
            elseif (dataHolder{all}.delayTime == 300)
                id_baseline_D300(1,end+1) = all;
                cond_dPrime_300B(1,end+1) = 1;
                cond_dPrime(1,end+1) = 1;
            end
            
        elseif (dataHolder{all}.groundTruthCond_id == 1)
            id_T_noChange(1,end+1) = all;
            id_T_baseline(1,end+1) = all;
            id_baseline_D50(1,end+1) = all;
            id_baseline_D70(1,end+1) = all;
            id_baseline_D90(1,end+1) = all;
            id_baseline_D100(1,end+1) = all;
            id_baseline_D200(1,end+1) = all;
            id_baseline_D300(1,end+1) = all;
            
            cond_dPrime(1,end+1) = 0;
            cond_dPrime_50B(1,end+1) = 0;
            cond_dPrime_70B(1,end+1) = 0;
            cond_dPrime_90B(1,end+1) = 0;
            cond_dPrime_100B(1,end+1) = 0;
            cond_dPrime_200B(1,end+1) = 0;
            cond_dPrime_300B(1,end+1) = 0;
        elseif (dataHolder{all}.groundTruthCond_id == 2)
            id_T_salient_change(1,end+1) = all;
            id_T_salient(1,end+1) = all;
            if (dataHolder{all}.delayTime == 50)
                id_salient_D50(1,end+1) = all;
                cond_dPrime(1,end+1) = 1;
                cond_dPrime_50S(1,end+1) = 1;
            elseif (dataHolder{all}.delayTime == 70)
                id_salient_D70(1,end+1) = all;
                cond_dPrime(1,end+1) = 1;
                 cond_dPrime_70S(1,end+1) = 1;
            elseif (dataHolder{all}.delayTime == 90)
                id_salient_D90(1,end+1) = all;
                cond_dPrime(1,end+1) = 1;
                cond_dPrime_90S(1,end+1) = 1;
            elseif (dataHolder{all}.delayTime == 100)
                id_salient_D100(1,end+1) = all;
                cond_dPrime(1,end+1) = 1;
                cond_dPrime_100S(1,end+1) = 1;
            elseif (dataHolder{all}.delayTime == 200)
                id_salient_D200(1,end+1) = all;
                cond_dPrime(1,end+1) = 1;
                cond_dPrime_200S(1,end+1) = 1;
            elseif (dataHolder{all}.delayTime == 300)
                id_salient_D300(1,end+1) = all;
                cond_dPrime(1,end+1) = 1;
                cond_dPrime_300S(1,end+1) = 1;
            end
            
        elseif (dataHolder{all}.groundTruthCond_id == 3)
            id_T_salient_noChange(1,end+1) = all;
            id_T_salient(1,end+1) = all;
            id_salient_D50(1,end+1) = all;
            id_salient_D70(1,end+1) = all;
            id_salient_D90(1,end+1) = all;
            id_salient_D100(1,end+1) = all;
            id_salient_D200(1,end+1) = all;
            id_salient_D300(1,end+1) = all;
            cond_dPrime(1,end+1) = 0;
            cond_dPrime_50S(1,end+1) = 0;
            cond_dPrime_70S(1,end+1) = 0;
            cond_dPrime_90S(1,end+1) = 0;
            cond_dPrime_100S(1,end+1) = 0;
            cond_dPrime_200S(1,end+1) = 0;
            cond_dPrime_300S(1,end+1) = 0;
           
        end
        
        if (dataHolder{all}.delayTime == 50 || dataHolder{all}.delayTime == 0)
            id_D50(1,end+1) = all;
        end
        if (dataHolder{all}.delayTime == 70 || dataHolder{all}.delayTime == 0)
            id_D70(1,end+1) = all;
        end
        if (dataHolder{all}.delayTime == 90 || dataHolder{all}.delayTime == 0)
            id_D90(1,end+1) = all;
        end
        if (dataHolder{all}.delayTime == 100 || dataHolder{all}.delayTime == 0)
            id_D100(1,end+1) = all;
        end
        
    end
end

propCorrect_change = calcPerformanceCorrect(responses, id_T_change);
propCorrect_noChange = calcPerformanceCorrect(responses, id_T_noChange);
propCorrect_salient_Change = calcPerformanceCorrect(responses, id_T_salient_change);
propCorrect_salient_noChange =calcPerformanceCorrect(responses, id_T_salient_noChange);



title('Performance in each condition')
plot([1,2,3,4],[propCorrect_change,propCorrect_noChange,propCorrect_salient_Change,propCorrect_salient_noChange]);
set(gca, 'FontSize', 15, ...
         'XTick', [1:1:4], 'XTickLabel',{'Baseline','B Catch', 'Salient','S Catch'});
xlabel('condition')
ylabel('prop correct');


propCorrect_D50 = calcPerformanceCorrect(responses, id_D50);
propCorrect_D70 = calcPerformanceCorrect(responses, id_D70);
% propCorrect_D90 = calcPerformanceCorrect(responses, id_D90);
% propCorrect_D100 = calcPerformanceCorrect(responses, id_D100);
% propCorrect_D200 = calcPerformanceCorrect(responses, id_D200);



[d50_B ,std_d_50 ,ci_d_50,crit_50 ,var_d_50, HitRate_50_B, FaRate_50] = calculateDprime(responses(id_baseline_D50), cond_dPrime(id_baseline_D50));
[d70_B ,std_d_70 ,ci_d_70,crit_70 ,var_d_70, HitRate_70_B, FaRate_70] = calculateDprime(responses(id_baseline_D70), cond_dPrime(id_baseline_D70));
[d90_B ,std_d_90 ,ci_d_90,crit_90 ,var_d_90, HitRate_90_B, FaRate_90] = calculateDprime(responses(id_baseline_D90), cond_dPrime(id_baseline_D90));
[d100_B ,std_d_100 ,ci_d_100,crit_100 ,var_d_100, HitRate_100_B, FaRate_100] = calculateDprime(responses(id_baseline_D100), cond_dPrime(id_baseline_D100));
[d200_B ,std_d_200 ,ci_d_200,crit_200 ,var_d_200, HitRate_200_B, FaRate_200] = calculateDprime(responses(id_baseline_D200), cond_dPrime(id_baseline_D200));
[d300_B ,std_d_300 ,ci_d_300,crit_300 ,var_d_300, HitRate_300_B, FaRate_300] = calculateDprime(responses(id_baseline_D300), cond_dPrime(id_baseline_D300));

[d50_S ,std_d_50 ,ci_d_50,crit_50 ,var_d_50, HitRate_50_S, FaRate_50] = calculateDprime(responses(id_salient_D50), cond_dPrime(id_salient_D50));
[d70_S ,std_d_70 ,ci_d_70,crit_70 ,var_d_70, HitRate_70_S, FaRate_70] = calculateDprime(responses(id_salient_D70), cond_dPrime(id_salient_D70));
[d90_S ,std_d_90 ,ci_d_90,crit_90 ,var_d_90, HitRate_90_S, FaRate_90] = calculateDprime(responses(id_salient_D90), cond_dPrime(id_salient_D90));
[d100_S ,std_d_100 ,ci_d_100,crit_100 ,var_d_100, HitRate_100_S, FaRate_100] = calculateDprime(responses(id_salient_D100), cond_dPrime(id_salient_D100));
[d200_S ,std_d_200 ,ci_d_100,crit_200 ,var_d_200, HitRate_200_S, FaRate_200] = calculateDprime(responses(id_salient_D200), cond_dPrime(id_salient_D200));
[d300_S ,std_d_300 ,ci_d_300,crit_300 ,var_d_300, HitRate_300_S, FaRate_300] = calculateDprime(responses(id_salient_D300), cond_dPrime(id_salient_D300));

figure()
plot([1,2,3,4,5,6],[HitRate_50_B,HitRate_70_B,HitRate_90_B,HitRate_100_B,HitRate_200_B,HitRate_300_B]);
hold on
plot([1,2,3,4,5,6],[HitRate_50_S,HitRate_70_S,HitRate_90_S,HitRate_100_S,HitRate_200_S,HitRate_300_S]);
set(gca, 'FontSize', 15, ...
         'XTick', [1:1:6], 'XTickLabel',{'B,50ms','B,70ms', 'B,90ms','B,100ms','B,200ms','B,300ms'});
xlabel('Delay Time')
ylabel('Hit rate');
legend('Baseline','Salient')
% ylim([0 1]);
title('Performance at each delay time')

figure()
plot([1,2,3,4,5,6],[d50_B,d70_B,d90_B,d100_B,d200_B,d300_B]);
hold on
plot([1,2,3,4,5,6],[d50_S,d70_S,d90_S,d100_S,d200_S,d300_S]);

set(gca, 'FontSize', 15, ...
         'XTick', [1:1:6], 'XTickLabel',{'B,50ms','B,70ms', 'B,90ms','B,100ms','B,200ms','B,300ms'});
xlabel('Delay Time')
ylabel('d-prime');
ylim([0 1]);
legend('Baseline','Salient')
title('Performance at each delay time ')





figure()
set(gca, 'FontSize', 15, ...
         'XTick', [1:1:6], 'XTickLabel',{'S,50ms','S,70ms', 'S,90ms','S,100ms','S,200ms','S,300ms'});
xlabel('Delay Time')
ylabel('d-prime');
ylim([0 1]);
title('Performance at each delay time - Salient')

figure()
[no,xo] = hist(condition, [0,1,2,3]);
bar([1:1:4],no/length(condition))
title('Probability of each condition occurance')
set(gca, 'FontSize', 15, ...
         'XTick', [1:1:4], 'XTickLabel',{'Baseline','B Catch', 'Salient','S Catch'});
xlabel('condition')
ylabel('Probability');
box off


 

function propCorrect = calcPerformanceCorrect(responseVector, totalVectorIds)
    
    propCorrect = length(find(responseVector(totalVectorIds) == 1))/length(totalVectorIds);
    
end
