function [output, validTrials,...
    countNoTracks, countBlinks,countInvalid,...
    ms_trials] = discardTrialsManTag(pptrialsInMS, k, startTimeTargetOn, endTimeTargetOff)
% Discards unnecssary trials
% Currently discards notracks, blinks and 
% microsaccades

findnoTracks = find((pptrialsInMS{k}.notracks.start > startTimeTargetOn) &  (pptrialsInMS{k}.notracks.start<endTimeTargetOff));
if length(findnoTracks) == 0
   countNoTracks = 0;
   output = 1;
   validTrials = pptrialsInMS{k};
   ms_trials = 0;
else
    countNoTracks = 1;
    validTrials = 0;
    output = 0; 
    ms_trials = 0;
end



if output
    findBlinks = find((pptrialsInMS{k}.blinks.start > startTimeTargetOn) &  (pptrialsInMS{k}.blinks.start<endTimeTargetOff));
    if length(findBlinks) == 0
        countBlinks = 0;
        output = 1;
        validTrials = pptrialsInMS{k};
        ms_trials = 0;
    else
        countBlinks = 1; %length(findBlinks);
        validTrials = 0;
        output = 0;
        ms_trials = 0;
    end
else
    countBlinks = 0;
    ms_trials = 0;
end


if output
    findInvalid = find((pptrialsInMS{k}.invalid.start > startTimeTargetOn) &  (pptrialsInMS{k}.invalid.start<endTimeTargetOff));
    if length(findInvalid) == 0
        countInvalid = 0;
        output = 1;
        validTrials = pptrialsInMS{k};
        ms_trials = 0;
    else
        countInvalid = 1; %length(findBlinks);
        validTrials = 0;
        output = 0;
        ms_trials = 0;
    end
else
    countInvalid = 0;
    ms_trials = 0;
end



end
