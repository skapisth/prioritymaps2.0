direct =  'C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\priorityMaps2.0\scriptsAndData\images\expBG_jpg\';

pathToSave = 'C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\priorityMaps2.0\scriptsAndData\';
folderName = 'histEqImgs';
save = 1;



numImgs = length(dir(strcat(direct,'*.jpg')));

% if save
%     RGB  = rand(1000, 1000, 3);  % RGB Image
%     R    = RGB(:, :, 1);
%     G    = RGB(:, :, 2);
%     B    = RGB(:, :, 3);
%     IMG1 = cat(3, G, R, B);
%     imwrite(IMG1,fullfile(strcat(direct,sprintf('%d,',numImgs+1)'.jpg')))
% end

 if ~exist(pathToSave, 'dir')
     mkdir(pathToSave)
 end
 if ~exist(strcat(pathToSave, folderName), 'dir')
     mkdir(strcat(pathToSave, folderName))
 end

for num = 32%1:(numImgs+1)
    
    fullpath = strcat(direct, sprintf('%d',num),'.jpg');
    
    img = imread(fullpath);
    
    %CONVERT THE RGB IMAGE INTO HSV IMAGE FORMAT
    HSV = rgb2hsv(img);
    
    %PERFORM HISTOGRAM EQUALIZATION ON INTENSITY COMPONENT
    Heq = histeq(HSV(:,:,3));
    
    HSV_mod = HSV;
    HSV_mod(:,:,3) = Heq;
    img_EQ_RGB = hsv2rgb(HSV_mod);
    
    if size(img_EQ_RGB,1) > size(img_EQ_RGB,2)
        img_EQ_RGB = permute(img_EQ_RGB, [2 1 3]);
    end
    
    img_EQ_RGB = imresize(img_EQ_RGB,[1080,1920]);
    
    
    fName = string(num);
    imwrite(img_EQ_RGB,fullfile(strcat(pathToSave, folderName,'\',fName,'.png')))
     
    
    
    figure;
    str_before = sprintf('image - %d - Before Histogram Equalization',num);
    str_after = sprintf('image - %d - After Histogram Equalization',num);
    subplot(1,2,1),imshow(img);title(str_before);
    subplot(1,2,2),imshow(img_EQ_RGB);title(str_after);
    
    %DISPLAY THE HISTOGRAM OF THE ORIGINAL AND THE EQUALIZED IMAGE
    hist_In = zeros([256 3]);
    hist_Out = zeros([256 3]);
    
    %HISTOGRAM OF THE RED,GREEN AND BLUE COMPONENTS
    hist_In(:,1) = imhist(img(:,:,1),256); %RED
    hist_In(:,2) = imhist(img(:,:,2),256); %GREEN
    hist_In(:,3) = imhist(img(:,:,3),256); %BLUE

    hist_Out(:,1) = imhist(img_EQ_RGB(:,:,1),256); %RED
    hist_Out(:,2) = imhist(img_EQ_RGB(:,:,2),256); %GREEN
    hist_Out(:,3) = imhist(img_EQ_RGB(:,:,3),256); %BLUE
    
%     mymap=[1 0 0; 0.2 1 0; 0 0.2 1];
    mymap = [1 0 0; 0 1 0 ;0 0 1];
    figure;
    subplot(1,2,1),bar(hist_In);colormap(mymap);legend('RED CHANNEL','GREEN CHANNEL','BLUE CHANNEL');title('Before Applying Histogram Equalization');
    subplot(1,2,2),bar(hist_Out);colormap(mymap);legend('RED CHANNEL','GREEN CHANNEL','BLUE CHANNEL');title('After Applying Histogram Equalization');
    close all
    
end