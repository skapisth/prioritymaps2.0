
function propCorrect = calcPerformanceCorrect(responseVector, totalVectorIds)
    
    propCorrect = length(find(responseVector(totalVectorIds) == 1))/length(totalVectorIds);
    
end