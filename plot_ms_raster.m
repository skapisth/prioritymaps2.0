function plot_ms_raster(ms_trials)
  
    nTrials = length(ms_trials);
    spikeTimes = cell(nTrials,1);
    [trial_id,time] = deal([]);
    
    for all_ts = 1:length(ms_trials)
        startTime = ms_trials{all_ts}.TimeFixationOff;
        endTime = ms_trials{all_ts}.TimeResponseOn;
       
        trial_time = ms_trials{all_ts}.TimeResponseOn - ms_trials{all_ts}.TimeFixationOff;
        findMicroSaccades = find((ms_trials{all_ts}.microsaccades.start > startTime  &  ms_trials{all_ts}.microsaccades.start<endTime));
        
        ms_start = round(ms_trials{all_ts}.microsaccades.start(findMicroSaccades) - trial_time);
        spikeTimes{all_ts} = ms_start;
        
        for each = 1:length(ms_start)
            trial_id(1,end+1) = all_ts;
            time(1,end+1) = ms_start(each);
        end
        
    end

    figure()
    plotSpikeRaster(spikeTimes,'PlotType','scatter','XLimForCell',[0 500]);
    xlabel('Time (ms)')
    ylabel('Trial')

    
end

