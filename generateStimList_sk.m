close all;
clear all;
clc;
% initialize the random number generator
rng default
% the first number on each sequence in the StimList indicates the trial number
% the second number indicates whether the cue indicates left or right (-1= left 1= right)
% the third number indicats whether the cue is valid invalid or neutral (valid = 1 invalid = 0 neutral = 2 catch = 3)

% use the ratio typically used in a posner task 
% 75% of the trials are valid
% 8:2:4:3 from JongenEtAl06

BlockRepetition = 100;
% every BlockSize trials subjects will be exposed to the same number of
% cues types
BlockSize = 200; 
p_b_Change = 25;%(8*100)/17;
p_b_noChange = 25;%23;%(4*100)/17;
p_s_Change = 25;%(3*100)/17;
p_s_noChange = 25;%(2*100)/17;
num_b_Change = (p_b_Change*BlockSize)/100;
num_b_noChange = (p_b_noChange*BlockSize)/100;
num_s_Change = (p_s_Change*BlockSize)/100;
num_s_noChange = (p_s_noChange*BlockSize)/100;

% minTime = 50;
% maxTime = 450;
trial = [1:1:200];

delayTimes = randperm(length(trial));


TrialType = [zeros(1,num_b_noChange) ones(1,num_b_Change)  ones(1,num_s_noChange)*2 ones(1,num_s_Change)*3];

C = [];
T = [];

r = randi([0 1],[BlockRepetition,1]);


for rr = 1: BlockRepetition
    if (r(rr)==1)
        Cue = [ones(1,ceil(num_b_Change/2)) ones(1,floor(num_b_Change/2))...
        ones(1,floor(num_s_Change/2))*3 ones(1,ceil(num_s_Change/2))*3 ...
        zeros(1,floor(num_b_noChange/2)) zeros(1,ceil(num_b_noChange/2))...
        ones(1,floor(num_s_noChange/2)) *2 ones(1,ceil(num_s_noChange/2))*2];
    elseif (r(rr)==0)
        Cue = [ones(1,ceil(num_b_Change/2)) ones(1,floor(num_b_Change/2))...
        ones(1,floor(num_s_Change/2))*3 ones(1,ceil(num_s_Change/2))*3 ...
        zeros(1,floor(num_b_noChange/2)) zeros(1,ceil(num_b_noChange/2)) ...
        ones(1,floor(num_s_noChange/2))*2 ones(1,ceil(num_s_noChange/2))*2];           
    end
    [x idx] = shuffle(Cue);
    C = [C Cue(idx)];
    T = [T TrialType(idx)];
end

ct = [];
time =  round(1 + (200-1) .* rand(length(C),1));
[x idx] = shuffle(time);
ct = [ct time(idx)];


% write the file
fnameStim = 'C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\priorityMaps2.0\scriptsAndData\StimList.txt';
f = fopen(fnameStim, 'w');
fprintf(f,'*'); %Dummy trial number
fprintf(f, '\r\n');
for ii = 1:length(T)
    W = strrep(num2str(ii),'\','\\'); % trial number
    fprintf(f, W);
     fprintf(f, '\r\n');
    W = strrep(num2str(C(ii)),'\','\\'); % condition
    fprintf(f, W);
     fprintf(f, '\r\n');
    W = strrep(num2str(ct(ii)),'\','\\'); % timing
    fprintf(f, W);
    fprintf(f, '\r\n');
    
    if(ii ~= length(T))
        fprintf(f,'*'); %Dummy trial number
        fprintf(f, '\r\n');
    end
end

fclose(f);

% check

% proportion of each trial type
b_noChange = length(find(C==0))/(length(C));
b_Change = length(find(C==1))/(length(C));
s_noChange = length(find(C==2))/(length(C));
s_Change = length(find(C==3))/(length(C));

fprintf('Proportion baseline no-change: %.2f \n', b_noChange)
fprintf('Proportion baseline change: %.2f \n', b_Change)
fprintf('Proportion salient no-change: %.2f \n', s_noChange)
fprintf('Proportion salient change: %.2f \n', s_Change)

[t_0_50,t_50_100,t_100_150,t_150_200,t_200_250,t_250_300,t_300_350,t_350_400,t_400_450] =  deal([]);

for allTimes = 1:length(ct)
    if (ct(allTimes) >=0 && ct(allTimes) <50)
        t_0_50(1,end+1) = allTimes;
    elseif (ct(allTimes) >=50 && ct(allTimes) <100)
        t_50_100(1,end+1) = allTimes;
    elseif (ct(allTimes) >=100 && ct(allTimes) <150)
        t_100_150(1,end+1) = allTimes;
    elseif (ct(allTimes) >=150 && ct(allTimes) <200)
        t_150_200(1,end+1) = allTimes;
    elseif (ct(allTimes) >=200 && ct(allTimes) <250)
        t_200_250(1,end+1) = allTimes;
    elseif (ct(allTimes) >=250 && ct(allTimes) <300)
        t_250_300(1,end+1) = allTimes;
    elseif (ct(allTimes) >=300 && ct(allTimes) <350)
        t_300_350(1,end+1) = allTimes;
    elseif (ct(allTimes) >=350 && ct(allTimes) <400)
        t_350_400(1,end+1) = allTimes;
    elseif (ct(allTimes) >=400 && ct(allTimes) <450)
        t_400_450(1,end+1) = allTimes;
    end
    
end

b_noChange_idx = find(C == 0);
b_change_idx = find(C == 1);
s_noChange_idx = find(C == 2);
s_change_idx = find(C == 3);

b_noCh_0_50 = b_noChange_idx(ismember(b_noChange_idx,t_0_50));
b_Ch_0_50 = b_change_idx(ismember(b_change_idx,t_0_50));

fprintf('Prop baseline no-change 0-50ms: %.2f \n', (length(b_noCh_0_50)/length(C))*100)
fprintf('Prop baseline change 0-50ms: %.2f \n', (length(b_Ch_0_50)/length(C))*100)

b_noCh_50_100 = b_noChange_idx(ismember(b_noChange_idx,t_50_100));
b_Ch_50_100 = b_change_idx(ismember(b_change_idx,t_50_100));

fprintf('Prop baseline no-change 50-100ms: %.2f \n', (length(b_noCh_50_100)/length(C))*100)
fprintf('Prop baseline change 50-100ms: %.2f \n', (length(b_Ch_50_100)/length(C))*100)

b_noCh_100_150 = b_noChange_idx(ismember(b_noChange_idx,t_100_150));
b_Ch_100_150 = b_change_idx(ismember(b_change_idx,t_100_150));

fprintf('Prop baseline no-change 100-100ms: %.2f \n', (length(b_noCh_100_150)/length(C))*100)
fprintf('Prop baseline change 50-100ms: %.2f \n', (length(b_Ch_100_150)/length(C))*100);

b_noCh_150_200 = b_noChange_idx(ismember(b_noChange_idx,t_150_200));
b_Ch_150_200 = b_change_idx(ismember(b_change_idx,t_150_200));

fprintf('Prop baseline no-change 150-200ms: %.2f \n', (length(b_noCh_150_200)/length(C))*100)
fprintf('Prop baseline change 150-200ms: %.2f \n', (length(b_Ch_150_200)/length(C))*100);

b_noCh_200_250 = b_noChange_idx(ismember(b_noChange_idx,t_200_250));
b_Ch_200_250 = b_change_idx(ismember(b_change_idx,t_200_250));

fprintf('Prop baseline no-change 200-250ms: %.2f \n', (length(b_noCh_200_250)/length(C))*100)
fprintf('Prop baseline change 200-250ms: %.2f \n', (length(b_Ch_200_250)/length(C))*100);

b_noCh_250_300 = b_noChange_idx(ismember(b_noChange_idx,t_250_300));
b_Ch_250_300 = b_change_idx(ismember(b_change_idx,t_250_300));

fprintf('Prop baseline no-change 250-300ms: %.2f \n', (length(b_noCh_250_300)/length(C))*100)
fprintf('Prop baseline change 250-300ms: %.2f \n', (length(b_Ch_250_300)/length(C))*100);

b_noCh_300_350 = b_noChange_idx(ismember(b_noChange_idx,t_300_350));
b_Ch_300_350 = b_change_idx(ismember(b_change_idx,t_300_350));

fprintf('Prop baseline no-change 300-350ms: %.2f \n', (length(b_noCh_300_350)/length(C))*100)
fprintf('Prop baseline change 300-350ms: %.2f \n', (length(b_Ch_300_350)/length(C))*100);

b_noCh_350_400 = b_noChange_idx(ismember(b_noChange_idx,t_350_400));
b_Ch_350_400 = b_change_idx(ismember(b_change_idx,t_350_400));

fprintf('Prop baseline no-change 350-400ms: %.2f \n', (length(b_noCh_350_400)/length(C))*100)
fprintf('Prop baseline change 350-400ms: %.2f \n', (length(b_Ch_350_400)/length(C))*100);

b_noCh_400_450 = b_noChange_idx(ismember(b_noChange_idx,t_400_450));
b_Ch_400_450 = b_change_idx(ismember(b_change_idx,t_400_450));

fprintf('Prop baseline no-change 400-450ms: %.2f \n', (length(b_noCh_400_450)/length(C))*100)
fprintf('Prop baseline change 400-450ms: %.2f \n\n', (length(b_Ch_400_450)/length(C))*100);

%%----------------
s_noCh_0_50 = s_noChange_idx(ismember(s_noChange_idx,t_0_50));
s_Ch_0_50 = s_change_idx(ismember(s_change_idx,t_0_50));

fprintf('Prop salient no-change 0-50ms: %.2f \n\n', (length(s_noCh_0_50)/length(C))*100)
fprintf('Prop salient change 0-50ms: %.2f \n', (length(s_Ch_0_50)/length(C))*100)

s_noCh_50_100 = s_noChange_idx(ismember(s_noChange_idx,t_50_100));
s_Ch_50_100 = s_change_idx(ismember(s_change_idx,t_50_100));

fprintf('Prop salient no-change 50-100ms: %.2f \n', (length(s_noCh_50_100)/length(C))*100)
fprintf('Prop salient change 50-100ms: %.2f \n', (length(s_Ch_50_100)/length(C))*100)

s_noCh_100_150 = s_noChange_idx(ismember(s_noChange_idx,t_100_150));
s_Ch_100_150 = s_change_idx(ismember(s_change_idx,t_100_150));

fprintf('Prop salient no-change 100-150ms: %.2f \n', (length(s_noCh_100_150)/length(C))*100)
fprintf('Prop salient change 100-150ms: %.2f \n', (length(s_Ch_100_150)/length(C))*100);

s_noCh_150_200 = s_noChange_idx(ismember(s_noChange_idx,t_150_200));
s_Ch_150_200 = s_change_idx(ismember(s_change_idx,t_150_200));

fprintf('Prop salient no-change 150-200ms: %.2f \n', (length(s_noCh_150_200)/length(C))*100)
fprintf('Prop salient change 150-200ms: %.2f \n', (length(s_Ch_150_200)/length(C))*100);

s_noCh_200_250 = s_noChange_idx(ismember(s_noChange_idx,t_200_250));
s_Ch_200_250 = s_change_idx(ismember(s_change_idx,t_200_250));

fprintf('Prop salient no-change 200-250ms: %.2f \n', (length(s_noCh_200_250)/length(C))*100)
fprintf('Prop salient change 200-250ms: %.2f \n', (length(s_Ch_200_250)/length(C))*100);

s_noCh_250_300 = s_noChange_idx(ismember(s_noChange_idx,t_250_300));
s_Ch_250_300 = s_change_idx(ismember(s_change_idx,t_250_300));

fprintf('Prop salient no-change 250-300ms: %.2f \n', (length(s_noCh_250_300)/length(C))*100)
fprintf('Prop salient change 250-300ms: %.2f \n', (length(s_Ch_250_300)/length(C))*100);

s_noCh_300_350 = s_noChange_idx(ismember(s_noChange_idx,t_300_350));
s_Ch_300_350 = s_change_idx(ismember(s_change_idx,t_300_350));

fprintf('Prop salient no-change 300-350ms: %.2f \n', (length(s_noCh_300_350)/length(C))*100)
fprintf('Prop salient change 300-350ms: %.2f \n', (length(s_Ch_300_350)/length(C))*100);

s_noCh_350_400 = s_noChange_idx(ismember(s_noChange_idx,t_350_400));
s_Ch_350_400 = s_change_idx(ismember(s_change_idx,t_350_400));

fprintf('Prop salient no-change 350-400ms: %.2f \n', (length(s_noCh_350_400)/length(C))*100)
fprintf('Prop salient change 350-400ms: %.2f \n', (length(s_Ch_350_400)/length(C))*100);

s_noCh_400_450 = s_noChange_idx(ismember(s_noChange_idx,t_400_450));
s_Ch_400_450 = s_change_idx(ismember(s_change_idx,t_400_450));

fprintf('Prop salient no-change 400-450ms: %.2f \n', (length(s_noCh_400_450)/length(C))*100)
fprintf('Prop salient change 400-450ms: %.2f \n', (length(s_Ch_400_450)/length(C))*100);





