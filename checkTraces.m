function pptrials = checkTraces(pptrials, singleSegs, filepath)


% if strcmp('D',params.machine)
%     filepath = sprintf('%s/%s',filepath1, params.session);
% else
%     filepath = filepath1;
% end
%test

trialId = 1:length(pptrials);
PLOT_DRIFTS = input('Plot individual segments (y/n): ','s');

if PLOT_DRIFTS == 'y'
    
    figure('position',[2300, 100, 1500, 800])
    subplot(2,2,2)
        for ii = 1:length(pptrials)
            plot(singleSegs(ii,:));
            hold on
        end
    subplot(2,2,[3,4]);
    %     figure('position',[2300, 100, 1500, 800])
     trialCounter = 1;
    for driftIdx = 1:length(pptrials)
       if (trialCounter) > length(trialId)
            return;
        end
        currentTrialId = trialId(trialCounter);
        hold off
        poiStart = pptrials{currentTrialId}.TimeStimulusOn;
        poiEnd = min(pptrials{currentTrialId}.TimeStimulusOff);%, pptrials{currentTrialId}.ResponseTime
        
        poi = fill([poiStart, poiStart ...
            poiEnd, poiEnd], ...
            [-50, 50, 50, -50], ...
            'g', 'EdgeColor', 'g', 'LineWidth', 2, 'Clipping', 'On', 'FaceAlpha', 0.25);
        
        xTrace = pptrials{currentTrialId}.x.position + pptrials{currentTrialId}.xoffset * pptrials{currentTrialId}.pixelAngle;
        yTrace = pptrials{currentTrialId}.y.position + pptrials{currentTrialId}.yoffset * pptrials{currentTrialId}.pixelAngle;
        
        hold on
        hx = plot(1:(1000/330):length(xTrace)*(1000/330),xTrace, 'Color', [0 0 200] / 255, 'HitTest', 'off', 'LineWidth', 2);
        hy = plot(1:(1000/330):length(yTrace)*(1000/330),yTrace, 'Color', [0 180 0] / 255, 'HitTest', 'off', 'LineWidth', 2);
        subplot(2,2,1)
        plot(singleSegs(driftIdx,:))
        axis([0 180 0 150]) 

        subplot(2,2,[3,4]);
%         axis([poiStart - 400, poiEnd + 400, -500, 500])
         axis([poiStart - 400, poiEnd + 400, -30, 30])
        xlabel('Time','FontWeight','bold')
        ylabel('arcmin','FontWeight','bold')
        title(sprintf('Trial: %i', currentTrialId)); %
        
        set(gca, 'FontSize', 12)
        timeTrial = sprintf('Stimulus Duration is %.1f ms', pptrials{currentTrialId}.TimeStimulusOff-pptrials{currentTrialId}.TimeStimulusOn);
%         timeTrial = sprintf('Stimulus Duration is %.1f ms', pptrials{currentTrialId}.TimeResponseOn-pptrials{currentTrialId}.TimeFixationOff);
        
%         text(20,502,timeTrial);
        text(20,32,timeTrial);
        for i = 1:length(pptrials{currentTrialId}.microsaccades.start)
            poi = fill([pptrials{currentTrialId}.microsaccades.start(i), pptrials{currentTrialId}.microsaccades.start(i) ...
                pptrials{currentTrialId}.microsaccades.start(i) + pptrials{currentTrialId}.microsaccades.duration(i), pptrials{currentTrialId}.microsaccades.start(i) + pptrials{currentTrialId}.microsaccades.duration(i)], ...
                [-35, 35, 35, -35], ...
                'r', 'EdgeColor', 'r', 'LineWidth', 2, 'Clipping', 'On', 'FaceAlpha', 0.25);
        end
        legend([hx, hy, poi], {'X','Y','MS'},'FontWeight','bold')
        cont = input('Stop (s), MS in the drift(m), wrong ms labelled(c), or toss (d) or saccade (k) or manTagSacc(l) OR \n Back a trial(z): ','s');
        if cont == 'z'
            if trialCounter == 1
                trialCounter = 1;
            else
                trialCounter = trialCounter-1;
            end
            continue;
        end
        if cont == 'l'
            sStartTime = input('What is the Saccade start time?');
            pptrials{currentTrialId}.manTagSacc.start = sStartTime;
            sDuration = input('What is the end time of the S? ');
            pptrials{currentTrialId}.manTagSacc.duration = (sDuration - sStartTime);
            fprintf('Saving pptrials\n')
            save(fullfile(filepath, 'pptrials.mat'), 'pptrials')
        end
        cont = input('is a saccade missed (y/n): ','s');
        if cont == 'y'
            sStartTime = input('What is the Saccade start time?');
            pptrials{currentTrialId}.saccades.start = sStartTime;
            sDuration = input('What is the end time of the S? ');
            pptrials{currentTrialId}.saccades.duration = (sDuration - sStartTime);
            checkMore = input('is there another one(y/n): ','s');
            if checkMore == 'y'
                sStartTime = input('What is the Saccade start time?');
                pptrials{currentTrialId}.saccades.start(1,end+1) = sStartTime;
                sDuration = input('What is the end time of the S? ');
                pptrials{currentTrialId}.saccades.duration(1,end+1) = (sDuration - sStartTime);
            end
            checkMore = input('are there more missed(y/n): ','s');
            if checkMore == 'y'
                sStartTime = input('What is the Saccade start time?');
                pptrials{currentTrialId}.saccades.start(1,end+1) = sStartTime;
                sDuration = input('What is the end time of the S? ');
                pptrials{currentTrialId}.saccades.duration(1,end+1) = (sDuration - sStartTime);
            end
            fprintf('Saving pptrials\n')
            save(fullfile(filepath, 'pptrials.mat'), 'pptrials')
         end
         cont = input('Stop (s), MS in the drift(m), wrong ms labelled(c), or toss (d) or saccade (k): ','s');
        if cont == 's'
            save(fullfile(filepath, 'pptrials.mat'), 'pptrials')
            break
        elseif cont == 'm'
            
            moreMS = input('is a MS missed (y/n): ','s');
            if moreMS == 'y'
                whichMS = input('which MS is not tagged (1/2): ','s');
                if whichMS == '1'
                    ms2StartTime = input('What is the MS start time?');
                    pptrials{currentTrialId}.microsaccades.start(1,end+1) = ms2StartTime;
                    ms2Duration = input('What is the end time of the MS? ');
                    pptrials{currentTrialId}.microsaccades.duration(1,end+1) = (ms2Duration - ms2StartTime);
                    checkMore = input('is there another one(y/n): ','s');
                    if checkMore == 'y'
                        ms3StartTime = input('What is the MS start time?');
                        pptrials{currentTrialId}.microsaccades.start(1,end+1) = ms3StartTime;
                        ms3Duration = input('What is the end time of the MS? ');
                        pptrials{currentTrialId}.microsaccades.duration(1,end+1) = (ms3Duration - ms3StartTime);
                    end
                elseif whichMS == '2'
                    ms3StartTime = input('What is the MS start time?');
                    pptrials{currentTrialId}.microsaccades.start(1,end+1) = ms3StartTime;
                    ms3Duration = input('What is the end time of the MS? ');
                    pptrials{currentTrialId}.microsaccades.duration(1,end+1) = (ms3Duration - ms3StartTime);
                end
            elseif moreMS == 'n'
                msStartTime = input('What is the MS start time?');
                pptrials{currentTrialId}.microsaccades.start = msStartTime;
                msDuration = input('What is the end time of the MS? ');
                pptrials{currentTrialId}.microsaccades.duration = (msDuration - msStartTime);
            end
            
            fprintf('Saving pptrials\n')
            save(fullfile(filepath, 'pptrials.mat'), 'pptrials')
            continue;
        elseif cont == 'd'
            invalidStartTime = input('What is the bad eye movement start time? ');
            pptrials{currentTrialId}.invalid.start = invalidStartTime;
            fprintf('Saving pptrials\n')
            save(fullfile(filepath, 'pptrials.mat'), 'pptrials')
            continue;
        elseif cont == 'c'
            fprintf('Total MS times: %i \n', pptrials{currentTrialId}.microsaccades.start)
            dStartTime = input('When does the wrong MS start?');
            [~,wrongMS] = min(abs(dStartTime - pptrials{currentTrialId}.microsaccades.start));%find((pptrials{currentTrialId}.microsaccades.start) == dStartTime);
            msWrongDuration = input('What is the end time of the MS? ');
            
            pptrials{currentTrialId}.microsaccades.duration(wrongMS) = (msWrongDuration - dStartTime);
            newStartTime = input('When does the MS actually start? (set to 0 if not present)');
            pptrials{currentTrialId}.microsaccades.start(wrongMS) = newStartTime;
            newDurationTime = input('When does the MS end? (set to 0 if not present)');
            pptrials{currentTrialId}.microsaccades.duration(wrongMS) = newDurationTime-newStartTime;
            
            moreMS = input('are more wrongly tagged(y/n): ','s');
            if moreMS == 'y'
                whichMS = input('which MS is wrongly tagged (2/3): ','s');
                if whichMS == '2'
                    dStartTime = input('When does the wrong MS start?');
                    [~,wrongMS] = min(abs(dStartTime - pptrials{currentTrialId}.microsaccades.start));%find((pptrials{currentTrialId}.microsaccades.start) == dStartTime);
                    msWrongDuration = input('What is the end time of the MS? ');

                    pptrials{currentTrialId}.microsaccades.duration(wrongMS) = (msWrongDuration - dStartTime);
                    newStartTime = input('When does the MS actually start? (set to 0 if not present)');
                    pptrials{currentTrialId}.microsaccades.start(wrongMS) = newStartTime;
                    newDurationTime = input('When does the MS end? (set to 0 if not present)');
                    pptrials{currentTrialId}.microsaccades.duration(wrongMS) = newDurationTime-newStartTime;
                    checkMore = input('is there another one(y/n): ','s');
                    if checkMore== 'y'
                       dStartTime = input('When does the wrong MS start?');
                        [~,wrongMS] = min(abs(dStartTime - pptrials{currentTrialId}.microsaccades.start));%find((pptrials{currentTrialId}.microsaccades.start) == dStartTime);
                        msWrongDuration = input('What is the end time of the MS? ');

                        pptrials{currentTrialId}.microsaccades.duration(wrongMS) = (msWrongDuration - dStartTime);
                        newStartTime = input('When does the MS actually start? (set to 0 if not present)');
                        pptrials{currentTrialId}.microsaccades.start(wrongMS) = newStartTime;
                        newDurationTime = input('When does the MS end? (set to 0 if not present)');
                        pptrials{currentTrialId}.microsaccades.duration(wrongMS) = newDurationTime-newStartTime;
                        checkMore = input('is there another one(y/n): ','s');
                    end
                    if checkMore== 'y'
                       dStartTime = input('When does the wrong MS start?');
                        [~,wrongMS] = min(abs(dStartTime - pptrials{currentTrialId}.microsaccades.start));%find((pptrials{currentTrialId}.microsaccades.start) == dStartTime);
                        msWrongDuration = input('What is the end time of the MS? ');

                        pptrials{currentTrialId}.microsaccades.duration(wrongMS) = (msWrongDuration - dStartTime);
                        newStartTime = input('When does the MS actually start? (set to 0 if not present)');
                        pptrials{currentTrialId}.microsaccades.start(wrongMS) = newStartTime;
                        newDurationTime = input('When does the MS end? (set to 0 if not present)');
                        pptrials{currentTrialId}.microsaccades.duration(wrongMS) = newDurationTime-newStartTime;
                        checkMore = input('is there another one(y/n): ','s');
                    end
                elseif whichMS == '3'
                        dStartTime = input('When does the wrong MS start?');
                        [~,wrongMS] = min(abs(dStartTime - pptrials{currentTrialId}.microsaccades.start));%find((pptrials{currentTrialId}.microsaccades.start) == dStartTime);
                        msWrongDuration = input('What is the end time of the MS? ');

                        pptrials{currentTrialId}.microsaccades.duration(wrongMS) = (msWrongDuration - dStartTime);
                        newStartTime = input('When does the MS actually start? (set to 0 if not present)');
                        pptrials{currentTrialId}.microsaccades.start(wrongMS) = newStartTime;
                        newDurationTime = input('When does the MS end? (set to 0 if not present)');
                        pptrials{currentTrialId}.microsaccades.duration(wrongMS) = newDurationTime-newStartTime;
                end
            end
            
            fprintf('Saving pptrials\n')
            save(fullfile(filepath, 'pptrials.mat'), 'pptrials')
            continue;
        end
    trialCounter = trialCounter + 1;   
    end
    
    
end

close

end
