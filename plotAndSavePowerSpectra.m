clear all;
direct =  'C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\priorityMaps2.0\scriptsAndData\images\expBG_jpg\';
pathToSave = 'C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\priorityMaps2.0\scriptsAndData\';
folderName = 'powerSpec';

if ~exist(pathToSave, 'dir')
    mkdir(pathToSave)
end
if ~exist(strcat(pathToSave, folderName), 'dir')
    mkdir(strcat(pathToSave, folderName))
end


% numImgs = length(dir(strcat(direct,'*.jpg')));

pixel_angle = 0.6; % arcmin, angle of each pixel in image
Fs_spatial = 60 / pixel_angle; % samples per degree of visual angle, (cpd)

val = 18;
for num = 1:9
    
    fullpath = strcat(direct, sprintf('%d',num+val),'.jpg');
    
    img_color = double(imread(fullpath));
    
    img = rgb2gray(img_color);
    img = imresize(img,[1080,1920]);
    
    size_img_x = size(img,2);
    size_img_y = size(img,1);
    
    
    % fft2 computes the spectrum of an image, a complex matrix.
    spec_img = fft2(img); % representation of image in frequency domain

    % It can be decomposed into its magnitude and phase components.
    mag_img = abs(spec_img); % magnitude spectrum
    phase_img = angle(spec_img); % phase spectrum

    
    % The Nyquist frequency is the highest frequency we can detect with this
    % sampling frequency.
    nyquist_spatial = Fs_spatial / 2; % cpd)

    % matlab by default returns spectra from 0 to 2*nyquist.
    img_spatial_freq_x = Fs_spatial * (0:size_img_x-1) / size_img_x;%  (cpd)
    img_spatial_freq_y = Fs_spatial * (0:size_img_y-1) / size_img_y;%  (cpd)

    % we prefer to plot them from -nysquist to nyquist
    img_spatial_freq_shifted_x = Fs_spatial * (-floor(size_img_x/2)+1:(size_img_x/2)) / size_img_x;
    img_spatial_freq_shifted_y = Fs_spatial * (-floor(size_img_y/2)+1:(size_img_y/2)) / size_img_y;

    % Magnitude spectra are usually easier to visually as a power-spectrum:
    power_nature = mag_img.^2;

    % Plotting
    if num == 1
        fig = figure();
    end
    hold on
    subplot(3, 3, num);
    pcolor(img_spatial_freq_x, img_spatial_freq_y, pow2db(power_nature));
    axis square; shading interp;
    colormap hot; caxis([0, 60]);
    hcb = colorbar; set(get(hcb, 'Title'), 'String', 'db');
    title(strcat('power spectrum - img', sprintf('%d',num+val)));
    xlabel('spatial frequency (cpd)'); ylabel('spatial frequency (cpd)');
    

%     subplot(2, 2, 2);
%     pcolor(img_spatial_freq_shifted_x, img_spatial_freq_shifted_y,fftshift(pow2db(power_nature)));
%     axis square; shading interp;
%     colormap hot; caxis([0, 60]);
%     hcb = colorbar; set(get(hcb, 'Title'), 'String', 'db');
%     title('power spectrum (shifted)');
%     xlabel('spatial frequency (cpd)'); ylabel('spatial frequency (cpd)');
    
    

    
    
end
% fName = string(num);
% saveas(fig, fullfile(strcat(pathToSave, folderName,'\',fName,'.png')))
% close all
