function [output, validTrials,...
    countNoTracks, countBlinks,countInvalid,...
    countMicroSaccades, countSaccades,ms_trials] = discardTrials(pptrialsInMS, k, startTimeTargetOn, endTimeTargetOff, changeOff,ms_trials)
% Discards unnecssary trials
% Currently discards notracks, blinks and 
% microsaccades

findnoTracks = find((pptrialsInMS{k}.notracks.start > startTimeTargetOn) &  (pptrialsInMS{k}.notracks.start<endTimeTargetOff));
if length(findnoTracks) == 0
   countNoTracks = 0;
   output = 1;
   validTrials = pptrialsInMS{k};
%    ms_trials = 0;
else
    countNoTracks = 1;
    validTrials = 0;
    output = 0; 
%     ms_trials = 0;
end



if output
    findBlinks = find((pptrialsInMS{k}.blinks.start > startTimeTargetOn) &  (pptrialsInMS{k}.blinks.start<endTimeTargetOff));
    if length(findBlinks) == 0
        countBlinks = 0;
        output = 1;
        validTrials = pptrialsInMS{k};
%         ms_trials = 0;
    else
        countBlinks = 1; %length(findBlinks);
        validTrials = 0;
        output = 0;
%         ms_trials = 0;
    end
else
    countBlinks = 0;
%     ms_trials = 0;
end


if output
    findInvalid = find((pptrialsInMS{k}.invalid.start > startTimeTargetOn) &  (pptrialsInMS{k}.invalid.start<endTimeTargetOff));
    if length(findInvalid) == 0
        countInvalid = 0;
        output = 1;
        validTrials = pptrialsInMS{k};
%         ms_trials = 0;
    else
        countInvalid = 1; %length(findBlinks);
        validTrials = 0;
        output = 0;
%         ms_trials = 0;
    end
else
    countInvalid = 0;
%     ms_trials = 0;
end

if output
    findMicroSaccades = find((pptrialsInMS{k}.microsaccades.start > startTimeTargetOn &  pptrialsInMS{k}.microsaccades.start<changeOff));
  
    if isempty(findMicroSaccades)
        countMicroSaccades = 0;
        output = 1;
        validTrials = pptrialsInMS{k};
%         ms_trials = 0;
    else

        %% use for fixation
%         countMicroSaccades = length(findMicroSaccades);
%         output = 0;
%         validTrials = 0;%pptrialsInMS{k};
        
        %% use when saccade is used
        for all = 1:length(findMicroSaccades)
             msTimeSlot = pptrialsInMS{k}.microsaccades.start(findMicroSaccades(all)) - startTimeTargetOn;
             if (all == 1 && msTimeSlot <=150 && length(findMicroSaccades) == 1)
%                  countMicroSaccades = 0;
                 countMicroSaccades = length(findMicroSaccades);
                 validTrials = pptrialsInMS{k};
                 output = 1;
             else
                 countMicroSaccades = length(findMicroSaccades);
                 output = 0;
                 validTrials = pptrialsInMS{k};
             end
        end
        ms_trials{1,end+1} = pptrialsInMS{k};
    end
        
 
    
else
    countMicroSaccades = 0;
end

if output 
    if (countBlinks == 0 && countNoTracks == 0 && countMicroSaccades == 0)
        findSaccades = find((pptrialsInMS{k}.saccades.start > startTimeTargetOn) &  (pptrialsInMS{k}.saccades.start< endTimeTargetOff));

        if isempty(findSaccades)
            countSaccades = 0;
            output = 1;
            validTrials = pptrialsInMS{k};
%             ms_trials = 0;
        else

            countSaccades = length(findSaccades);
            output = 0;
            validTrials = 0;
            %%use when saccade isused
            for all = 1:length(findSaccades)
                 sTimeSlot = pptrialsInMS{k}.saccades.start(findSaccades(all)) - startTimeTargetOn;
                 if (all == 1 && sTimeSlot <=150 && length(findSaccades) == 1)
%                      countSaccades = 0;
                     validTrials = pptrialsInMS{k};
                     output = 1;
                 else
                     output = 0;
                     validTrials = pptrialsInMS{k};
                 end
            end
%             ms_trials = pptrialsInMS{k};
        end
    else
        countSaccades = 0;
        
    end
else
    countSaccades = 0;
    
end




    

end
