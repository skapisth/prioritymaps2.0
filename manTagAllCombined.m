clear all;
close all;
clc

userFlags = struct(...
     'saveFileFlag', '0',...
     'combinePPTrials', '0',...
      'manMark','1');
  
subject = 'SK';
userVariables = struct(...
    'folderName','1',...
    'pathToSaveTaggedData',  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\priorityMaps2.0\scriptsAndData\data\',subject,'\','eyeTracking\disc\noFace\','taggedData\'),...
    'allCombined_path', strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\priorityMaps2.0\scriptsAndData\data\',subject,'\','eyeTracking','\disc\noFace','\','allCombined\'));
conversionFactor = 1000/330;

pathToFile = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\priorityMaps2.0\scriptsAndData\data\',subject,'\','eyeTracking','\disc\noFace','\','actual_time\09122021\1\');

load([strcat(pathToFile,'pptrials.mat')])
dataHolder = pptrials;
dataHolder = convertSamplesToMs(dataHolder, conversionFactor);

ppTrialsForManTag = {};

for all = 1:length(dataHolder)
    timeOn = dataHolder{all}.TimeFixationOff;
    timeOff = dataHolder{all}.TimeResponseOn;
    [output, ~,...
        ~, ~,~,...
        ~] = discardTrialsManTag(dataHolder, all, timeOn, timeOff);
    
    
    if (output == 1) 
        RT = dataHolder{all}.TimeResponseOff - dataHolder{all}.TimeResponseOn;
        if ( RT > 700 ||RT < 100)
            output = 0;
        end
    end
    
     if (output == 1)
            ppTrialsForManTag{1,end+1} = dataHolder{all};
     end
end

if (userFlags.manMark == '1')
    pptrials = callToSaveCombineAndManuallyMark(userVariables, userFlags,ppTrialsForManTag);
end