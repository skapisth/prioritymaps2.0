function list = CalList2()

% stimulus duration: stimOff-saccOn.
% this is because the stimulus duration 
% is calculated from the moment the eye lands.

list = eis_readData([], 'x');
list = eis_readData(list, 'y');

list = eis_readData(list, 'trigger', 'blink');
list = eis_readData(list, 'trigger', 'notrack');
list = eis_readData(list, 'uservar','xoffset');
list = eis_readData(list, 'uservar','yoffset');

%time of the response (locked to the start of the trial)		
list = eis_readData(list, 'uservar','screenw');
list = eis_readData(list, 'uservar','screenh');
list = eis_readData(list, 'uservar','screenr');
list = eis_readData(list, 'uservar','pixelAngle');
list = eis_readData(list, 'uservar','realSacc');
list = eis_readData(list, 'uservar','orUp');
list = eis_readData(list, 'uservar','orDown');
list = eis_readData(list, 'uservar','orLeft');
list = eis_readData(list, 'uservar','orRight');
list = eis_readData(list, 'uservar','changeGaborLoc');
list = eis_readData(list, 'uservar','saccDur');
list = eis_readData(list, 'uservar','leftAngle');
list = eis_readData(list, 'uservar','rightAngle');

list = eis_readData(list, 'uservar', 'fixEcc');
list = eis_readData(list, 'uservar', 'centerX');
list = eis_readData(list, 'uservar', 'centerY');
list = eis_readData(list, 'uservar', 'periphX');
list = eis_readData(list, 'uservar', 'periphY');

list = eis_readData(list, 'uservar', 'fixTime');
list = eis_readData(list, 'uservar', 'gratingWidth');
list = eis_readData(list, 'uservar', 'contrast');
list = eis_readData(list, 'uservar', 'gaborSpatialFrequency');
list = eis_readData(list, 'uservar', 'trialid');
%list = eis_readData(list, 'uservar', 'd_Eye_From_Face');
% list = eis_readData(list, 'uservar', 'bgContrast');


list = eis_readData(list, 'uservar', 'pestLevel_top');
list = eis_readData(list, 'uservar', 'pestLevel_bottom');
list = eis_readData(list, 'uservar', 'pestLevel_left');
list = eis_readData(list, 'uservar', 'pestLevel_right');
list = eis_readData(list, 'uservar', 'minPestLevel');
list = eis_readData(list, 'uservar', 'maxPestLevel');

list = eis_readData(list, 'stream',0, 'double');
list = eis_readData(list, 'stream',1, 'double');
list = eis_readData(list, 'stream',2, 'double');
list = eis_readData(list, 'stream',3, 'double');
list = eis_readData(list, 'stream',4, 'double');
list = eis_readData(list, 'stream',5, 'double');
list = eis_readData(list, 'stream',6, 'double');
list = eis_readData(list, 'stream',7, 'double');


%list = eis_readData(list, 'uservar', 'landingX');
%list = eis_readData(list, 'uservar', 'landingY');
list = eis_readData(list, 'uservar', 'Response');
list = eis_readData(list, 'uservar', 'groundTruthCond_id');
list = eis_readData(list, 'uservar', 'whichCondition');
list = eis_readData(list, 'uservar', 'nearORfar');
list = eis_readData(list, 'uservar', 'circle_1_loc');
list = eis_readData(list, 'uservar', 'circle_2_loc');

list = eis_readData(list, 'uservar', 'TimeFixationOn');
list = eis_readData(list, 'uservar', 'TimeFixationOff');
list = eis_readData(list, 'uservar', 'TimeStimulusOn');
list = eis_readData(list, 'uservar', 'TimeStimulusOff');
list = eis_readData(list, 'uservar', 'TimeDelayOn');
list = eis_readData(list, 'uservar', 'TimeDelayOff');
list = eis_readData(list, 'uservar', 'TimeChangeOn');
list = eis_readData(list, 'uservar', 'TimeChangeOff');
list = eis_readData(list, 'uservar', 'TimeResponseOn');
list = eis_readData(list, 'uservar', 'TimeResponseOff');
list = eis_readData(list, 'uservar', 'delayTime');
list = eis_readData(list, 'uservar', 'delayTimePicked');





end








