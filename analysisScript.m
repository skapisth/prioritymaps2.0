clc;
close all
clear all;
eyeTrackingData = 1;

processAndPlot = 1;

userFlags = struct(...
     'saveFileFlag', '0',...
     'combinePPTrials', '0');

subject = 'SK';
date = '02222021';
session = '1';
combined = '0';
conversionFactor = 1000/330;

if eyeTrackingData
    userVariables = struct(...
    'folderName','1',...
    'pathToFile', strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\priorityMaps2.0\scriptsAndData\data\',subject,'\','eyeTracking','\'),...
    'pathToSave',  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\priorityMaps2.0\scriptsAndData\data\',subject,'\','eyeTracking','\','\processedMats\'),...
    'allCombined_path',strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\priorityMaps2.0\scriptsAndData\data\SK\','eyeTracking','\','allCombined\'));
   
else
    userVariables = struct(...
        'folderName','1',...
        'pathToFile', strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\priorityMaps2.0\scriptsAndData\data\',subject,'\'),...
        'pathToSave',  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\priorityMaps2.0\scriptsAndData\data\',subject,'\processedMats\'),...
        'allCombined_path','C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\priorityMaps2.0\scriptsAndData\data\SK\allCombined\');
end

fullpath = strcat(userVariables.pathToFile,date,'\');

S = dir(fullpath);
numSessions = sum([S(~ismember({S.name},{'.','..'})).isdir]);

if (userFlags.saveFileFlag == '1' || userFlags.combinePPTrials == '1')
    for eachSess = 1%length(numSessions)
        userVariables.folderName = 4;%eachSess;
        sessFolder = strcat(fullpath,string(eachSess),'\');
        load([strcat(sessFolder,'pptrials.mat')])
        if eyeTrackingData
            dataHolder = pptrials;
            callToSaveCombineAndManuallyMark(userVariables, userFlags,dataHolder)
            dataHolder = convertSamplesToMs(dataHolder, conversionFactor);
        else
            dataHolder = pptrials.user;
            callToSaveCombineAndManuallyMark(userVariables, userFlags,dataHolder')
        end
        
    end
end


if processAndPlot
    [responses,condition,delayTimeChoices,delayTimeChoicesPicked...
        delayTimesOccurence_B,delayTimesOccurence_S,...
        reactionTimes] = deal([]);


    if combined == '1'
        load([strcat(userVariables.allCombined_path,'pptrials.mat')])
        dataHolder = pptrials;
    else
        load([strcat(userVariables.pathToFile,date,'\',session,'\','pptrials.mat')])
        if eyeTrackingData
            dataHolder = pptrials;
        else
            dataHolder = pptrials.user;
        end
    end

    % else
    %     dataHolder = pptrials;
    % end



    for all = 1:length(dataHolder)
        
        timeOn = dataHolder{all}.TimeFixationOff;
        timeOff = dataHolder{all}.TimeResponseOn;
        [output, validTrials,...
         countNoTracks, countBlinks,...
         countMicroSaccades, countSaccades] = discardTrials(dataHolder, all, timeOn, timeOff);
        
        if output
            condition(1,end+1)        = dataHolder{all}.groundTruthCond_id;
            responses(1,end+1)        = dataHolder{all}.Response;
            delayTimeChoicesPicked(1,end+1) = dataHolder{all}.delayTimePicked;
            delayTimeChoices(1,end+1) = dataHolder{all}.delayTime;
            reactionTimes(1,end+1) = dataHolder{all}.TimeResponseOff - dataHolder{all}.TimeResponseOn;
        end
    end

    all_delayTimeChoicesPicked = unique(delayTimeChoicesPicked);
    all_delayTimeChoices = unique(delayTimeChoices);
    all_conditionLabels  = unique(condition);



    idx_baseline             = find(condition == all_conditionLabels(1));
    idx_baseline_catch       = find(condition == all_conditionLabels(2));
    idx_salient              = find(condition == all_conditionLabels(3));
    idx_salient_catch        = find(condition == all_conditionLabels(4));


    conditionNames = ["baseline","salient"];
    for numDelayTimes = 1:length(all_delayTimeChoicesPicked)
        varNameTimings       = "trials_"  + num2str(all_delayTimeChoicesPicked(numDelayTimes)) + "_" + "ms";
        eval("trials_" + num2str(all_delayTimeChoicesPicked(numDelayTimes)) + "_" +"ms" + "=" + "[]");
        numCounts     = find(delayTimeChoicesPicked == all_delayTimeChoicesPicked(numDelayTimes));
        for all = 1:length(numCounts)
            eval(varNameTimings + "(1,end+1)" + "=" + num2str(numCounts(all)));
        end

        for conds = 1:length(conditionNames)
            varNameCondition1 = "idx_" + conditionNames(conds)+ "_" + "change" + "_" + num2str(all_delayTimeChoicesPicked(numDelayTimes)) + "_" + "ms";
            eval(varNameCondition1 + "=" + "[]");
            
            varNameCondition2 = "idx_" + conditionNames(conds)+ "_" + "noChange" + "_" + num2str(all_delayTimeChoicesPicked(numDelayTimes)) + "_" + "ms";
            eval(varNameCondition2 + "=" + "[]");

            if (conds == 1 )
                
                varNameFinal = strcat(varNameTimings,"(","ismember","(",varNameTimings,",","idx_baseline",")",")");
                eval(varNameCondition1  +  "=" + varNameFinal);
                varNameFinal = strcat(varNameTimings,"(","ismember","(",varNameTimings,",","idx_baseline_catch",")",")");
                eval(varNameCondition2  +  "=" + varNameFinal);

                varNameLength = strcat("delayTimesOccurence" + "=" + "length(" + varNameCondition1 + ")" );
                eval(varNameLength );
                

                varNameLabel = "baselineLabel_" + num2str(all_delayTimeChoicesPicked(numDelayTimes));
                eval(varNameLabel + "=" + "[]");
                eval(varNameLabel + "(1,end+1)" + "=" + num2str(0));

                delayTimesOccurence_B(1,end+1) = delayTimesOccurence;
                varNameLength = strcat("delayTimesOccurence" + "=" + "length(" + varNameCondition2 + ")" );
                eval(varNameLength );
                delayTimesOccurence_B(1,end+1) = delayTimesOccurence;
                
                
            elseif (conds == 2)

                varNameFinal = strcat(varNameTimings,"(","ismember","(",varNameTimings,",","idx_salient",")",")");
                eval(varNameCondition1  +  "=" + varNameFinal);
                varNameFinal = strcat(varNameTimings,"(","ismember","(",varNameTimings,",","idx_salient_catch",")",")");
                eval(varNameCondition2  +  "=" + varNameFinal);

                varNameLength = strcat("delayTimesOccurence" + "=" + "length(" + varNameCondition1 + ")" );
                eval(varNameLength );
                

                varNameLabel = "salientLabel_" + num2str(all_delayTimeChoicesPicked(numDelayTimes));
                eval(varNameLabel + "=" + "[]");
                eval(varNameLabel + "(1,end+1)" + "=" + num2str(0));

                delayTimesOccurence_S(1,end+1) = delayTimesOccurence;
                varNameLength = strcat("delayTimesOccurence" + "=" + "length(" + varNameCondition2 + ")" );
                eval(varNameLength );
                delayTimesOccurence_S(1,end+1) = delayTimesOccurence;
                
            end
        end
    end


    condition_labels_original = condition;

    %% relabeling conditions to be binary for calculating d-primes
    condition(condition == 0)  = -1; %Baseline change
    condition(condition == 1)  = -2; %Baseline no change
    condition(condition == 2)  = -3; %Salient change
    condition(condition == 3)  = -4; %Salient no change

    condition(condition == -1)  = 1; %Baseline change
    condition(condition == -2)  = 0; %Baseline no change
    condition(condition == -3)  = 1; %Salient change
    condition(condition == -4)  = 0; %Salient no change

    id_baseline_50ms_0ms = [idx_baseline_change_50_ms,idx_baseline_noChange_50_ms];
    id_baseline_70ms_0ms = [idx_baseline_change_70_ms,idx_baseline_noChange_70_ms];
    id_baseline_90ms_0ms = [idx_baseline_change_90_ms,idx_baseline_noChange_90_ms];
    id_baseline_100ms_0ms = [idx_baseline_change_100_ms,idx_baseline_noChange_100_ms];
    id_baseline_150ms_0ms = [idx_baseline_change_150_ms,idx_baseline_noChange_150_ms];
    id_baseline_200ms_0ms = [idx_baseline_change_200_ms,idx_baseline_noChange_200_ms];
    id_baseline_250ms_0ms = [idx_baseline_change_250_ms,idx_baseline_noChange_250_ms];
    id_baseline_300ms_0ms = [idx_baseline_change_300_ms,idx_baseline_noChange_300_ms];

    id_salient_50ms_0ms = [idx_salient_change_50_ms,idx_salient_noChange_50_ms];
    id_salient_70ms_0ms = [idx_salient_change_70_ms,idx_salient_noChange_70_ms];
    id_salient_90ms_0ms = [idx_salient_change_90_ms,idx_salient_noChange_90_ms];
    id_salient_100ms_0ms = [idx_salient_change_100_ms,idx_salient_noChange_100_ms];
    id_salient_150ms_0ms = [idx_salient_change_150_ms,idx_salient_noChange_150_ms];
    id_salient_200ms_0ms = [idx_salient_change_200_ms,idx_salient_noChange_200_ms];
    id_salient_250ms_0ms = [idx_salient_change_250_ms,idx_salient_noChange_250_ms];
    id_salient_300ms_0ms = [idx_salient_change_300_ms,idx_salient_noChange_300_ms];

    [d50_B ,std_d_50_B ,ci_d_50_B,crit_50_B ,var_d_50_B, HitRate_50_B, FaRate_50_B] = calculateDprime(responses(id_baseline_50ms_0ms), condition(id_baseline_50ms_0ms));
    [d70_B ,std_d_70_B ,ci_d_70_B,crit_70_B ,var_d_70_B, HitRate_70_B, FaRate_70_B] = calculateDprime(responses(id_baseline_70ms_0ms), condition(id_baseline_70ms_0ms));
    [d90_B ,std_d_90_B ,ci_d_90_B,crit_90_B ,var_d_90_B, HitRate_90_B, FaRate_90_B] = calculateDprime(responses(id_baseline_90ms_0ms), condition(id_baseline_90ms_0ms));
    [d100_B ,std_d_100_B ,ci_d_100_B,crit_100_B ,var_d_100_B, HitRate_100_B, FaRate_100_B] = calculateDprime(responses(id_baseline_100ms_0ms), condition(id_baseline_100ms_0ms));
    [d150_B ,std_d_150_B ,ci_d_150_B,crit_150_B ,var_d_150_B, HitRate_150_B, FaRate_150_B] = calculateDprime(responses(id_baseline_150ms_0ms), condition(id_baseline_150ms_0ms));
    [d200_B ,std_d_200_B ,ci_d_200_B,crit_200_B ,var_d_200_B, HitRate_200_B, FaRate_200_B] = calculateDprime(responses(id_baseline_200ms_0ms), condition(id_baseline_200ms_0ms));
    [d250_B ,std_d_250_B ,ci_d_250_B,crit_250_B ,var_d_250_B, HitRate_250_B, FaRate_250_B] = calculateDprime(responses(id_baseline_250ms_0ms), condition(id_baseline_250ms_0ms));
    [d300_B ,std_d_300_B ,ci_d_300_B,crit_300_B ,var_d_300_B, HitRate_300_B, FaRate_300_B] = calculateDprime(responses(id_baseline_300ms_0ms), condition(id_baseline_300ms_0ms));

    [d50_S ,std_d_50_S ,ci_d_50_S,crit_50_S ,var_d_50_S, HitRate_50_S, FaRate_50_S] = calculateDprime(responses(id_salient_50ms_0ms), condition(id_salient_50ms_0ms));
    [d70_S ,std_d_70_S ,ci_d_70_S,crit_70_S ,var_d_70_S, HitRate_70_S, FaRate_70_S] = calculateDprime(responses(id_salient_70ms_0ms), condition(id_salient_70ms_0ms));
    [d90_S ,std_d_90_S ,ci_d_90_S,crit_90_S ,var_d_90_S, HitRate_90_S, FaRate_90_S] = calculateDprime(responses(id_salient_90ms_0ms), condition(id_salient_90ms_0ms));
    [d100_S ,std_d_100_S ,ci_d_100_S,crit_100_S ,var_d_100_S, HitRate_100_S, FaRate_100_S] = calculateDprime(responses(id_salient_100ms_0ms), condition(id_salient_100ms_0ms));
    [d150_S ,std_d_150_S ,ci_d_150_S,crit_150_S ,var_d_150_S, HitRate_150_S, FaRate_150_S] = calculateDprime(responses(id_salient_150ms_0ms), condition(id_salient_150ms_0ms));
    [d200_S ,std_d_200_S ,ci_d_200_S,crit_200_S ,var_d_200_S, HitRate_200_S, FaRate_200_S] = calculateDprime(responses(id_salient_200ms_0ms), condition(id_salient_200ms_0ms));
    [d250_S ,std_d_250_S ,ci_d_250_S,crit_250_S ,var_d_250_S, HitRate_250_S, FaRate_250_S] = calculateDprime(responses(id_salient_250ms_0ms), condition(id_salient_250ms_0ms));
    [d300_S ,std_d_300_S ,ci_d_300_S,crit_300_S ,var_d_300_S, HitRate_300_S, FaRate_300_S] = calculateDprime(responses(id_salient_300ms_0ms), condition(id_salient_300ms_0ms));



    figure()
    plot([1,2,3,4,5,6,7,8],[crit_50_B,crit_70_B,crit_90_B,crit_100_B,crit_150_B,crit_200_B,crit_250_B,crit_300_B],'-o','markerFaceColor','m','Color','m');
    hold on
    plot([1,2,3,4,5,6,7,8],[crit_50_S,crit_70_S,crit_90_S,crit_100_S,crit_150_S,crit_200_S,crit_250_S,crit_300_S],'-o','markerFaceColor','b','Color','b');

    set(gca, 'FontSize', 15, ...
        'XTick', [1:1:8], 'XTickLabel',{'50ms','70ms', '90ms','100ms','150ms','200ms','250ms','300ms'});
    xlim([0 8])
    xlabel('Delay Time')
    ylabel('criterion bias');
    % ylim([0 1]);
    legend('Baseline','Salient')
    
    figure()
    plot([1,2,3,4,5,6,7,8],[d50_B,d70_B,d90_B,d100_B,d150_B,d200_B,d250_B,d300_B],'-o','markerFaceColor','m','Color','m');
    hold on
    plot([1,2,3,4,5,6,7,8],[d50_S,d70_S,d90_S,d100_S,d150_S,d200_S,d250_S,d300_S],'-o','markerFaceColor','b','Color','b');

    set(gca, 'FontSize', 15, ...
        'XTick', [1:1:8], 'XTickLabel',{'50ms','70ms', '90ms','100ms','150ms','200ms','250ms','300ms'});
    xlim([0 8])
    xlabel('Delay Time')
    ylabel('d-prime');
    % ylim([0 1]);
    legend('Baseline','Salient')

    figure()
    plot([1,2,3,4,5,6,7,8],[HitRate_50_B,HitRate_70_B,HitRate_90_B,HitRate_100_B,HitRate_150_B,HitRate_200_B,HitRate_250_B,HitRate_300_B],'-o','markerFaceColor','m','Color','m');
    hold on
    plot([1,2,3,4,5,6,7,8],[HitRate_50_S,HitRate_70_S,HitRate_90_S,HitRate_100_S,HitRate_150_S,HitRate_200_S,HitRate_250_S,HitRate_300_S],'-o','markerFaceColor','b','Color','b');
    set(gca, 'FontSize', 15, ...
        'XTick', [1:1:8], 'XTickLabel',{'50ms','70ms', '90ms','100ms','150ms','200ms','250ms','300ms'});
    xlim([0 8])
    xlabel('Delay Time')
    ylabel('Hit rate');
    legend('Baseline','Salient')

    % [p50,h50,~] = ranksum(HitRate_50_B ,HitRate_50_S);
    % [p70,h70,~] = ranksum(HitRate_70_B ,HitRate_70_S);
    % [p90,h90,~] = ranksum(HitRate_90_B ,HitRate_90_S);
    % [p100,h100,~] = ranksum(HitRate_100_B ,HitRate_100_S);
    % [p200,h200,~] = ranksum(HitRate_200_B ,HitRate_200_S);
    % [p300,h300,~] = ranksum(HitRate_300_B ,HitRate_300_S);

    %% Reaction times plots
    figure()
    plot([1,2,3,4,5,6,7,8],[mean(reactionTimes(id_baseline_50ms_0ms)),mean(reactionTimes(id_baseline_70ms_0ms)),mean(reactionTimes(id_baseline_90ms_0ms)),...
                            mean(reactionTimes(id_baseline_100ms_0ms)),mean(reactionTimes(id_baseline_150ms_0ms)),mean(reactionTimes(id_baseline_200ms_0ms)),...
                            mean(reactionTimes(id_baseline_250ms_0ms)),mean(reactionTimes(id_baseline_300ms_0ms))],'-o','markerFaceColor','m','Color','m');
    hold on
    plot([1,2,3,4,5,6,7,8],[mean(reactionTimes(id_salient_50ms_0ms)),mean(reactionTimes(id_salient_70ms_0ms)),mean(reactionTimes(id_salient_90ms_0ms)),...
                            mean(reactionTimes(id_salient_100ms_0ms)),mean(reactionTimes(id_salient_150ms_0ms)),mean(reactionTimes(id_salient_200ms_0ms)),...
                            mean(reactionTimes(id_salient_250ms_0ms)),mean(reactionTimes(id_salient_300ms_0ms))],'-o','markerFaceColor','b','Color','b');

    set(gca, 'FontSize', 15, ...
        'XTick', [1:1:8], 'XTickLabel',{'50ms','70ms', '90ms','100ms','150ms','200ms','250ms','300ms'});
    xlim([0 8])
    xlabel('Delay Time')
    ylabel('Reaction times');
    % ylim([0 1]);
    legend('Baseline','Salient')


    %% Probability of occurence plots
    figure()
    [no,xo] = hist(condition_labels_original, [0,1,2,3]);
    bar([1:1:4],no/length(condition_labels_original))
    title('Probability of each condition occurance')
    set(gca, 'FontSize', 15, ...
        'XTick', [1:1:4], 'XTickLabel',{'Baseline','B No change', 'Salient','S No change'});
    xlim([0 5])
    xlabel('condition')
    ylabel('Probability');
    box off
    %% Probablity of all delaytime choices occuring through the entire exp
    figure()
    [no,xo] = hist(delayTimeChoices(delayTimeChoices~=0), [50,70,90,100,150,200,250,300]);
    bar([1:1:8],no/length(delayTimeChoices))
    title('Probability of each delay time occuring')
    set(gca, 'FontSize', 15, ...
        'XTick', [1:1:8], 'XTickLabel',{'50ms','70ms', '90ms','100ms','150ms','200ms','250ms','300ms'});
    xlim([0 9])
    xlabel('Delay Times')
    ylabel('Probability');
    box off
    %% Probablity of all delaytime choices occuring through each condition
    figure();
    dTimes_B = [(length(idx_baseline_change_50_ms)/length(delayTimeChoicesPicked))*100,(length(idx_baseline_noChange_50_ms)/length(delayTimeChoicesPicked))*100;...
                (length(idx_baseline_change_70_ms)/length(delayTimeChoicesPicked))*100,(length(idx_baseline_noChange_70_ms)/length(delayTimeChoicesPicked))*100;...
                (length(idx_baseline_change_90_ms)/length(delayTimeChoicesPicked))*100,(length(idx_baseline_noChange_90_ms)/length(delayTimeChoicesPicked))*100;...
                (length(idx_baseline_change_100_ms)/length(delayTimeChoicesPicked))*100,(length(idx_baseline_noChange_100_ms)/length(delayTimeChoicesPicked))*100;...
                (length(idx_baseline_change_150_ms)/length(delayTimeChoicesPicked))*100,(length(idx_baseline_noChange_150_ms)/length(delayTimeChoicesPicked))*100;...
                (length(idx_baseline_change_200_ms)/length(delayTimeChoicesPicked))*100,(length(idx_baseline_noChange_200_ms)/length(delayTimeChoicesPicked))*100;...
                (length(idx_baseline_change_250_ms)/length(delayTimeChoicesPicked))*100,(length(idx_baseline_noChange_250_ms)/length(delayTimeChoicesPicked))*100;...
                (length(idx_baseline_change_300_ms)/length(delayTimeChoicesPicked))*100,(length(idx_baseline_noChange_300_ms)/length(delayTimeChoicesPicked))*100];
            
    bar([1:1:8],dTimes_B)
    title('Probability of each delay time occuring  - Baseline')
    set(gca, 'FontSize', 15, ...
        'XTick', [1:1:8], 'XTickLabel',{'50ms','70ms', '90ms','100ms','150ms','200ms','250ms','300ms'});
    xlim([0 9])
    xlabel('Delay Times')
    ylabel('Probability');
    legend('change', 'no change')
    box off
    
    figure();
    dTimes_S = [(length(idx_salient_change_50_ms)/length(delayTimeChoicesPicked))*100,(length(idx_salient_noChange_50_ms)/length(delayTimeChoicesPicked))*100;...
                (length(idx_salient_change_70_ms)/length(delayTimeChoicesPicked))*100,(length(idx_salient_noChange_70_ms)/length(delayTimeChoicesPicked))*100;...
                (length(idx_salient_change_90_ms)/length(delayTimeChoicesPicked))*100,(length(idx_salient_noChange_90_ms)/length(delayTimeChoicesPicked))*100;...
                (length(idx_salient_change_100_ms)/length(delayTimeChoicesPicked))*100,(length(idx_salient_noChange_100_ms)/length(delayTimeChoicesPicked))*100;...
                (length(idx_salient_change_150_ms)/length(delayTimeChoicesPicked))*100,(length(idx_salient_noChange_150_ms)/length(delayTimeChoicesPicked))*100;...
                (length(idx_salient_change_200_ms)/length(delayTimeChoicesPicked))*100,(length(idx_salient_noChange_200_ms)/length(delayTimeChoicesPicked))*100;...
                (length(idx_salient_change_250_ms)/length(delayTimeChoicesPicked))*100,(length(idx_salient_noChange_250_ms)/length(delayTimeChoicesPicked))*100;...
                (length(idx_salient_change_300_ms)/length(delayTimeChoicesPicked))*100,(length(idx_salient_noChange_300_ms)/length(delayTimeChoicesPicked))*100];
            
    bar([1:1:8],dTimes_S)
    title('Probability of each delay time occuring  - Salient')
    set(gca, 'FontSize', 15, ...
        'XTick', [1:1:8], 'XTickLabel',{'50ms','70ms', '90ms','100ms','150ms','200ms','250ms','300ms'});
    xlim([0 9])
    xlabel('Delay Times')
    ylabel('Probability');
    legend('change', 'no change')
    box off
    
    %% Prop correct in each condition plots
    propCorrect_B = calcPerformanceCorrect(responses, idx_baseline);
    propCorrect_BC = calcPerformanceCorrect(responses, idx_baseline_catch);
    propCorrect_S = calcPerformanceCorrect(responses, idx_salient);
    propCorrect_SC =calcPerformanceCorrect(responses, idx_salient_catch);

    ci_perf_B = ciPerf(responses(idx_baseline));
    ci_perf_BC = ciPerf(responses(idx_baseline_catch));
    ci_perf_S = ciPerf(responses(idx_salient));
    ci_perf_SC = ciPerf(responses(idx_salient_catch));

    figure();
    errorbar([propCorrect_B propCorrect_BC propCorrect_S propCorrect_SC], [ci_perf_B ci_perf_BC ci_perf_S ci_perf_SC],...
                'o', 'LineStyle', '-','LineWidth',2);

    % plot([1,2,3,4],[propCorrect_B,propCorrect_BC,propCorrect_S,propCorrect_SC]);
    set(gca, 'FontSize', 15, ...
        'XTick', [1:1:4], 'XTickLabel',{'B, change','B, no change', 'S, change','S, no change'});
    xlim([0 4])
    xlabel('condition')
    ylabel('prop correct');
%     title('Performance in each condition')
    %% Prop correct in each condition plots at diff time delays
    pCorr_change_B_50ms = calcPerformanceCorrect(responses, idx_baseline_change_50_ms);
    pCorr_change_B_70ms = calcPerformanceCorrect(responses, idx_baseline_change_70_ms);
    pCorr_change_B_90ms = calcPerformanceCorrect(responses, idx_baseline_change_90_ms);
    pCorr_change_B_100ms = calcPerformanceCorrect(responses, idx_baseline_change_100_ms);
    pCorr_change_B_150ms = calcPerformanceCorrect(responses, idx_baseline_change_150_ms);
    pCorr_change_B_200ms = calcPerformanceCorrect(responses, idx_baseline_change_200_ms);
    pCorr_change_B_250ms = calcPerformanceCorrect(responses, idx_baseline_change_250_ms);
    pCorr_change_B_300ms = calcPerformanceCorrect(responses, idx_baseline_change_300_ms);
    
    pCorr_noChange_B_50ms = calcPerformanceCorrect(responses, idx_baseline_noChange_50_ms);
    pCorr_noChange_B_70ms = calcPerformanceCorrect(responses, idx_baseline_noChange_70_ms);
    pCorr_noChange_B_90ms = calcPerformanceCorrect(responses, idx_baseline_noChange_90_ms);
    pCorr_noChange_B_100ms = calcPerformanceCorrect(responses, idx_baseline_noChange_100_ms);
    pCorr_noChange_B_150ms = calcPerformanceCorrect(responses, idx_baseline_noChange_150_ms);
    pCorr_noChange_B_200ms = calcPerformanceCorrect(responses, idx_baseline_noChange_200_ms);
    pCorr_noChange_B_250ms = calcPerformanceCorrect(responses, idx_baseline_noChange_250_ms);
    pCorr_noChange_B_300ms = calcPerformanceCorrect(responses, idx_baseline_noChange_300_ms);

    ci_perf_change_B_50ms = ciPerf(responses(idx_baseline_change_50_ms));
    ci_perf_change_B_70ms = ciPerf(responses(idx_baseline_change_70_ms));
    ci_perf_change_B_90ms = ciPerf(responses(idx_baseline_change_90_ms));
    ci_perf_change_B_100ms = ciPerf(responses(idx_baseline_change_100_ms));
    ci_perf_change_B_150ms = ciPerf(responses(idx_baseline_change_150_ms));
    ci_perf_change_B_200ms = ciPerf(responses(idx_baseline_change_200_ms));
    ci_perf_change_B_250ms = ciPerf(responses(idx_baseline_change_250_ms));
    ci_perf_change_B_300ms = ciPerf(responses(idx_baseline_change_300_ms));
    
    ci_perf_noChange_B_50ms = ciPerf(responses(idx_baseline_noChange_50_ms));
    ci_perf_noChange_B_70ms = ciPerf(responses(idx_baseline_noChange_70_ms));
    ci_perf_noChange_B_90ms = ciPerf(responses(idx_baseline_noChange_90_ms));
    ci_perf_noChange_B_100ms = ciPerf(responses(idx_baseline_noChange_100_ms));
    ci_perf_noChange_B_150ms = ciPerf(responses(idx_baseline_noChange_150_ms));
    ci_perf_noChange_B_200ms = ciPerf(responses(idx_baseline_noChange_200_ms));
    ci_perf_noChange_B_250ms = ciPerf(responses(idx_baseline_noChange_250_ms));
    ci_perf_noChange_B_300ms = ciPerf(responses(idx_baseline_noChange_300_ms));

    
    pCorr_change_S_50ms = calcPerformanceCorrect(responses, idx_salient_change_50_ms);
    pCorr_change_S_70ms = calcPerformanceCorrect(responses, idx_salient_change_70_ms);
    pCorr_change_S_90ms = calcPerformanceCorrect(responses, idx_salient_change_90_ms);
    pCorr_change_S_100ms = calcPerformanceCorrect(responses, idx_salient_change_100_ms);
    pCorr_change_S_150ms = calcPerformanceCorrect(responses, idx_salient_change_150_ms);
    pCorr_change_S_200ms = calcPerformanceCorrect(responses, idx_salient_change_200_ms);
    pCorr_change_S_250ms = calcPerformanceCorrect(responses, idx_salient_change_250_ms);
    pCorr_change_S_300ms = calcPerformanceCorrect(responses, idx_salient_change_300_ms);
    
    pCorr_noChange_S_50ms = calcPerformanceCorrect(responses, idx_salient_noChange_50_ms);
    pCorr_noChange_S_70ms = calcPerformanceCorrect(responses, idx_salient_noChange_70_ms);
    pCorr_noChange_S_90ms = calcPerformanceCorrect(responses, idx_salient_noChange_90_ms);
    pCorr_noChange_S_100ms = calcPerformanceCorrect(responses, idx_salient_noChange_100_ms);
    pCorr_noChange_S_150ms = calcPerformanceCorrect(responses, idx_salient_noChange_150_ms);
    pCorr_noChange_S_200ms = calcPerformanceCorrect(responses, idx_salient_noChange_200_ms);
    pCorr_noChange_S_250ms = calcPerformanceCorrect(responses, idx_salient_noChange_250_ms);
    pCorr_noChange_S_300ms = calcPerformanceCorrect(responses, idx_salient_noChange_300_ms);

    ci_perf_change_S_50ms = ciPerf(responses(idx_salient_change_50_ms));
    ci_perf_change_S_70ms = ciPerf(responses(idx_salient_change_70_ms));
    ci_perf_change_S_90ms = ciPerf(responses(idx_salient_change_90_ms));
    ci_perf_change_S_100ms = ciPerf(responses(idx_salient_change_100_ms));
    ci_perf_change_S_150ms = ciPerf(responses(idx_salient_change_150_ms));
    ci_perf_change_S_200ms = ciPerf(responses(idx_salient_change_200_ms));
    ci_perf_change_S_250ms = ciPerf(responses(idx_salient_change_250_ms));
    ci_perf_change_S_300ms = ciPerf(responses(idx_salient_change_300_ms));
    
    ci_perf_noChange_S_50ms = ciPerf(responses(idx_salient_noChange_50_ms));
    ci_perf_noChange_S_70ms = ciPerf(responses(idx_salient_noChange_70_ms));
    ci_perf_noChange_S_90ms = ciPerf(responses(idx_salient_noChange_90_ms));
    ci_perf_noChange_S_100ms = ciPerf(responses(idx_salient_noChange_100_ms));
    ci_perf_noChange_S_150ms = ciPerf(responses(idx_salient_noChange_150_ms));
    ci_perf_noChange_S_200ms = ciPerf(responses(idx_salient_noChange_200_ms));
    ci_perf_noChange_S_250ms = ciPerf(responses(idx_salient_noChange_250_ms));
    ci_perf_noChange_S_300ms = ciPerf(responses(idx_salient_noChange_300_ms));

    
    figure();
    errorbar([pCorr_change_B_50ms pCorr_noChange_B_50ms,...
              pCorr_change_B_70ms pCorr_noChange_B_70ms,...
              pCorr_change_B_90ms pCorr_noChange_B_90ms,...
              pCorr_change_B_100ms pCorr_noChange_B_100ms,...
              pCorr_change_B_150ms pCorr_noChange_B_150ms,...
              pCorr_change_B_200ms pCorr_noChange_B_200ms,...
              pCorr_change_B_250ms pCorr_noChange_B_250ms,...
              pCorr_change_B_300ms pCorr_noChange_B_300ms],...
              [ci_perf_change_B_50ms ci_perf_noChange_B_50ms,...
               ci_perf_change_B_70ms ci_perf_noChange_B_70ms,...
               ci_perf_change_B_90ms ci_perf_noChange_B_90ms,...
               ci_perf_change_B_100ms ci_perf_noChange_B_100ms,...
               ci_perf_change_B_150ms ci_perf_noChange_B_150ms,...
               ci_perf_change_B_200ms ci_perf_noChange_B_200ms,...
               ci_perf_change_B_250ms ci_perf_noChange_B_250ms,...
               ci_perf_change_B_300ms ci_perf_noChange_B_300ms],...
                'o', 'LineStyle', '-','LineWidth',2);
            
    hold on
    errorbar([pCorr_change_S_50ms pCorr_noChange_S_50ms,...
              pCorr_change_S_70ms pCorr_noChange_S_70ms,...
              pCorr_change_S_90ms pCorr_noChange_S_90ms,...
              pCorr_change_S_100ms pCorr_noChange_S_100ms,...
              pCorr_change_S_150ms pCorr_noChange_S_150ms,...
              pCorr_change_S_200ms pCorr_noChange_S_200ms,...
              pCorr_change_S_250ms pCorr_noChange_S_250ms,...
              pCorr_change_S_300ms pCorr_noChange_S_300ms],...
              [ci_perf_change_S_50ms ci_perf_noChange_S_50ms,...
               ci_perf_change_S_70ms ci_perf_noChange_S_70ms,...
               ci_perf_change_S_90ms ci_perf_noChange_S_90ms,...
               ci_perf_change_S_100ms ci_perf_noChange_S_100ms,...
               ci_perf_change_S_150ms ci_perf_noChange_S_150ms,...
               ci_perf_change_S_200ms ci_perf_noChange_S_200ms,...
               ci_perf_change_S_250ms ci_perf_noChange_S_250ms,...
               ci_perf_change_S_300ms ci_perf_noChange_S_300ms],...
                'o', 'LineStyle', '-','LineWidth',2);
    set(gca, 'FontSize', 15, ...
        'XTick', [1:1:8], 'XTickLabel',{'50ms, change','50ms, no change',...
                                        '70ms, change','70ms, no change',...
                                        '90ms, change','90ms, no change',...
                                        '100ms, change','100ms, no change',...
                                        '150ms, change','150ms, no change',...
                                        '200ms, change','200ms, no change',...
                                        '250ms, change','250ms, no change',...
                                        '300ms, change','300ms, no change'});
    
    
    
    xlim([0 8])
    xlabel('condition')
    ylabel('prop correct');
    %% Prop correct @ each time slot, BASELINE
    cJet = jet(length(all_delayTimeChoicesPicked));
    % propCorrect_B_0ms = calcPerformanceCorrect(responses, id_baseline_50ms_0_ms);
    propCorrect_B_50ms = calcPerformanceCorrect(responses, id_baseline_50ms_0ms);
    propCorrect_B_70ms = calcPerformanceCorrect(responses, id_baseline_70ms_0ms);
    propCorrect_B_90ms =calcPerformanceCorrect(responses, id_baseline_90ms_0ms);
    propCorrect_B_100ms =calcPerformanceCorrect(responses, id_baseline_100ms_0ms);
    propCorrect_B_150ms =calcPerformanceCorrect(responses, id_baseline_150ms_0ms);
    propCorrect_B_200ms =calcPerformanceCorrect(responses, id_baseline_200ms_0ms);
    propCorrect_B_250ms =calcPerformanceCorrect(responses, id_baseline_250ms_0ms);
    propCorrect_B_300ms =calcPerformanceCorrect(responses, id_baseline_300ms_0ms);

    % ci_perf_B_0ms = ciPerf(responses(id_baseline_50ms_0ms));
    ci_perf_B_50ms = ciPerf(responses(id_baseline_50ms_0ms));
    ci_perf_B_70ms = ciPerf(responses(id_baseline_70ms_0ms));
    ci_perf_B_90ms = ciPerf(responses(id_baseline_90ms_0ms));
    ci_perf_B_100ms = ciPerf(responses(id_baseline_100ms_0ms));
    ci_perf_B_150ms = ciPerf(responses(id_baseline_150ms_0ms));
    ci_perf_B_200ms = ciPerf(responses(id_baseline_200ms_0ms));
    ci_perf_B_250ms = ciPerf(responses(id_baseline_250ms_0ms));
    ci_perf_B_300ms = ciPerf(responses(id_baseline_300ms_0ms));


    % propCorrect_S_0ms = calcPerformanceCorrect(responses, idx_salient_0_ms);
    propCorrect_S_50ms = calcPerformanceCorrect(responses, id_salient_50ms_0ms);
    propCorrect_S_70ms = calcPerformanceCorrect(responses, id_salient_70ms_0ms);
    propCorrect_S_90ms =calcPerformanceCorrect(responses, id_salient_90ms_0ms);
    propCorrect_S_100ms =calcPerformanceCorrect(responses, id_salient_100ms_0ms);
    propCorrect_S_150ms =calcPerformanceCorrect(responses, id_salient_150ms_0ms);
    propCorrect_S_200ms =calcPerformanceCorrect(responses, id_salient_200ms_0ms);
    propCorrect_S_250ms =calcPerformanceCorrect(responses, id_salient_250ms_0ms);
    propCorrect_S_300ms =calcPerformanceCorrect(responses, id_salient_300ms_0ms);

    % ci_perf_S_0ms = ciPerf(responses(id_salient_50ms_0ms));
    ci_perf_S_50ms = ciPerf(responses(id_salient_50ms_0ms));
    ci_perf_S_70ms = ciPerf(responses(id_salient_70ms_0ms));
    ci_perf_S_90ms = ciPerf(responses(id_salient_90ms_0ms));
    ci_perf_S_100ms = ciPerf(responses(id_salient_100ms_0ms));
    ci_perf_S_150ms = ciPerf(responses(id_salient_150ms_0ms));
    ci_perf_S_200ms = ciPerf(responses(id_salient_200ms_0ms));
    ci_perf_S_250ms = ciPerf(responses(id_salient_250ms_0ms));
    ci_perf_S_300ms = ciPerf(responses(id_salient_300ms_0ms));


    [h_50,p_50] = ttest2(responses(id_baseline_50ms_0ms),responses(id_salient_50ms_0ms));
    [h_70,p_70] = ttest2(responses(id_baseline_70ms_0ms),responses(id_salient_70ms_0ms));
    [h_90,p_90] = ttest2(responses(id_baseline_90ms_0ms),responses(id_salient_90ms_0ms));
    [h_100,p_100] = ttest2(responses(id_baseline_100ms_0ms),responses(id_salient_100ms_0ms));
    [h_150,p_150] = ttest2(responses(id_baseline_150ms_0ms),responses(id_salient_150ms_0ms));
    [h_200,p_200] = ttest2(responses(id_baseline_200ms_0ms),responses(id_salient_200ms_0ms));
    [h_250,p_250] = ttest2(responses(id_baseline_250ms_0ms),responses(id_salient_250ms_0ms));
    [h_300,p_300] = ttest2(responses(id_baseline_300ms_0ms),responses(id_salient_300ms_0ms));

    [p50,h50,~] = ranksum(propCorrect_B_50ms ,propCorrect_S_50ms);
    [p70,h70,~] = ranksum(propCorrect_B_70ms ,propCorrect_S_70ms);
    [p90,h90,~] = ranksum(propCorrect_B_90ms ,propCorrect_S_90ms);
    [p100,h100,~] = ranksum(propCorrect_B_100ms,propCorrect_S_100ms);
    [p150,h150,~] = ranksum(propCorrect_B_150ms,propCorrect_S_150ms);
    [p200,h200,~] = ranksum(propCorrect_B_200ms ,propCorrect_S_200ms);
    [p250,h250,~] = ranksum(propCorrect_B_250ms ,propCorrect_S_250ms);
    [p300,h300,~] = ranksum(propCorrect_B_300ms ,propCorrect_S_300ms);
    
    figure();

    errorbar([propCorrect_B_50ms propCorrect_B_70ms...
              propCorrect_B_90ms propCorrect_B_100ms,...
              propCorrect_B_150ms propCorrect_B_200ms,...
              propCorrect_B_250ms propCorrect_B_300ms],...
              [ ci_perf_B_50ms ci_perf_B_70ms ci_perf_B_90ms ci_perf_B_100ms,...
                ci_perf_B_150ms ci_perf_B_200ms ci_perf_B_250ms ci_perf_B_300ms],...
                'o', 'LineStyle', '-','LineWidth',2,'Color','m');
   
    hold on
    errorbar([ propCorrect_S_50ms propCorrect_S_70ms...
              propCorrect_S_90ms propCorrect_S_100ms,...
              propCorrect_S_150ms propCorrect_S_200ms,...
             propCorrect_S_250ms  propCorrect_S_300ms],...
              [ ci_perf_S_50ms ci_perf_S_70ms ci_perf_S_90ms ci_perf_S_100ms,...
                ci_perf_S_150ms ci_perf_S_200ms ci_perf_S_250ms ci_perf_S_300ms],...
              'o', 'LineStyle', '-','LineWidth',2,'Color','b');
    

    % plot([1,2,3,4],[propCorrect_B,propCorrect_BC,propCorrect_S,propCorrect_SC]);
    set(gca, 'FontSize', 15, ...
        'XTick', [1:1:8], 'XTickLabel',{ '50ms', '70ms','90ms', '100ms', '150ms', '200ms', '250ms', '300ms'});
    xlim([0 8])
    xlabel('condition')
    ylabel('prop correct');
    legend('Baseline', 'Salient');
%     title('Performance at each delay time')

end

function propCorrect = calcPerformanceCorrect(responseVector, totalVectorIds)
    
    propCorrect = length(find(responseVector(totalVectorIds) == 1))/length(totalVectorIds);
    
end

function ci_perf = ciPerf(data)
n = length(data);
p = sum(data)/length(data);
ci_perf = sqrt(p.*(1 - p)./(n));
end    


