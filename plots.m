%%-------------------Prop correct at smoothed temporal bin-----------------
figure()
plt = plot(TimeBins_B,PC_B,'LineStyle','-','Color',[0.6350 0.0780 0.1840])
pltHandles = plt;
hold on
shadedErrorBar(TimeBins_B,PC_B,PC_se_B,{'-r','color',[0.6350 0.0780 0.1840]},1)
hold on
plt = plot(TimeBins_S,PC_S,'LineStyle','-','Color',	[0, 0.4470, 0.7410])
pltHandles(2) = plt
hold on
shadedErrorBar(TimeBins_S,PC_S,PC_se_S,{'-r','color',[0, 0.4470, 0.7410]},1)
xlabel('Delay Times (ms)')
ylabel('Proportion Correct')
set(gca,'fontsize',27,'FontWeight','Bold')
% ylim([0.5 1])
% yline(0.5,'--k',{'Chance'})
legend(pltHandles,{'Baseline','Salient'})
legend box off
set(gca,'fontsize',27,'FontWeight','Bold')

tb_B = TimeBins_B(~isnan(TimeBins_B));
pc_B = PC_B(~isnan(PC_B));
se_B = PC_se_B(~isnan(PC_B));

tb_S = TimeBins_S(~isnan(TimeBins_S));
pc_S = PC_S(~isnan(PC_S));
se_S = PC_se_S(~isnan(PC_S));


figure()
plt = plot(tb_B,detrend(pc_B),'LineStyle','-','Color',[0.6350 0.0780 0.1840])
pltHandles = plt;
hold on
shadedErrorBar(tb_B,detrend(pc_B),se_B,{'-r','color',[0.6350 0.0780 0.1840]},1)
hold on
plt = plot(tb_S,detrend(pc_S),'LineStyle','-','Color',	[0, 0.4470, 0.7410])
pltHandles(2) = plt
hold on
shadedErrorBar(tb_S,detrend(pc_S),se_S,{'-r','color',[0, 0.4470, 0.7410]},1)
xlabel('Delay Times')
ylabel('Proportion Correct')
set(gca,'fontsize',27,'FontWeight','Bold')
% ylim([0.5 1])
% yline(0.5,'--k',{'Chance'})
legend(pltHandles,{'Baseline','Salient'})
legend box off
set(gca,'fontsize',27,'FontWeight','Bold')

time_vec_B = [0.001:0.001:length(pc_B)/1000];
sineFit(time_vec_B,detrend(pc_B),1)

time_vec_S = [0.001:0.001:length(pc_S)/1000];
sineFit(time_vec_S,detrend(pc_S),1)

%% Shaded RTs
figure()
plt = plot(TimeBins_B,RespTime_B,'LineStyle','-','Color',[0.6350 0.0780 0.1840])
pltHandles = plt;
hold on
shadedErrorBar(TimeBins_B,RespTime_B,RespTime_se_B,{'-r','color',[0.6350 0.0780 0.1840]},1)
hold on
plt = plot(TimeBins_S,RespTime_S,'LineStyle','-','Color',	[0, 0.4470, 0.7410])
pltHandles(2) = plt
hold on
shadedErrorBar(TimeBins_S,RespTime_S,RespTime_se_S,{'-r','color',[0, 0.4470, 0.7410]},1)
xlabel('Delay Times')
ylabel('Reaction Times')
set(gca,'fontsize',27,'FontWeight','Bold')
% ylim([0.5 1])
% yline(0.5,'--k',{'Chance'})
legend(pltHandles,{'Baseline','Salient'})
legend box off
set(gca,'fontsize',27,'FontWeight','Bold')

                        
 %% difference in performance plots
 diff_perf = PC_B - PC_S;
 figure()
 plot(TimeBins_S,diff_perf,'r');
 xlabel('Time after Fixation onset')
 ylabel('Difference in Performance')
 yline(0,'--k')
 ylim([-0.3,0.3])
 set(gca,'fontsize',27,'FontWeight','Bold')
%% prop correct plot with new binning
propCorrect_B_0_150ms =calcPerformanceCorrect(responses, id_baseline_ST_0_150ms);
propCorrect_B_150_300ms =calcPerformanceCorrect(responses, id_baseline_ST_150ms_300ms);
propCorrect_B_300_450ms =calcPerformanceCorrect(responses, id_baseline_ST_300ms_450ms);

ci_perf_B_0_150ms = ciPerf(responses(id_baseline_ST_0_150ms));
ci_perf_B_150_300ms = ciPerf(responses(id_baseline_ST_150ms_300ms));
ci_perf_B_300_450ms = ciPerf(responses(id_baseline_ST_300ms_450ms));

propCorrect_S_0_150ms  = calcPerformanceCorrect(responses, id_salient_ST_0_150ms);
propCorrect_S_150_300ms  = calcPerformanceCorrect(responses, id_salient_ST_150ms_300ms);
propCorrect_S_300_450ms = calcPerformanceCorrect(responses, id_salient_ST_300ms_450ms);

ci_perf_S_0_150ms = ciPerf(responses(id_salient_ST_0_150ms));
ci_perf_S_150_300ms = ciPerf(responses(id_salient_ST_150ms_300ms));
ci_perf_S_300_450ms = ciPerf(responses(id_salient_ST_300ms_450ms));

[E1, E2, z,hh_0_150, p_0_150]  = Z_Test(length(find(responses(id_baseline_ST_0_150ms) == 1)),length(responses(id_baseline_ST_0_150ms)),...
                           length(find(responses(id_salient_ST_0_150ms)==1)),length(responses(id_salient_ST_0_150ms)),0.05,...
                            'two sided',1);
[E1, E2, z,hh_150_300, p_150_300]  = Z_Test(length(find(responses(id_baseline_ST_150ms_300ms) == 1)),length(responses(id_baseline_ST_150ms_300ms)),...
                           length(find(responses(id_salient_ST_150ms_300ms)==1)),length(responses(id_salient_ST_150ms_300ms)),0.05,...
                            'two sided',1);      
[E1, E2, z,hh_300_450, p_300_450]  = Z_Test(length(find(responses(id_baseline_ST_300ms_450ms) == 1)),length(responses(id_baseline_ST_300ms_450ms)),...
                           length(find(responses(id_salient_ST_300ms_450ms)==1)),length(responses(id_salient_ST_300ms_450ms)),0.05,...
                            'two sided',1); 
                        
[E1, E2, z,hh_0_150_300_b, p_0_150_300_b]  = Z_Test(length(find(responses(id_baseline_ST_0_150ms) == 1)),length(responses(id_baseline_ST_0_150ms)),...
                           length(find(responses(id_baseline_ST_150ms_300ms)==1)),length(responses(id_baseline_ST_150ms_300ms)),0.05,...
                            'two sided',1);
[E1, E2, z,hh_0_150_300_s, p_0_150_300_s]  = Z_Test(length(find(responses(id_salient_ST_0_150ms) == 1)),length(responses(id_salient_ST_0_150ms)),...
                           length(find(responses(id_salient_ST_150ms_300ms)==1)),length(responses(id_salient_ST_150ms_300ms)),0.05,...
                            'two sided',1);
[E1, E2, z,hh_150_300_450_b, p_150_300_450_b]  = Z_Test(length(find(responses(id_baseline_ST_150ms_300ms) == 1)),length(responses(id_baseline_ST_150ms_300ms)),...
                           length(find(responses(id_baseline_ST_300ms_450ms) == 1)),length(responses(id_baseline_ST_300ms_450ms)),0.05,...
                            'two sided',1);      
[E1, E2, z,hh_150_300_450_s, p_150_300_450_s]  = Z_Test(length(find(responses(id_salient_ST_150ms_300ms)==1)),length(responses(id_salient_ST_150ms_300ms)),...
                           length(find(responses(id_salient_ST_300ms_450ms)==1)),length(responses(id_salient_ST_300ms_450ms)),0.05,...
                            'two sided',1); 
[E1, E2, z,hh_0_150_300_s, p_0_150_450_s]  = Z_Test(length(find(responses(id_salient_ST_0_150ms) == 1)),length(responses(id_salient_ST_0_150ms)),...
                            length(find(responses(id_salient_ST_300ms_450ms) == 1)),length(responses(id_salient_ST_300ms_450ms)),0.05,...
                            'two sided',1);
[E1, E2, z,hh_0_150_300_b, p_0_150_450_b]  = Z_Test(length(find(responses(id_baseline_ST_0_150ms) == 1)),length(responses(id_baseline_ST_0_150ms)),...
                            length(find(responses(id_baseline_ST_300ms_450ms) == 1)),length(responses(id_baseline_ST_300ms_450ms)),0.05,...
                            'two sided',1);

figure();

errorbar([propCorrect_B_0_150ms  propCorrect_B_150_300ms  propCorrect_B_300_450ms],...
    [ci_perf_B_0_150ms ci_perf_B_150_300ms  ci_perf_B_300_450ms],...
    'o', 'LineStyle', '-','LineWidth',2,'Color','m');

hold on
errorbar([propCorrect_S_0_150ms propCorrect_S_150_300ms propCorrect_S_300_450ms],...
    [ci_perf_S_0_150ms ci_perf_S_150_300ms ci_perf_S_300_450ms],...
    'o', 'LineStyle', '-','LineWidth',2,'Color','b');

text0_B = strcat('n=',string(length(id_baseline_ST_0_150ms)));
text(1.1,propCorrect_B_0_150ms ,text0_B,'color','r');

text1_B = strcat('n=',string(length(id_baseline_ST_150ms_300ms)));
text(2.1,propCorrect_B_150_300ms ,text1_B,'color','r');

text2_B = strcat('n=',string(length(id_baseline_ST_300ms_450ms)));
text(3.1,propCorrect_B_300_450ms ,text2_B,'color','r');

text0_S = strcat('n=',string(length(id_salient_ST_0_150ms)));
text(1.1,propCorrect_S_0_150ms ,text0_S,'color','r');

text1_S = strcat('n=',string(length(id_salient_ST_150ms_300ms)));
text(2.1,propCorrect_S_150_300ms ,text1_S,'color','r');

text2_S = strcat('n=',string(length(id_salient_ST_300ms_450ms)));
text(3.1,propCorrect_S_300_450ms ,text2_S,'color','r');

set(gca, 'FontSize', 15, ...
    'XTick', [1:1:3], 'XTickLabel',{'0-150','150-300','300-450'});
xlim([0 4])
ylim([0.4 1])
legend box off
xlabel('Delay Times (ms)')
ylabel('Proportion Correct')
set(gca,'fontsize',27,'FontWeight','Bold')
legend('Baseline', 'Salient');
legend box off

%% prop correct faces with specific questions

tot_perf_faces_B = calcPerformanceCorrect(allStoreCorr,timingBaseline);
tot_perf_faces_S = calcPerformanceCorrect(allStoreCorr,timingSalient);

ci_perf_tot_faces_B = ciPerf(allStoreCorr(timingBaseline));
ci_perf_tot_faces_S = ciPerf(allStoreCorr(timingSalient));

figure();

errorbar([tot_perf_faces_B,tot_perf_faces_S],...
    [ci_perf_tot_faces_B,ci_perf_tot_faces_S],...
    'o', 'LineStyle', '-','LineWidth',2,'Color','k');


set(gca, 'FontSize', 15, ...
    'XTick', [1:1:2], 'XTickLabel',{'baseline','salient'});
xlim([0 3])
% ylim([0.4 1])
legend box off
xlabel('condition')
ylabel('Proportion Correct for faces')
set(gca,'fontsize',27,'FontWeight','Bold')
legend off
% legend('Baseline', 'Salient');

propC_gender_B = calcPerformanceCorrect(allStoreCorr,idx_gender_B);
ci_perf_gender_B = ciPerf(allStoreCorr(idx_gender_B));

propC_gender_S = calcPerformanceCorrect(allStoreCorr,idx_gender_S);
ci_perf_gender_S = ciPerf(allStoreCorr(idx_gender_S));

propC_dir_B = calcPerformanceCorrect(allStoreCorr,idx_dir_B);
ci_perf_dir_B = ciPerf(allStoreCorr(idx_dir_B));

propC_dir_S = calcPerformanceCorrect(allStoreCorr,idx_dir_S);
ci_perf_dir_S = ciPerf(allStoreCorr(idx_dir_S));

propC_emotion_B = calcPerformanceCorrect(allStoreCorr,idx_emotion_B);
ci_perf_emotion_B = ciPerf(allStoreCorr(idx_emotion_B));

propC_emotion_S = calcPerformanceCorrect(allStoreCorr,idx_emotion_S);
ci_perf_emotion_S = ciPerf(allStoreCorr(idx_emotion_S));

figure();

errorbar([propC_gender_B propC_dir_B propC_emotion_B],...
    [ci_perf_gender_B ci_perf_dir_B ci_perf_emotion_B],...
    'o', 'LineStyle', '-','LineWidth',2,'Color','m');

hold on
errorbar([propC_gender_S propC_dir_S propC_emotion_S],...
    [ci_perf_gender_S ci_perf_dir_S ci_perf_emotion_S],...
    'o', 'LineStyle', '-','LineWidth',2,'Color','b');

set(gca, 'FontSize', 15, ...
    'XTick', [1:1:3], 'XTickLabel',{'gender','direction','emotion'});
xlim([0 4])
% ylim([0.4 1])
legend box off
xlabel('Face Question')
ylabel('Proportion Correct for faces')
set(gca,'fontsize',27,'FontWeight','Bold')
legend('Baseline', 'Salient');
legend box off

%% prop correct plot with face Resp wider binning
propCorrect_face_B_0_150ms =calcPerformanceCorrect(allStoreCorr, id_baseline_ST_0_150ms);
propCorrect_face_B_150_300ms =calcPerformanceCorrect(allStoreCorr, id_baseline_ST_150ms_300ms);
propCorrect_face_B_300_450ms =calcPerformanceCorrect(allStoreCorr, id_baseline_ST_300ms_450ms);

ci_perf_face_B_0_150ms = ciPerf(allStoreCorr(id_baseline_ST_0_150ms));
ci_perf_face_B_150_300ms = ciPerf(allStoreCorr(id_baseline_ST_150ms_300ms));
ci_perf_face_B_300_450ms = ciPerf(allStoreCorr(id_baseline_ST_300ms_450ms));

propCorrect_face_S_0_150ms  = calcPerformanceCorrect(allStoreCorr, id_salient_ST_0_150ms);
propCorrect_face_S_150_300ms  = calcPerformanceCorrect(allStoreCorr, id_salient_ST_150ms_300ms);
propCorrect_face_S_300_450ms = calcPerformanceCorrect(allStoreCorr, id_salient_ST_300ms_450ms);

ci_perf_face_S_0_150ms = ciPerf(allStoreCorr(id_salient_ST_0_150ms));
ci_perf_face_S_150_300ms = ciPerf(allStoreCorr(id_salient_ST_150ms_300ms));
ci_perf_face_S_300_450ms = ciPerf(allStoreCorr(id_salient_ST_300ms_450ms));


figure();

errorbar([propCorrect_face_B_0_150ms  propCorrect_face_B_150_300ms  propCorrect_face_B_300_450ms],...
    [ci_perf_face_B_0_150ms ci_perf_face_B_150_300ms  ci_perf_face_B_300_450ms],...
    'o', 'LineStyle', '-','LineWidth',2,'Color','m');

hold on
errorbar([propCorrect_face_S_0_150ms propCorrect_face_S_150_300ms propCorrect_face_S_300_450ms],...
    [ci_perf_face_S_0_150ms ci_perf_face_S_150_300ms ci_perf_face_S_300_450ms],...
    'o', 'LineStyle', '-','LineWidth',2,'Color','b');

text0_B = strcat('n=',string(length(id_baseline_ST_0_150ms)));
text(1.1,propCorrect_face_B_0_150ms ,text0_B,'color','r');

text1_B = strcat('n=',string(length(id_baseline_ST_150ms_300ms)));
text(2.1,propCorrect_face_B_150_300ms ,text1_B,'color','r');

text2_B = strcat('n=',string(length(id_baseline_ST_300ms_450ms)));
text(3.1,propCorrect_face_B_300_450ms ,text2_B,'color','r');

text0_S = strcat('n=',string(length(id_salient_ST_0_150ms)));
text(1.1,propCorrect_face_S_0_150ms ,text0_S,'color','r');

text1_S = strcat('n=',string(length(id_salient_ST_150ms_300ms)));
text(2.1,propCorrect_face_S_150_300ms ,text1_S,'color','r');

text2_S = strcat('n=',string(length(id_salient_ST_300ms_450ms)));
text(3.1,propCorrect_face_S_300_450ms ,text2_S,'color','r');

set(gca, 'FontSize', 15, ...
    'XTick', [1:1:3], 'XTickLabel',{'0-150','150-300','300-450'});
xlim([0 4])
% ylim([0.4 1])
legend box off
xlabel('Delay Times (ms)')
ylabel('Proportion Correct for faces')
set(gca,'fontsize',27,'FontWeight','Bold')
legend('Baseline', 'Salient');
legend box off
%% Prop correct @ each time slot, BASELINE
% cJet = jet(length(all_delayTimeChoicesPicked));

% propC_B = calcPerformanceCorrect(responses,id_baseline_ST);
% propC_S = calcPerformanceCorrect(responses,id_salient_ST);

propCorrect_B_0ms =calcPerformanceCorrect(responses, id_baseline_ST_0ms);
propCorrect_B_50ms =calcPerformanceCorrect(responses, id_baseline_ST_50ms_0ms);
propCorrect_B_100ms =calcPerformanceCorrect(responses, id_baseline_ST_100ms_0ms);
propCorrect_B_150ms =calcPerformanceCorrect(responses, id_baseline_ST_150ms_0ms);
propCorrect_B_200ms =calcPerformanceCorrect(responses, id_baseline_ST_200ms_0ms);
propCorrect_B_250ms =calcPerformanceCorrect(responses, id_baseline_ST_250ms_0ms);
propCorrect_B_300ms =calcPerformanceCorrect(responses, id_baseline_ST_300ms_0ms);
propCorrect_B_350ms =calcPerformanceCorrect(responses, id_baseline_ST_350ms_0ms);
propCorrect_B_400ms =calcPerformanceCorrect(responses, id_baseline_ST_400ms_0ms);

ci_perf_B_0ms = ciPerf(responses(id_baseline_ST_0ms));
ci_perf_B_50ms = ciPerf(responses(id_baseline_ST_50ms_0ms));
ci_perf_B_100ms = ciPerf(responses(id_baseline_ST_100ms_0ms));
ci_perf_B_150ms = ciPerf(responses(id_baseline_ST_150ms_0ms));
ci_perf_B_200ms = ciPerf(responses(id_baseline_ST_200ms_0ms));
ci_perf_B_250ms = ciPerf(responses(id_baseline_ST_250ms_0ms));
ci_perf_B_300ms = ciPerf(responses(id_baseline_ST_300ms_0ms));
ci_perf_B_350ms = ciPerf(responses(id_baseline_ST_350ms_0ms));
ci_perf_B_400ms = ciPerf(responses(id_baseline_ST_400ms_0ms));


propCorrect_S_0ms  = calcPerformanceCorrect(responses, id_salient_ST_0ms);
propCorrect_S_50ms  = calcPerformanceCorrect(responses, id_salient_ST_50ms_0ms);
propCorrect_S_100ms = calcPerformanceCorrect(responses, id_salient_ST_100ms_0ms);
propCorrect_S_150ms = calcPerformanceCorrect(responses, id_salient_ST_150ms_0ms);
propCorrect_S_200ms = calcPerformanceCorrect(responses, id_salient_ST_200ms_0ms);
propCorrect_S_250ms = calcPerformanceCorrect(responses, id_salient_ST_250ms_0ms);
propCorrect_S_300ms = calcPerformanceCorrect(responses, id_salient_ST_300ms_0ms);
propCorrect_S_350ms = calcPerformanceCorrect(responses, id_salient_ST_350ms_0ms);
propCorrect_S_400ms = calcPerformanceCorrect(responses, id_salient_ST_400ms_0ms);

ci_perf_S_0ms = ciPerf(responses(id_salient_ST_0ms));
ci_perf_S_50ms = ciPerf(responses(id_salient_ST_50ms_0ms));
ci_perf_S_100ms = ciPerf(responses(id_salient_ST_100ms_0ms));
ci_perf_S_150ms = ciPerf(responses(id_salient_ST_150ms_0ms));
ci_perf_S_200ms = ciPerf(responses(id_salient_ST_200ms_0ms));
ci_perf_S_250ms = ciPerf(responses(id_salient_ST_250ms_0ms));
ci_perf_S_300ms = ciPerf(responses(id_salient_ST_300ms_0ms));
ci_perf_S_350ms = ciPerf(responses(id_salient_ST_350ms_0ms));
ci_perf_S_400ms = ciPerf(responses(id_salient_ST_400ms_0ms));

[E1, E2, z,hh_0_50, p_0_50]  = Z_Test(length(find(responses(id_baseline_ST_0ms) == 1)),length(responses(id_baseline_ST_0ms)),...
                           length(find(responses(id_salient_ST_0ms)==1)),length(responses(id_salient_ST_0ms)),0.05,...
                            'two sided',1);
[E1, E2, z,hh_50_100, p_50_100]  = Z_Test(length(find(responses(id_baseline_ST_50ms_0ms) == 1)),length(responses(id_baseline_ST_50ms_0ms)),...
                           length(find(responses(id_salient_ST_50ms_0ms)==1)),length(responses(id_salient_ST_50ms_0ms)),0.05,...
                            'two sided',1);      
[E1, E2, z,hh_100_150, p_100_150]  = Z_Test(length(find(responses(id_baseline_ST_100ms_0ms) == 1)),length(responses(id_baseline_ST_100ms_0ms)),...
                           length(find(responses(id_salient_ST_100ms_0ms)==1)),length(responses(id_salient_ST_100ms_0ms)),0.05,...
                            'two sided',1); 
[E1, E2, z,hh_150_200, p_150_200]  = Z_Test(length(find(responses(id_baseline_ST_150ms_0ms) == 1)),length(responses(id_baseline_ST_150ms_0ms)),...
                           length(find(responses(id_salient_ST_150ms_0ms)==1)),length(responses(id_salient_ST_150ms_0ms)),0.05,...
                            'two sided',1);
[E1, E2, z,hh_200_250, p_200_250]  = Z_Test(length(find(responses(id_baseline_ST_200ms_0ms) == 1)),length(responses(id_baseline_ST_200ms_0ms)),...
                           length(find(responses(id_salient_ST_200ms_0ms)==1)),length(responses(id_salient_ST_200ms_0ms)),0.05,...
                            'two sided',1); 
[E1, E2, z,hh_250_300, p_250_300]  = Z_Test(length(find(responses(id_baseline_ST_250ms_0ms) == 1)),length(responses(id_baseline_ST_250ms_0ms)),...
                           length(find(responses(id_salient_ST_250ms_0ms)==1)),length(responses(id_salient_ST_250ms_0ms)),0.05,...
                            'two sided',1); 
[E1, E2, z,hh_300_350, p_300_350]  = Z_Test(length(find(responses(id_baseline_ST_300ms_0ms) == 1)),length(responses(id_baseline_ST_300ms_0ms)),...
                           length(find(responses(id_salient_ST_300ms_0ms)==1)),length(responses(id_salient_ST_300ms_0ms)),0.05,...
                            'two sided',1); 
[E1, E2, z,hh_350_400, p_350_400]  = Z_Test(length(find(responses(id_baseline_ST_350ms_0ms) == 1)),length(responses(id_baseline_ST_350ms_0ms)),...
                           length(find(responses(id_salient_ST_350ms_0ms)==1)),length(responses(id_salient_ST_350ms_0ms)),0.05,...
                            'two sided',1);
[E1, E2, z,hh_400_450, p_400_450]  = Z_Test(length(find(responses(id_baseline_ST_400ms_0ms) == 1)),length(responses(id_baseline_ST_400ms_0ms)),...
                           length(find(responses(id_salient_ST_400ms_0ms)==1)),length(responses(id_salient_ST_400ms_0ms)),0.05,...
                            'two sided',1);

figure();

errorbar([propCorrect_B_0ms  propCorrect_B_50ms  propCorrect_B_100ms,...
    propCorrect_B_150ms propCorrect_B_200ms,...
    propCorrect_B_250ms propCorrect_B_300ms,...
    propCorrect_B_350ms propCorrect_B_400ms],...
    [ci_perf_B_0ms ci_perf_B_50ms  ci_perf_B_100ms,...
    ci_perf_B_150ms ci_perf_B_200ms ci_perf_B_250ms ci_perf_B_300ms,...
    ci_perf_B_350ms ci_perf_B_400ms],...
    'o', 'LineStyle', '-','LineWidth',2,'Color','m');

hold on
errorbar([propCorrect_S_0ms propCorrect_S_50ms propCorrect_S_100ms,...
    propCorrect_S_150ms propCorrect_S_200ms,...
    propCorrect_S_250ms  propCorrect_S_300ms,...
    propCorrect_S_350ms  propCorrect_S_400ms],...
    [ci_perf_S_0ms ci_perf_S_50ms ci_perf_S_100ms,...
    ci_perf_S_150ms ci_perf_S_200ms ci_perf_S_250ms ci_perf_S_300ms,...
    ci_perf_S_350ms ci_perf_S_400ms],...
    'o', 'LineStyle', '-','LineWidth',2,'Color','b');

text0_B = strcat('n=',string(length(id_baseline_ST_0ms)));
text(1.1,propCorrect_B_0ms ,text0_B,'color','r');

text1_B = strcat('n=',string(length(id_baseline_ST_50ms_0ms)));
text(2.1,propCorrect_B_50ms ,text1_B,'color','r');

text2_B = strcat('n=',string(length(id_baseline_ST_100ms_0ms)));
text(3.1,propCorrect_B_100ms ,text2_B,'color','r');

text3_B = strcat('n=',string(length(id_baseline_ST_150ms_0ms)));
text(4.1,propCorrect_B_150ms ,text3_B,'color','r');

text4_B = strcat('n=',string(length(id_baseline_ST_200ms_0ms)));
text(5.1,propCorrect_B_200ms ,text4_B,'color','r');

text5_B = strcat('n=',string(length(id_baseline_ST_250ms_0ms)));
text(6.1,propCorrect_B_250ms ,text5_B,'color','r');

text6_B = strcat('n=',string(length(id_baseline_ST_300ms_0ms)));
text(7.1,propCorrect_B_300ms ,text6_B,'color','r');

text7_B = strcat('n=',string(length(id_baseline_ST_350ms_0ms)));
text(8.1,propCorrect_B_350ms ,text7_B,'color','r');

text8_B = strcat('n=',string(length(id_baseline_ST_400ms_0ms)));
text(9.1,propCorrect_B_400ms ,text8_B,'color','r');

text0_S = strcat('n=',string(length(id_salient_ST_0ms)));
text(1.1,propCorrect_S_0ms ,text0_S,'color','r');

text1_S = strcat('n=',string(length(id_salient_ST_50ms_0ms)));
text(2.1,propCorrect_S_50ms ,text1_S,'color','r');

text2_S = strcat('n=',string(length(id_salient_ST_100ms_0ms)));
text(3.1,propCorrect_S_100ms ,text2_S,'color','r');

text3_S = strcat('n=',string(length(id_salient_ST_150ms_0ms)));
text(4.1,propCorrect_S_150ms ,text3_S,'color','r');

text4_S = strcat('n=',string(length(id_salient_ST_200ms_0ms)));
text(5.1,propCorrect_S_200ms ,text4_S,'color','r');

text5_S = strcat('n=',string(length(id_salient_ST_250ms_0ms)));
text(6.1,propCorrect_S_250ms ,text5_S,'color','r');

text6_S = strcat('n=',string(length(id_salient_ST_300ms_0ms)));
text(7.1,propCorrect_S_300ms ,text6_S,'color','r');

text7_S = strcat('n=',string(length(id_salient_ST_350ms_0ms)));
text(8.1,propCorrect_S_350ms ,text7_S,'color','r');

text8_S = strcat('n=',string(length(id_salient_ST_400ms_0ms)));
text(9.1,propCorrect_S_400ms ,text8_S,'color','r');

set(gca, 'FontSize', 15, ...
    'XTick', [1:1:9], 'XTickLabel',{'0-50','50-100','100-150','150-200','200-250','250-300','300-350','350-400','400-450'});
xlim([0 10])
legend box off
xlabel('Time after Fixation onset (ms)')
ylabel('Proportion Correct')
set(gca,'fontsize',27,'FontWeight','Bold')
legend('Baseline', 'Salient');
legend box off



%%--------------Hit rate-----------------------------
figure()
plt = plot(TimeBins_B,HR_B,'Color','m')
pltHandles = plt;
hold on
plt = plot(TimeBins_S,HR_S,'Color','b')
pltHandles(2) = plt
legend(pltHandles,{'Baseline','Salient'})
legend box off
xlabel('Timeline (ms)')
ylabel('Hit Rate')
set(gca,'fontsize',27,'FontWeight','Bold')
%%--------------False alarm rate-----------------------------
figure()
plt = plot(TimeBins_B,FA_B,'Color','m')
pltHandles = plt;
hold on
plt = plot(TimeBins_S,FA_S,'Color','b')
pltHandles(2) = plt
legend(pltHandles,{'Baseline','Salient'})
legend box off
xlabel('Timeline (ms)')
ylabel('False alarm Rate')
set(gca,'fontsize',27,'FontWeight','Bold')
%%-------------------d-prime ------------------------------
figure()
plt = plot(TimeBins_B,Dprime_B,'Color','m')
pltHandles = plt;
% hold on
% shadedErrorBar(TimeBins_B,Dprime_B,Dprime_CI_B,{'-or','color','m'},1)
hold on
plt = plot(TimeBins_S,Dprime_S,'Color','b')
pltHandles(2) = plt
hold on
% shadedErrorBar(TimeBins_S,Dprime_S,Dprime_CI_B,{'-or','color','b'},1)
legend(pltHandles,{'Baseline','Salient'})
legend box off
xlabel('Timeline')
ylabel('d-prime')
set(gca,'fontsize',27,'FontWeight','Bold')
%%-------------------------

[d0_B ,std_d_0_B ,ci_d_0_B,crit_0_B ,var_d_0_B, HitRate_0_B, FaRate_0_B] = calculateDprime(responses(id_baseline_ST_0ms), condition(id_baseline_ST_0ms));
[d50_B ,std_d_50_B ,ci_d_50_B,crit_50_B ,var_d_50_B, HitRate_50_B, FaRate_50_B] = calculateDprime(responses(id_baseline_ST_50ms_0ms), condition(id_baseline_ST_50ms_0ms));
[d100_B ,std_d_100_B ,ci_d_100_B,crit_100_B ,var_d_100_B, HitRate_100_B, FaRate_100_B] = calculateDprime(responses(id_baseline_ST_100ms_0ms), condition(id_baseline_ST_100ms_0ms));
[d150_B ,std_d_150_B ,ci_d_150_B,crit_150_B ,var_d_150_B, HitRate_150_B, FaRate_150_B] = calculateDprime(responses(id_baseline_ST_150ms_0ms), condition(id_baseline_ST_150ms_0ms));
[d200_B ,std_d_200_B ,ci_d_200_B,crit_200_B ,var_d_200_B, HitRate_200_B, FaRate_200_B] = calculateDprime(responses(id_baseline_ST_200ms_0ms), condition(id_baseline_ST_200ms_0ms));
[d250_B ,std_d_250_B ,ci_d_250_B,crit_250_B ,var_d_250_B, HitRate_250_B, FaRate_250_B] = calculateDprime(responses(id_baseline_ST_250ms_0ms), condition(id_baseline_ST_250ms_0ms));
[d300_B ,std_d_300_B ,ci_d_300_B,crit_300_B ,var_d_300_B, HitRate_300_B, FaRate_300_B] = calculateDprime(responses(id_baseline_ST_300ms_0ms), condition(id_baseline_ST_300ms_0ms));
[d350_B ,std_d_350_B ,ci_d_350_B,crit_350_B ,var_d_350_B, HitRate_350_B, FaRate_350_B] = calculateDprime(responses(id_baseline_ST_350ms_0ms), condition(id_baseline_ST_350ms_0ms));
[d400_B ,std_d_400_B ,ci_d_400_B,crit_400_B ,var_d_400_B, HitRate_400_B, FaRate_400_B] = calculateDprime(responses(id_baseline_ST_400ms_0ms), condition(id_baseline_ST_400ms_0ms));

[d0_S ,std_d_0_S ,ci_d_0_S,crit_0_S ,var_d_0_S, HitRate_0_S, FaRate_0_S] = calculateDprime(responses(id_salient_ST_0ms), condition(id_salient_ST_0ms));
[d50_S ,std_d_50_S ,ci_d_50_S,crit_50_S ,var_d_50_S, HitRate_50_S, FaRate_50_S] = calculateDprime(responses(id_salient_ST_50ms_0ms), condition(id_salient_ST_50ms_0ms));
[d100_S ,std_d_100_S ,ci_d_100_S,crit_100_S ,var_d_100_S, HitRate_100_S, FaRate_100_S] = calculateDprime(responses(id_salient_ST_100ms_0ms), condition(id_salient_ST_100ms_0ms));
[d150_S ,std_d_150_S ,ci_d_150_S,crit_150_S ,var_d_150_S, HitRate_150_S, FaRate_150_S] = calculateDprime(responses(id_salient_ST_150ms_0ms), condition(id_salient_ST_150ms_0ms));
[d200_S ,std_d_200_S ,ci_d_200_S,crit_200_S ,var_d_200_S, HitRate_200_S, FaRate_200_S] = calculateDprime(responses(id_salient_ST_200ms_0ms), condition(id_salient_ST_200ms_0ms));
[d250_S ,std_d_250_S ,ci_d_250_S,crit_250_S ,var_d_250_S, HitRate_250_S, FaRate_250_S] = calculateDprime(responses(id_salient_ST_250ms_0ms), condition(id_salient_ST_250ms_0ms));
[d300_S ,std_d_300_S ,ci_d_300_S,crit_300_S ,var_d_300_S, HitRate_300_S, FaRate_300_S] = calculateDprime(responses(id_salient_ST_300ms_0ms), condition(id_salient_ST_300ms_0ms));
[d350_S ,std_d_350_S ,ci_d_350_S,crit_350_S ,var_d_350_S, HitRate_350_S, FaRate_350_S] = calculateDprime(responses(id_salient_ST_350ms_0ms), condition(id_salient_ST_350ms_0ms));
[d400_S ,std_d_400_S ,ci_d_400_S,crit_400_S ,var_d_400_S, HitRate_400_S, FaRate_400_S] = calculateDprime(responses(id_salient_ST_400ms_0ms), condition(id_salient_ST_400ms_0ms));

figure()
plot([1,2,3,4,5,6,7,8,9],[d0_B,d50_B,d100_B,d150_B,d200_B,d250_B,d300_B,d350_B,d400_B],'-o','markerFaceColor','m','Color','m');
hold on
plot([1,2,3,4,5,6,7,8,9],[d0_S,d50_S,d100_S,d150_S,d200_S,d250_S,d300_S,d350_S,d400_S],'-o','markerFaceColor','b','Color','b');

set(gca, 'FontSize', 15, ...
    'XTick', [1:1:9], 'XTickLabel',{'0-50','50-100','100-150','150-200','200-250','250-300','300-350','350-400','400-450'});
xlim([0 10])
xlabel('Delay Time (ms)')
ylabel('d-prime');
% ylim([0 1]);
legend('Baseline','Salient')
set(gca,'fontsize',27,'FontWeight','Bold')
legend box off

figure()
plot([1,2,3,4,5,6,7,8,9],[HitRate_0_B,HitRate_50_B,HitRate_100_B,HitRate_150_B,HitRate_200_B,HitRate_250_B,HitRate_300_B,HitRate_350_B,HitRate_400_B],'-o','markerFaceColor','m','Color','m');
hold on
plot([1,2,3,4,5,6,7,8,9],[HitRate_0_S,HitRate_50_S,HitRate_100_S,HitRate_150_S,HitRate_200_S,HitRate_250_S,HitRate_300_S,HitRate_350_S,HitRate_400_S],'-o','markerFaceColor','b','Color','b');
set(gca, 'FontSize', 15, ...
    'XTick', [1:1:9], 'XTickLabel',{'0-50','50-100','100-150','150-200','200-250','250-300','300-350','350-400','400-450'});
xlim([0 10])
xlabel('Delay Time (ms)')
ylabel('Hit rate');
legend('Baseline','Salient')
set(gca,'fontsize',27,'FontWeight','Bold')
legend box off

figure()
plot([1,2,3,4,5,6,7,8,9],[FaRate_0_B,FaRate_50_B,FaRate_100_B,FaRate_150_B,FaRate_200_B,FaRate_250_B,FaRate_300_B,FaRate_350_B,FaRate_400_B],'-o','markerFaceColor','m','Color','m');
hold on
plot([1,2,3,4,5,6,7,8,9],[FaRate_0_S,FaRate_50_S,FaRate_100_S,FaRate_150_S,FaRate_200_S,FaRate_250_S,FaRate_300_S,FaRate_350_S,FaRate_400_S],'-o','markerFaceColor','b','Color','b');
set(gca, 'FontSize', 15, ...
    'XTick', [1:1:9], 'XTickLabel',{'0-50','50-100','100-150','150-200','200-250','250-300','300-350','350-400','400-450'});
xlim([0 10])
xlabel('Delay Time (ms)')
ylabel('False alarm rate');
legend('Baseline','Salient')
set(gca,'fontsize',27,'FontWeight','Bold')
legend box off

figure()
plot([1,2,3,4,5,6,7,8,9],[crit_0_B,crit_50_B,crit_100_B,crit_150_B,crit_200_B,crit_250_B,crit_300_B,crit_350_B,crit_400_B],'-o','markerFaceColor','m','Color','m');
hold on
plot([1,2,3,4,5,6,7,8,9],[crit_0_S,crit_50_S,crit_100_S,crit_150_S,crit_200_S,crit_250_S,crit_300_S,crit_350_S,crit_400_S],'-o','markerFaceColor','b','Color','b');

set(gca, 'FontSize', 15, ...
    'XTick', [1:1:9], 'XTickLabel',{'0-50','50-100','100-150','150-200','200-250','250-300','300-350','350-400','400-450'});
xlim([0 10])
xlabel('Delay Time (ms)')
ylabel('criterion bias');
legend('Baseline','Salient')
set(gca,'fontsize',27,'FontWeight','Bold')
legend box off

%% Reaction times plots
figure()
plot([1,2,3,4,5,6,7,8,9],[mean(reactionTimes(id_baseline_ST_0ms)),mean(reactionTimes(id_baseline_ST_50ms_0ms)),mean(reactionTimes(id_baseline_ST_100ms_0ms)),...
    mean(reactionTimes(id_baseline_ST_150ms_0ms)),mean(reactionTimes(id_baseline_ST_200ms_0ms)),...
    mean(reactionTimes(id_baseline_ST_250ms_0ms)),mean(reactionTimes(id_baseline_ST_300ms_0ms)),...
    mean(reactionTimes(id_baseline_ST_350ms_0ms)),mean(reactionTimes(id_baseline_ST_400ms_0ms))],'-o','markerFaceColor','m','Color','m');
hold on
plot([1,2,3,4,5,6,7,8,9],[mean(reactionTimes(id_salient_ST_0ms)),mean(reactionTimes(id_salient_ST_50ms_0ms)),mean(reactionTimes(id_salient_ST_100ms_0ms)),...
    mean(reactionTimes(id_salient_ST_150ms_0ms)),mean(reactionTimes(id_salient_ST_200ms_0ms)),...
    mean(reactionTimes(id_salient_ST_250ms_0ms)),mean(reactionTimes(id_salient_ST_300ms_0ms)),...
    mean(reactionTimes(id_salient_ST_350ms_0ms)),mean(reactionTimes(id_salient_ST_400ms_0ms))],'-o','markerFaceColor','b','Color','b');

set(gca, 'FontSize', 15, ...
    'XTick', [1:1:9], 'XTickLabel',{'0-50','50-100','100-150','150-200','200-250','250-300','300-350','350-400','400-450'});
xlim([0 10])
xlabel('Delay Time (ms)')
ylabel('Overall Reaction times');
% ylim([0 1]);
legend('Baseline','Salient')
set(gca,'fontsize',27,'FontWeight','Bold')
legend box off

%% Reaction time plots (150ms) time bin

figure()
plot([1,2,3],[mean(reactionTimes(id_baseline_ST_0_150ms)),mean(reactionTimes(id_baseline_ST_150ms_300ms)),mean(reactionTimes(id_baseline_ST_300ms_450ms))],'-o','markerFaceColor','m','Color','m');
hold on
plot([1,2,3],[mean(reactionTimes(id_salient_ST_0_150ms)),mean(reactionTimes(id_salient_ST_150ms_300ms)),mean(reactionTimes(id_salient_ST_300ms_450ms))],'-o','markerFaceColor','b','Color','b');

set(gca, 'FontSize', 15, ...
    'XTick', [1:1:3], 'XTickLabel',{'0-150','150-300','300-450'});
xlim([0 4])
xlabel('Delay Time (ms)')
ylabel('Overall Reaction times');
% ylim([0 1]);
legend('Baseline','Salient')
set(gca,'fontsize',27,'FontWeight','Bold')
legend box off

changeCond = find(condition == 1);
noChangeCond = find(condition == 0);
corrResponse = find(responses == 1);
wrongResponse = find(responses == 0);

hits = changeCond(ismember(changeCond,corrResponse));
falseAlarm = noChangeCond(ismember(noChangeCond,wrongResponse));

id_B_0_hits = hits(ismember(hits,id_baseline_ST_0ms));
id_B_50_hits = hits(ismember(hits,id_baseline_ST_50ms_0ms));
id_B_100_hits = hits(ismember(hits,id_baseline_ST_100ms_0ms));
id_B_150_hits = hits(ismember(hits,id_baseline_ST_150ms_0ms));
id_B_200_hits = hits(ismember(hits,id_baseline_ST_200ms_0ms));
id_B_250_hits = hits(ismember(hits,id_baseline_ST_250ms_0ms));
id_B_300_hits = hits(ismember(hits,id_baseline_ST_300ms_0ms));
id_B_350_hits = hits(ismember(hits,id_baseline_ST_350ms_0ms));
id_B_400_hits = hits(ismember(hits,id_baseline_ST_400ms_0ms));

id_S_0_hits = hits(ismember(hits,id_salient_ST_0ms));
id_S_50_hits = hits(ismember(hits,id_salient_ST_50ms_0ms));
id_S_100_hits = hits(ismember(hits,id_salient_ST_100ms_0ms));
id_S_150_hits = hits(ismember(hits,id_salient_ST_150ms_0ms));
id_S_200_hits = hits(ismember(hits,id_salient_ST_200ms_0ms));
id_S_250_hits = hits(ismember(hits,id_salient_ST_250ms_0ms));
id_S_300_hits = hits(ismember(hits,id_salient_ST_300ms_0ms));
id_S_350_hits = hits(ismember(hits,id_salient_ST_350ms_0ms));
id_S_400_hits = hits(ismember(hits,id_salient_ST_400ms_0ms));

id_B_0_fa = falseAlarm(ismember(falseAlarm,id_baseline_ST_0ms));
id_B_50_fa = falseAlarm(ismember(falseAlarm,id_baseline_ST_50ms_0ms));
id_B_100_fa = falseAlarm(ismember(falseAlarm,id_baseline_ST_100ms_0ms));
id_B_150_fa = falseAlarm(ismember(falseAlarm,id_baseline_ST_150ms_0ms));
id_B_200_fa = falseAlarm(ismember(falseAlarm,id_baseline_ST_200ms_0ms));
id_B_250_fa = falseAlarm(ismember(falseAlarm,id_baseline_ST_250ms_0ms));
id_B_300_fa = falseAlarm(ismember(falseAlarm,id_baseline_ST_300ms_0ms));
id_B_350_fa = falseAlarm(ismember(falseAlarm,id_baseline_ST_350ms_0ms));
id_B_400_fa = falseAlarm(ismember(falseAlarm,id_baseline_ST_400ms_0ms));

id_S_0_fa = falseAlarm(ismember(falseAlarm,id_salient_ST_0ms));
id_S_50_fa = falseAlarm(ismember(falseAlarm,id_salient_ST_50ms_0ms));
id_S_100_fa = falseAlarm(ismember(falseAlarm,id_salient_ST_100ms_0ms));
id_S_150_fa = falseAlarm(ismember(falseAlarm,id_salient_ST_150ms_0ms));
id_S_200_fa = falseAlarm(ismember(falseAlarm,id_salient_ST_200ms_0ms));
id_S_250_fa = falseAlarm(ismember(falseAlarm,id_salient_ST_250ms_0ms));
id_S_300_fa = falseAlarm(ismember(falseAlarm,id_salient_ST_300ms_0ms));
id_S_350_fa = falseAlarm(ismember(falseAlarm,id_salient_ST_350ms_0ms));
id_S_400_fa = falseAlarm(ismember(falseAlarm,id_salient_ST_400ms_0ms));

figure()
plot([1,2,3,4,5,6,7,8,9],[mean(reactionTimes(id_B_0_hits)),mean(reactionTimes(id_B_50_hits)),mean(reactionTimes(id_B_100_hits)),...
    mean(reactionTimes(id_B_150_hits)),mean(reactionTimes(id_B_200_hits)),...
    mean(reactionTimes(id_B_250_hits)),mean(reactionTimes(id_B_300_hits)),...
    mean(reactionTimes(id_B_350_hits)),mean(reactionTimes(id_B_400_hits))],'-o','markerFaceColor','m','Color','m');
hold on
plot([1,2,3,4,5,6,7,8,9],[mean(reactionTimes(id_S_0_hits)),mean(reactionTimes(id_S_50_hits)),mean(reactionTimes(id_S_100_hits )),...
    mean(reactionTimes(id_S_150_hits )),mean(reactionTimes(id_S_200_hits )),...
    mean(reactionTimes(id_S_250_hits )),mean(reactionTimes(id_S_300_hits )),...
    mean(reactionTimes(id_S_350_hits )),mean(reactionTimes(id_S_400_hits ))],'-o','markerFaceColor','b','Color','b');

set(gca, 'FontSize', 15, ...
    'XTick', [1:1:9], 'XTickLabel',{'0-50','50-100','100-150','150-200','200-250','250-300','300-350','350-400','400-450'});
xlim([0 10])
xlabel('Delay Time (ms)')
ylabel('Reaction times (hit trials)');
% ylim([0 1]);
legend('Baseline','Salient')
legend box off
set(gca,'fontsize',27,'FontWeight','Bold')

figure()
plot([1,2,3,4,5,6,7,8,9],[mean(reactionTimes(id_B_0_fa)),mean(reactionTimes(id_B_50_fa)),mean(reactionTimes(id_B_100_fa)),...
    mean(reactionTimes(id_B_150_fa)),mean(reactionTimes(id_B_200_fa)),...
    mean(reactionTimes(id_B_250_fa)),mean(reactionTimes(id_B_300_fa)),...
    mean(reactionTimes(id_B_350_fa)),mean(reactionTimes(id_B_400_fa))],'-o','markerFaceColor','m','Color','m');
hold on
plot([1,2,3,4,5,6,7,8,9],[mean(reactionTimes(id_S_0_fa)),mean(reactionTimes(id_S_50_fa)),mean(reactionTimes(id_S_100_fa)),...
    mean(reactionTimes(id_S_150_fa)),mean(reactionTimes(id_S_200_fa)),...
    mean(reactionTimes(id_S_250_fa)),mean(reactionTimes(id_S_300_fa)),...
    mean(reactionTimes(id_S_350_fa)),mean(reactionTimes(id_S_400_fa))],'-o','markerFaceColor','b','Color','b');

set(gca, 'FontSize', 15, ...
    'XTick', [1:1:9], 'XTickLabel',{'0-50','50-100','100-150','150-200','200-250','250-300','300-350','350-400','400-450'});
xlim([0 10])
xlabel('Delay Time (ms)')
ylabel('Reaction times (false alarm trials)');
% ylim([0 1]);
legend('Baseline','Salient')
legend box off
set(gca,'fontsize',27,'FontWeight','Bold')

%% Probability of occurence plots
% figure()
% [no,xo] = hist(condition_labels_original, [0,1,2,3]);
% bar([1:1:4],no/length(condition_labels_original))
% title('Probability of each condition occurance')
% set(gca, 'FontSize', 15, ...
%     'XTick', [1:1:4], 'XTickLabel',{'Baseline','B No change', 'Salient','S No change'});
% xlim([0 5])
% xlabel('condition')
% ylabel('Probability');
% box off
% %% Probablity of all delaytime choices occuring through the entire exp
% figure()
% [no,xo] = hist(delayTimeChoicesPicked, [50,100,150,200,250,300,350,400]);
% bar([1:1:8],no/length(delayTimeChoicesPicked))
% title('Probability of each delay time occuring')
% set(gca, 'FontSize', 15, ...
%     'XTick', [1:1:8], 'XTickLabel',{'50-100ms','100-150ms','150-200ms','200-250ms','250-300ms','300-350ms','350-400ms','400-450ms'});
% xlim([0 9])
% xlabel('Delay Times')
% ylabel('Probability');
% box off
%% Probablity of all delaytime choices occuring through each condition
% figure();
% dTimes_B = [(length(idx_baseline_change_50_ms)/length(delayTimeChoicesPicked))*100,(length(idx_baseline_noChange_50_ms)/length(delayTimeChoicesPicked))*100;...
%     (length(idx_baseline_change_100_ms)/length(delayTimeChoicesPicked))*100,(length(idx_baseline_noChange_100_ms)/length(delayTimeChoicesPicked))*100;...
%     (length(idx_baseline_change_150_ms)/length(delayTimeChoicesPicked))*100,(length(idx_baseline_noChange_150_ms)/length(delayTimeChoicesPicked))*100;...
%     (length(idx_baseline_change_200_ms)/length(delayTimeChoicesPicked))*100,(length(idx_baseline_noChange_200_ms)/length(delayTimeChoicesPicked))*100;...
%     (length(idx_baseline_change_250_ms)/length(delayTimeChoicesPicked))*100,(length(idx_baseline_noChange_250_ms)/length(delayTimeChoicesPicked))*100;...
%     (length(idx_baseline_change_300_ms)/length(delayTimeChoicesPicked))*100,(length(idx_baseline_noChange_300_ms)/length(delayTimeChoicesPicked))*100;...
%     (length(idx_baseline_change_350_ms)/length(delayTimeChoicesPicked))*100,(length(idx_baseline_noChange_350_ms)/length(delayTimeChoicesPicked))*100;...
%     (length(idx_baseline_change_400_ms)/length(delayTimeChoicesPicked))*100,(length(idx_baseline_noChange_400_ms)/length(delayTimeChoicesPicked))*100]
% 
% bar([1:1:8],dTimes_B)
% title('Probability of each delay time occuring  - Baseline')
% set(gca, 'FontSize', 15, ...
%     'XTick', [1:1:8], 'XTickLabel',{'50-100ms','100-150ms','150-200ms','200-250ms','250-300ms','300-350ms','350-400ms','400-450ms'});
% xlim([0 9])
% xlabel('Delay Times')
% ylabel('Probability');
% legend('change', 'no change')
% box off
% 
% figure();
% dTimes_S = [(length(idx_salient_change_50_ms)/length(delayTimeChoicesPicked))*100,(length(idx_salient_noChange_50_ms)/length(delayTimeChoicesPicked))*100;...
%     (length(idx_salient_change_100_ms)/length(delayTimeChoicesPicked))*100,(length(idx_salient_noChange_100_ms)/length(delayTimeChoicesPicked))*100;...
%     (length(idx_salient_change_150_ms)/length(delayTimeChoicesPicked))*100,(length(idx_salient_noChange_150_ms)/length(delayTimeChoicesPicked))*100;...
%     (length(idx_salient_change_200_ms)/length(delayTimeChoicesPicked))*100,(length(idx_salient_noChange_200_ms)/length(delayTimeChoicesPicked))*100;...
%     (length(idx_salient_change_250_ms)/length(delayTimeChoicesPicked))*100,(length(idx_salient_noChange_250_ms)/length(delayTimeChoicesPicked))*100;...
%     (length(idx_salient_change_300_ms)/length(delayTimeChoicesPicked))*100,(length(idx_salient_noChange_300_ms)/length(delayTimeChoicesPicked))*100;...
%     (length(idx_salient_change_350_ms)/length(delayTimeChoicesPicked))*100,(length(idx_salient_noChange_350_ms)/length(delayTimeChoicesPicked))*100;...
%     (length(idx_salient_change_400_ms)/length(delayTimeChoicesPicked))*100,(length(idx_salient_noChange_400_ms)/length(delayTimeChoicesPicked))*100];
% 
% bar([1:1:8],dTimes_S)
% title('Probability of each delay time occuring  - Salient')
% set(gca, 'FontSize', 15, ...
%     'XTick', [1:1:8], 'XTickLabel',{'50-100ms','100-150ms','150-200ms','200-250ms','250-300ms','300-350ms','350-400ms','400-450ms'});
% xlim([0 9])
% xlabel('Delay Times')
% ylabel('Probability');
% legend('change', 'no change')
% box off
% 
% %% Prop correct in each condition plots
propCorrect_B = calcPerformanceCorrect(responses, idx_baseline);
propCorrect_BC = calcPerformanceCorrect(responses, idx_baseline_catch);
propCorrect_S = calcPerformanceCorrect(responses, idx_salient);
propCorrect_SC =calcPerformanceCorrect(responses, idx_salient_catch);

ci_perf_B = ciPerf(responses(idx_baseline));
ci_perf_BC = ciPerf(responses(idx_baseline_catch));
ci_perf_S = ciPerf(responses(idx_salient));
ci_perf_SC = ciPerf(responses(idx_salient_catch));

figure();
pC = [propCorrect_B propCorrect_BC ; propCorrect_S propCorrect_SC];

b = bar(pC);
% errorbar([propCorrect_B propCorrect_BC propCorrect_S propCorrect_SC], [ci_perf_B ci_perf_BC ci_perf_S ci_perf_SC],...
%     'o', 'LineStyle', '-','LineWidth',2);

% set(gca, 'FontSize', 15, ...
%     'XTick', [1:1:4], 'XTickLabel',{'B, change','B, no change', 'S, change','S, no change'});
set(gca, 'FontSize', 15, ...
    'XTick', [1:1:2], 'XTickLabel',{'Baseline','Salient'});
set(b, {'DisplayName'}, {'left','right'}')
legend()
legend box off
xlim([0 3])
xlabel('condition')
ylabel('prop correct');


totalPC_B =  (propCorrect_B + propCorrect_BC)/2;
totalPC_S =  (propCorrect_S + propCorrect_SC)/2;

total_pC = [totalPC_B;totalPC_S];
b2 = bar(total_pC);
set(gca,'fontsize',27,'FontWeight','Bold',...
    'XTick', [1:1:2], 'XTickLabel',{'Baseline','Salient'});


xlim([0 3])
xlabel('condition')
ylabel('prop correct');

%%------------------------------------------------
fprintf('Num no track trials: %.2f \n', length(find(recordNoTracks == 1)))
fprintf('Num blink trials: %.2f \n', length(find(recordBlinks == 1)))
fprintf('Num invalid trials: %.2f \n', length(find(recordInvalid == 1)))
fprintf('Num microsaccade trials: %.2f \n', length(find(recordMicroSaccades ~= 0)))
fprintf('Num saccade trials: %.2f \n', length(find(recordSaccades ~= 0)))

totalTrialsConsidered = length(find(recordNoTracks == 1)) + length(find(recordBlinks == 1)) + length(find(recordInvalid == 1)) + length(find(recordMicroSaccades ~= 0)) + length(find(recordSaccades ~= 0));

fprintf('Num clean trials: %.2f \n', length(dataHolder) - totalTrialsConsidered)





