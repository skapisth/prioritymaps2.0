function analyze_ms_trials(ms_trials,...
                         idx_gender_B,idx_emotion_B,idx_dir_B,...
                         idx_gender_S,idx_emotion_S,idx_dir_S)
                     
    [rate_ms_b_ch,rate_ms_a_ch,rate_overall,condition_ms_trials] = deal([]);
    for all = 1:length(ms_trials)
        condition_ms_trials(1,end+1)        = ms_trials{all}.groundTruthCond_id;
        find_ms_before_change = find((ms_trials{all}.microsaccades.start > ms_trials{all}.TimeFixationOff &  ms_trials{all}.microsaccades.start<ms_trials{all}.TimeChangeOn));
        find_ms_after_change = find((ms_trials{all}.microsaccades.start > ms_trials{all}.TimeChangeOff &  ms_trials{all}.microsaccades.start<ms_trials{all}.TimeResponseOn));
        find_ms_all = find((ms_trials{all}.microsaccades.start > ms_trials{all}.TimeFixationOff &  ms_trials{all}.microsaccades.start<ms_trials{all}.TimeResponseOn));
         
        total_stim_time = ms_trials{all}.TimeStimulusOff - ms_trials{all}.TimeFixationOff;
        
        if ~isempty(find_ms_before_change)
            rate_ms_b_ch(1,end+1) = (1000*length(find_ms_before_change))/total_stim_time;
        end
        if ~isempty(find_ms_after_change)
            rate_ms_a_ch(1,end+1) = (1000*length(find_ms_after_change))/total_stim_time;
        end
        if ~isempty(find_ms_all)
            rate_overall(1,end+1) = (1000*length(find_ms_all))/total_stim_time;
        end
        
    end
    
    all_conditionLabels  = unique(condition_ms_trials);

    idx_baseline             = find(condition_ms_trials == all_conditionLabels(1));
    idx_baseline_catch       = find(condition_ms_trials == all_conditionLabels(2));
    idx_salient              = find(condition_ms_trials == all_conditionLabels(3));
    idx_salient_catch        = find(condition_ms_trials == all_conditionLabels(4));

    baseline_ids = [idx_baseline,idx_baseline_catch];
    salient_ids = [idx_salient,idx_salient_catch];
    
    assert(length(salient_ids)+length(baseline_ids) == length(ms_trials));
    assert(length(idx_gender_B)+length(idx_emotion_B)+length(idx_dir_B)+...
           length(idx_gender_S)+length(idx_emotion_S)+length(idx_dir_S) == length(ms_trials)); 
    
    o_rate = mean(rate_overall);
    std_err_o_rate = std(rate_overall)/sqrt(length(rate_overall));
    b_rate = mean(rate_overall(baseline_ids));
    std_err_b_rate = std(rate_overall(baseline_ids))/sqrt(length(rate_overall(baseline_ids)));
    s_rate = mean(rate_overall(salient_ids));
    std_err_s_rate = std(rate_overall(salient_ids))/sqrt(length(rate_overall(salient_ids)));
    
    gender_b_rate    = mean(rate_overall(idx_gender_B));
    std_err_g_b_r = std(rate_overall(idx_gender_B))/sqrt(length(rate_overall(idx_gender_B)));
    
    emotion_b_rate   = mean(rate_overall(idx_emotion_B));
    std_err_e_b_r = std(rate_overall(idx_emotion_B))/sqrt(length(rate_overall(idx_emotion_B)));
    
    direction_b_rate = mean(rate_overall(idx_dir_B));
    std_err_d_b_r = std(rate_overall(idx_dir_B))/sqrt(length(rate_overall(idx_dir_B)));
    
    gender_s_rate    = mean(rate_overall(idx_gender_S));
    std_err_g_s_r = std(rate_overall(idx_gender_S))/sqrt(length(rate_overall(idx_gender_S)));
    
    emotion_s_rate   = mean(rate_overall(idx_emotion_S));
    std_err_e_s_r = std(rate_overall(idx_emotion_S))/sqrt(length(rate_overall(idx_emotion_S)));
    
    direction_s_rate = mean(rate_overall(idx_dir_S));
    std_err_d_s_r = std(rate_overall(idx_dir_S))/sqrt(length(rate_overall(idx_dir_S)));
    
    
    figure();

    errorbar([gender_b_rate ,emotion_b_rate,direction_b_rate],...
        [std_err_g_b_r,std_err_e_b_r,std_err_d_b_r],...
        'o', 'LineStyle', '-','LineWidth',2,'Color','k');
    hold on
    
    errorbar([gender_s_rate ,emotion_s_rate,direction_s_rate],...
        [std_err_g_s_r,std_err_e_s_r,std_err_d_s_r],...
        'o', 'LineStyle', '-','LineWidth',2,'Color','g');
    
    
    set(gca, 'FontSize', 15, ...
        'XTick', [1:1:3], 'XTickLabel',{'gender','emotion','direction'});
    xlim([0 4])
    % ylim([0.4 1])
    legend box off
    xlabel('condition')
    ylabel('avg microsaccade rate')
    set(gca,'fontsize',27,'FontWeight','Bold')
    legend('baseline','salient')
    
    
    figure();

    errorbar([o_rate,b_rate,s_rate],...
        [std_err_o_rate,std_err_b_rate,std_err_s_rate],...
        'o', 'LineStyle', '-','LineWidth',2,'Color','k');
    
    
    set(gca, 'FontSize', 15, ...
        'XTick', [1:1:3], 'XTickLabel',{'overall','baseline','salient'});
    xlim([0 4])
    % ylim([0.4 1])
    legend box off
    xlabel('condition')
    ylabel('avg microsaccade rate')
    set(gca,'fontsize',27,'FontWeight','Bold')
    legend off
    
end