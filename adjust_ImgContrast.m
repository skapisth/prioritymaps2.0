
dir_hisEqImgs = 'C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\priorityMaps2.0\scriptsAndData\histEqImgs\';

pathToSave = 'C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\priorityMaps2.0\scriptsAndData\';
folderName = 'contrastAdjImgs';



numImgs = length(dir(strcat(dir_hisEqImgs,'*.png')));

fullpath_ref = strcat(dir_hisEqImgs, 'ref','.jpg'); 
    
ref_Img = imread(fullpath_ref);

if ~exist(pathToSave, 'dir')
    mkdir(pathToSave)
end
if ~exist(strcat(pathToSave, folderName), 'dir')
    mkdir(strcat(pathToSave, folderName))
end

for num = 1:numImgs
    
    fullpath_img = strcat(dir_hisEqImgs, sprintf('%d',num),'.png');%strcat(dir_Imgs, sprintf('%d',num),'.jpg'); 
    
    img = imread(fullpath_img);
    
    J = imhistmatch(img, ref_Img);
    
%     figure();montage({J,img,ref_Img})
%     title('matched with ref img vs original vs ref img');
    
    
    fName = string(num);
    imwrite(J,fullfile(strcat(pathToSave, folderName,'\',fName,'.png')))
    
end