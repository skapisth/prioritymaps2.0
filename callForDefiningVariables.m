function callForDefiningVariables(condition,all_conditionLabels,delayTimeChoicesPicked,all_delayTimeChoicesPicked,timesBasedOnSaccade)
    
idx_baseline             = find(condition == all_conditionLabels(1));
idx_baseline_catch       = find(condition == all_conditionLabels(2));
idx_salient              = find(condition == all_conditionLabels(3));
idx_salient_catch        = find(condition == all_conditionLabels(4));


conditionNames = ["baseline","salient"];
for numDelayTimes = 1:length(all_delayTimeChoicesPicked)
    varNameTimings       = "trials_"  + num2str(all_delayTimeChoicesPicked(numDelayTimes)) + "_" + "ms";
    eval("trials_" + num2str(all_delayTimeChoicesPicked(numDelayTimes)) + "_" +"ms" + "=" + "[]");
    numCounts     = find(delayTimeChoicesPicked == all_delayTimeChoicesPicked(numDelayTimes));
    for all = 1:length(numCounts)
        eval(varNameTimings + "(1,end+1)" + "=" + num2str(numCounts(all)));
    end
    
    for conds = 1:length(conditionNames)
        varNameCondition1 = "idx_" + conditionNames(conds)+ "_" + "change" + "_" + num2str(all_delayTimeChoicesPicked(numDelayTimes)) + "_" + "ms";
        eval(varNameCondition1 + "=" + "[]");
        
        varNameCondition2 = "idx_" + conditionNames(conds)+ "_" + "noChange" + "_" + num2str(all_delayTimeChoicesPicked(numDelayTimes)) + "_" + "ms";
        eval(varNameCondition2 + "=" + "[]");
        
        if (conds == 1 )
            
            varNameFinal = strcat(varNameTimings,"(","ismember","(",varNameTimings,",","idx_baseline",")",")");
            eval(varNameCondition1  +  "=" + varNameFinal);
            varNameFinal = strcat(varNameTimings,"(","ismember","(",varNameTimings,",","idx_baseline_catch",")",")");
            eval(varNameCondition2  +  "=" + varNameFinal);
            
            varNameLength = strcat("delayTimesOccurence" + "=" + "length(" + varNameCondition1 + ")" );
            eval(varNameLength );
            
            
            varNameLabel = "baselineLabel_" + num2str(all_delayTimeChoicesPicked(numDelayTimes));
            eval(varNameLabel + "=" + "[]");
            eval(varNameLabel + "(1,end+1)" + "=" + num2str(0));
            
%             delayTimesOccurence_B(1,end+1) = delayTimesOccurence;
%             varNameLength = strcat("delayTimesOccurence" + "=" + "length(" + varNameCondition2 + ")" );
%             eval(varNameLength );
%             delayTimesOccurence_B(1,end+1) = delayTimesOccurence;
            
            
        elseif (conds == 2)
            
            varNameFinal = strcat(varNameTimings,"(","ismember","(",varNameTimings,",","idx_salient",")",")");
            eval(varNameCondition1  +  "=" + varNameFinal);
            varNameFinal = strcat(varNameTimings,"(","ismember","(",varNameTimings,",","idx_salient_catch",")",")");
            eval(varNameCondition2  +  "=" + varNameFinal);
            
            varNameLength = strcat("delayTimesOccurence" + "=" + "length(" + varNameCondition1 + ")" );
            eval(varNameLength );
            
            
            varNameLabel = "salientLabel_" + num2str(all_delayTimeChoicesPicked(numDelayTimes));
            eval(varNameLabel + "=" + "[]");
            eval(varNameLabel + "(1,end+1)" + "=" + num2str(0));
            
%             delayTimesOccurence_S(1,end+1) = delayTimesOccurence;
%             varNameLength = strcat("delayTimesOccurence" + "=" + "length(" + varNameCondition2 + ")" );
%             eval(varNameLength );
%             delayTimesOccurence_S(1,end+1) = delayTimesOccurence;
            
        end
    end
end

for numDelayTimes = 1:length(timesBasedOnSaccade)
    varNameTimings       = "trials_"  + num2str(timesBasedOnSaccade(numDelayTimes)) + "_" + "ms";
    eval("trials_" + num2str(timesBasedOnSaccade(numDelayTimes)) + "_" +"ms" + "=" + "[]");
    numCounts     = find(timesBasedOnSaccade == timesBasedOnSaccade(numDelayTimes));
    for all = 1:length(numCounts)
        eval(varNameTimings + "(1,end+1)" + "=" + num2str(numCounts(all)));
    end
    
    for conds = 1:length(conditionNames)
        varNameCondition1 = "idx_" + conditionNames(conds)+ "_"  + "ST_" + "change" + "_" + num2str(timesBasedOnSaccade(numDelayTimes)) + "_" + "ms";
        eval(varNameCondition1 + "=" + "[]");
        
        varNameCondition2 = "idx_" + conditionNames(conds)+ "_" + "ST_" + "noChange" + "_" + num2str(timesBasedOnSaccade(numDelayTimes)) + "_" + "ms";
        eval(varNameCondition2 + "=" + "[]");
        
        if (conds == 1 )
            
            varNameFinal = strcat(varNameTimings,"(","ismember","(",varNameTimings,",","idx_baseline",")",")");
            eval(varNameCondition1  +  "=" + varNameFinal);
            varNameFinal = strcat(varNameTimings,"(","ismember","(",varNameTimings,",","idx_baseline_catch",")",")");
            eval(varNameCondition2  +  "=" + varNameFinal);
            
            varNameLength = strcat("delayTimesOccurence" + "=" + "length(" + varNameCondition1 + ")" );
            eval(varNameLength );
            
            
            varNameLabel = "baselineLabel_" + num2str(timesBasedOnSaccade(numDelayTimes));
            eval(varNameLabel + "=" + "[]");
            eval(varNameLabel + "(1,end+1)" + "=" + num2str(0));
            
        elseif (conds == 2)
            
            varNameFinal = strcat(varNameTimings,"(","ismember","(",varNameTimings,",","idx_salient",")",")");
            eval(varNameCondition1  +  "=" + varNameFinal);
            varNameFinal = strcat(varNameTimings,"(","ismember","(",varNameTimings,",","idx_salient_catch",")",")");
            eval(varNameCondition2  +  "=" + varNameFinal);
            
            varNameLength = strcat("delayTimesOccurence" + "=" + "length(" + varNameCondition1 + ")" );
            eval(varNameLength );
            
            
            varNameLabel = "salientLabel_" + num2str(timesBasedOnSaccade(numDelayTimes));
            eval(varNameLabel + "=" + "[]");
            eval(varNameLabel + "(1,end+1)" + "=" + num2str(0));
            
%             delayTimesOccurence_S(1,end+1) = delayTimesOccurence;
%             varNameLength = strcat("delayTimesOccurence" + "=" + "length(" + varNameCondition2 + ")" );
%             eval(varNameLength );
%             delayTimesOccurence_S(1,end+1) = delayTimesOccurence;
            
        end
    end
end 
    
    
end