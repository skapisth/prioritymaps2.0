%%
% Q = 1, male = 0, female = 1
% Q = 2, away = 0, at you = 1
% Q = 3, smiling = 0, not smiling = 1


function [storeCorr_gender,storeCorr_emotion,storeCorr_dir,...
            idx_gender_B,idx_emotion_B,idx_dir_B,...
            idx_gender_S,idx_emotion_S,idx_dir_S] = processFacePerf(validTrials,...
                                                              storeCorr_gender,storeCorr_emotion,storeCorr_dir,...
                                                              idx_gender_B,idx_emotion_B,idx_dir_B,...
                                                              idx_gender_S,idx_emotion_S,idx_dir_S,cond_label)
    
    for all = 1:length(validTrials)
        
        if validTrials{all}.image_ques == 1
            val = contains(validTrials{all}.image,'m');
            if cond_label(all) == 0
                idx_gender_B(1,end+1) = all;
            else
                idx_gender_S(1,end+1) = all;
            end
            if val == 0
               val = contains(validTrials{all}.image,'f'); 
               if (validTrials{all}.ResponseFace == 1)
                   storeCorr_gender(1,end+1) = 1;
               else
                   storeCorr_gender(1,end+1) = 0;
               end

            else
                if (validTrials{all}.ResponseFace == 0)
                   storeCorr_gender(1,end+1) = 1;
               else
                   storeCorr_gender(1,end+1) = 0;
               end
               
            end
        elseif  validTrials{all}.image_ques == 2
            val = contains(validTrials{all}.image,'LA');
            if cond_label(all) == 0
                idx_dir_B(1,end+1) = all;
            else
                idx_dir_S(1,end+1) = all;
            end
            if val == 0
               if (validTrials{all}.ResponseFace == 1)
                   storeCorr_dir(1,end+1) = 1;
               else
                   storeCorr_dir(1,end+1) = 0;
               end
            else
                if (validTrials{all}.ResponseFace == 0)
                   storeCorr_dir(1,end+1) = 1;
               else
                   storeCorr_dir(1,end+1) = 0;
               end
               
            end
        elseif validTrials{all}.image_ques == 3
            val = contains(validTrials{all}.image,'Neutral');
            if cond_label(all) == 0
                idx_emotion_B(1,end+1) = all;
            else
                idx_emotion_S(1,end+1) = all;
            end
            
            if val == 0
                val = contains(validTrials{all}.image,'Smile'); 
               if (validTrials{all}.ResponseFace == 1)
                   storeCorr_emotion(1,end+1) = 1;
               else
                   storeCorr_emotion(1,end+1) = 0;
               end
            else
                if (validTrials{all}.ResponseFace == 0)
                   storeCorr_emotion(1,end+1) = 1;
               else
                   storeCorr_emotion(1,end+1) = 0;
               end
               
            end
        end
            
        
        
    end
    
    
    
end