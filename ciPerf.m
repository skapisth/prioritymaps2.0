
function ci_perf = ciPerf(data)
    
%% data = responses
n = length(data);
p = sum(data)/length(data);
ci_perf = sqrt(p.*(1 - p)./(n));
end    
