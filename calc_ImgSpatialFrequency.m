direct =  'C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\priorityMaps2.0\scriptsAndData\images\expBG_jpg\';

%% plot before and after equlaization histograms for each image in gray scale...and then have them for each channel 
%% of the color image

pixel_angle = 0.6;% arcmin, angle of each pixel in image

%% plot power spectrum for each image - sk


% numImgs = length(dir(strcat(direct,'*.png')));

val = 45;
figure()
for num = 1:9
    
    fullpath = strcat(direct, sprintf('%d',num+val),'.jpg');
    
    img = imread(fullpath);
    img = imresize(img,[1000,1000]);
    im_new = nan(size(img));
    subplot(2, 3, num);
    for each = 1:3
        
        im = img(:, :, each);

        % FFT of image and matrix inversion
        IM1 = fftshift( abs(fft2(im)) );
        
        f = RadialProfile(IM1,30,0.01);
        
        if each == 1
            f_red = f;
            plot(linspace(0, 60 / (pixel_angle * 2), 30), f, 'r'); hold on
        elseif each == 2
            f_green = f;
            plot(linspace(0, 60 / (pixel_angle * 2), 30), f, 'g'); hold on  
        elseif each == 3
            f_blue = f;
            plot(linspace(0, 60 / (pixel_angle * 2), 30), f, 'b'); hold on
        end
        if each == 3
            rgb_f = cat(2, f_red, f_green, f_blue);
            plot(linspace(0, 60 / (pixel_angle * 2), 90), rgb_f, 'k'); hold on
        end
        title(strcat('Radial profile - img', sprintf('%d',num+val)));
%         % magnitude is set to one
%         IM2 = IM1./abs(IM1);
% 
%         % builds X and Y of distance from centre
%         [X Y]=meshgrid( ...
%             [ round(size(IM2,2)/2):-1:1 ...
%             1:round(size(IM2,2)/2) ],...
%             [ round(size(IM2,1)/2):-1:1 ...
%             1:round(size(IM2,1)/2) ]);
% 
%         % rescale matrices in degrees
%         X = X.*pixel_angle;
%         X = reshape(X,size(IM2));
%         Y = Y.*pixel_angle;
% 
%         % creates radial matrix
%         R = sqrt(X.^2 + Y.^2);
% 
%         % apply 1/F.^2
%         if each == 2
%             mag = abs(IM1);
%         end
%         F = 1./R;
%         IM3 = IM2 .*F; %we can even take mag of one image and just multiply it to all - sk
%        
% 
%         % should not this be real already?
%         n = real( ifft2(ifftshift(IM3)) );
% 
%         im_new(:, :, each) = n;

    end
    
%     im_new = (im_new - min(im_new(:))) / (max(im_new(:)) - min(im_new(:)));
%     figure(); 
%     imshow(im_new);
   
   
end

