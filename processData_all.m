subjects = {'z001','z029','z072','PJ'};
% subjects = {'SK','z096','z145','z001','z029','z072'};
conversionFactor = 1000/340;
% timeSplits = {0,50,100,150,200,250,300,350,400,450};
timeSplits = {0,150,300,450};
all_PC_B = zeros(length(subjects),length(timeSplits)-1);
all_CI_B = zeros(length(subjects),length(timeSplits)-1);
all_PC_S = zeros(length(subjects),length(timeSplits)-1);
all_CI_S = zeros(length(subjects),length(timeSplits)-1);
c = jet(length(subjects)) ;

[id_baseline_ST_0_150ms,id_baseline_ST_150ms_300ms,id_baseline_ST_300ms_450ms,...
    id_salient_ST_0_150ms,id_salient_ST_150ms_300ms,id_salient_ST_300ms_450ms] = deal([]);

allResp = [];
for si = 1:length(subjects)
    [responses,condition,delayTimeChoices,delayTimeChoicesPicked,...
        delayTimesOccurence_B,delayTimesOccurence_S,reactionTimes,...
        timesBasedOnSaccade] = deal([]);
    
    [recordNoTracks, recordBlinks, recordInvalid, recordMicroSaccades,recordSaccades] = deal([]);
    [ematSL_time, delayOn] = deal([]);
    
    
    
    [validpptrialsMS,ms_trials] = deal({});
    numFaultyTrials = 0;
    num_discarded = 0;
    num_more_than_450 = 0;
    %         userVariables.allCombined_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\priorityMaps2.0\scriptsAndData\data\SK\eyeTracking\disc\noFace\',subjects{si},'\1\');
    userVariables.allCombined_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\priorityMaps2.0\scriptsAndData\data\',subjects{si},'\eyeTracking\disc\noFace\saccade','\','allCombined\');
    load([strcat(userVariables.allCombined_path,'pptrials.mat')])
    dataHolder = pptrials;
    dataHolder = convertSamplesToMs(dataHolder, conversionFactor);
    
    for all = 1:length(dataHolder)
        timeOn = dataHolder{all}.TimeFixationOff;
        timeOff = dataHolder{all}.TimeResponseOn;
        changeOff = dataHolder{all}.TimeChangeOff;
        [output, validTrials,...
            countNoTracks, countBlinks,countInvalid,...
            countMicroSaccades, countSaccades,ms_trials] = discardTrials(dataHolder, all, timeOn, timeOff,changeOff,ms_trials);
        
        recordNoTracks(all) = countNoTracks;
        recordBlinks(all) = countBlinks;
        recordInvalid(all) = countInvalid;
        recordMicroSaccades(all) = countMicroSaccades;
        recordSaccades(all) = countSaccades;
        
        dataHolder{all}.delayTimePicked = dataHolder{all}.TimeDelayOff - dataHolder{all}.TimeDelayOn;
        
        if (dataHolder{all}.delayTimePicked <=0 || dataHolder{all}.delayTimePicked >=500)
            output = 0;
            num_discarded = num_discarded + 1;
        end
        RT = dataHolder{all}.TimeResponseOff - dataHolder{all}.TimeResponseOn;
        if (RT > 700 || RT < 100)
            output = 0;
        end
        if (output == 1)
            validpptrialsMS{1,end+1} = dataHolder{all};
            responses(1,end+1)        = dataHolder{all}.Response;
            
            if (dataHolder{all}.TimeDelayOn < dataHolder{all}.TimeFixationOff)
                numFaultyTrials = numFaultyTrials+1;
            end
            ematSL_time(1,end+1) = dataHolder{all}.TimeFixationOff;
            delayOn(1,end+1) = dataHolder{all}.TimeDelayOn;
            saccToChange = dataHolder{all}.TimeDelayOff - dataHolder{all}.TimeFixationOff;
            timesBasedOnSaccade(1,end+1) = saccToChange;
            
            
            delayTimeChoices(1,end+1) = dataHolder{all}.delayTime;
            condition(1,end+1)        = dataHolder{all}.groundTruthCond_id;
            reactionTimes(1,end+1) = dataHolder{all}.TimeResponseOff - dataHolder{all}.TimeResponseOn;
        end
    end
    
    cond_label = condition;
    cond_label(cond_label == 1) = 0; %baseline
    cond_label(cond_label == 2) = 1; %salient
    cond_label(cond_label == 3) = 1; %salient
    
    for numTimes = 1:length(timeSplits)-1
        baseline_ids = find(timesBasedOnSaccade>=timeSplits{numTimes} & timesBasedOnSaccade<timeSplits{numTimes+1} & cond_label == 0);
        propCorrect_B = calcPerformanceCorrect(responses, baseline_ids);
        ci_perf_B = ciPerf(responses(baseline_ids));
        all_PC_B(si,numTimes) = propCorrect_B;
        all_CI_B(si,numTimes) = ci_perf_B;
        
        if numTimes == 1
            id_baseline_ST_0_150ms = [id_baseline_ST_0_150ms,baseline_ids];
        elseif numTimes == 2
            id_baseline_ST_150ms_300ms = [id_baseline_ST_150ms_300ms,baseline_ids];
        elseif numTimes == 3
            id_baseline_ST_300ms_450ms = [id_baseline_ST_300ms_450ms,baseline_ids];
        end
            
        salient_ids = find(timesBasedOnSaccade>=timeSplits{numTimes} & timesBasedOnSaccade<timeSplits{numTimes+1} & cond_label == 1);
        propCorrect_S = calcPerformanceCorrect(responses, salient_ids);
        ci_perf_S = ciPerf(responses(salient_ids));
        all_PC_S(si,numTimes) = propCorrect_S;
        all_CI_S(si,numTimes) = ci_perf_S;
        
        if numTimes == 1
            id_salient_ST_0_150ms = [id_salient_ST_0_150ms,salient_ids];
        elseif numTimes == 2
            id_salient_ST_150ms_300ms = [id_salient_ST_150ms_300ms,salient_ids];
        elseif numTimes == 3
            id_salient_ST_300ms_450ms = [id_salient_ST_300ms_450ms,salient_ids];
        end
        
        allResp = [allResp,responses];
        
    end
        
        
        
end
    


figure();

errorbar([mean(all_PC_B(:,1))  mean(all_PC_B(:,2)),...
    mean(all_PC_B(:,3))],...
    [ mean(all_CI_B(:,1))  mean(all_CI_B(:,2)),...
    mean(all_CI_B(:,3))],...
    'o', 'LineStyle', '-','LineWidth',2,'Color','k');
set(gca, 'FontSize', 15, ...
    'XTick', [1:1:3], 'XTickLabel',{'0-150ms','150-300ms','300-450ms'});
           
hold on
errorbar([mean(all_PC_S(:,1))  mean(all_PC_S(:,2)),...
    mean(all_PC_S(:,3))],...
    [ mean(all_CI_S(:,1))  mean(all_CI_S(:,2)),...
    mean(all_CI_S(:,3))],...
    'o', 'LineStyle', '-','LineWidth',2,'Color','g');
set(gca, 'FontSize', 15, ...
    'XTick', [1:1:3], 'XTickLabel',{'0-150ms','150-300ms','300-450ms'});
xlim([0 4])
ylim([0.5 1])
    
xlabel('Delay Times')
ylabel('Avg Proportion Correct')
set(gca,'fontsize',16)
legend('Baseline', 'Salient');
legend box off

[E1, E2, z,hh_0_150, p_0_150]  = Z_Test(length(find(allResp(id_baseline_ST_0_150ms) == 1)),length(allResp(id_baseline_ST_0_150ms)),...
                           length(find(allResp(id_salient_ST_0_150ms)==1)),length(allResp(id_salient_ST_0_150ms)),0.05,...
                            'two sided',1);
[E1, E2, z,hh_150_300, p_150_300]  = Z_Test(length(find(allResp(id_baseline_ST_150ms_300ms) == 1)),length(allResp(id_baseline_ST_150ms_300ms)),...
                           length(find(allResp(id_salient_ST_150ms_300ms)==1)),length(allResp(id_salient_ST_150ms_300ms)),0.05,...
                            'two sided',1);      
[E1, E2, z,hh_300_450, p_300_450]  = Z_Test(length(find(allResp(id_baseline_ST_300ms_450ms) == 1)),length(allResp(id_baseline_ST_300ms_450ms)),...
                           length(find(allResp(id_salient_ST_300ms_450ms)==1)),length(allResp(id_salient_ST_300ms_450ms)),0.05,...
                            'two sided',1);

 



[E1, E2, z,hh_0_150_300_b, p_0_150_300_b]  = Z_Test(length(find(allResp(id_baseline_ST_0_150ms) == 1)),length(allResp(id_baseline_ST_0_150ms)),...
                           length(find(allResp(id_baseline_ST_150ms_300ms)==1)),length(allResp(id_baseline_ST_150ms_300ms)),0.05,...
                            'two sided',1);
[E1, E2, z,hh_0_150_300_s, p_0_150_300_s]  = Z_Test(length(find(allResp(id_salient_ST_0_150ms) == 1)),length(allResp(id_salient_ST_0_150ms)),...
                           length(find(allResp(id_salient_ST_150ms_300ms)==1)),length(allResp(id_salient_ST_150ms_300ms)),0.05,...
                            'two sided',1);
[E1, E2, z,hh_150_300_450_b, p_150_300_450_b]  = Z_Test(length(find(allResp(id_baseline_ST_150ms_300ms) == 1)),length(allResp(id_baseline_ST_150ms_300ms)),...
                           length(find(allResp(id_baseline_ST_300ms_450ms) == 1)),length(allResp(id_baseline_ST_300ms_450ms)),0.05,...
                            'two sided',1);      
[E1, E2, z,hh_150_300_450_s, p_150_300_450_s]  = Z_Test(length(find(allResp(id_salient_ST_150ms_300ms)==1)),length(allResp(id_salient_ST_150ms_300ms)),...
                           length(find(allResp(id_salient_ST_300ms_450ms)==1)),length(allResp(id_salient_ST_300ms_450ms)),0.05,...
                            'two sided',1); 
[E1, E2, z,hh_0_150_300_b, p_0_150_450_b]  = Z_Test(length(find(allResp(id_baseline_ST_0_150ms) == 1)),length(allResp(id_baseline_ST_0_150ms)),...
                            length(find(allResp(id_baseline_ST_300ms_450ms) == 1)),length(allResp(id_baseline_ST_300ms_450ms)),0.05,...
                            'two sided',1);
[E1, E2, z,hh_0_150_300_s, p_0_150_450_s]  = Z_Test(length(find(allResp(id_salient_ST_0_150ms) == 1)),length(allResp(id_salient_ST_0_150ms)),...
                            length(find(allResp(id_salient_ST_300ms_450ms) == 1)),length(allResp(id_salient_ST_300ms_450ms)),0.05,...
                            'two sided',1);