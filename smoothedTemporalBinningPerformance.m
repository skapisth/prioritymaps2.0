function [TimeBins, TimeBins_std, DataPoints, ...
    n ,PC ,PC_se, PC_CI ,Dprime ,Dprime_CI, IDX,overlap, ...
    HR, FA, RespTime,RespTime_se] = SmoothedTemporalBinningPerformance(time,timeline,perf,resp,resp_time,targetO,window)


%% First Check that inputs are of equal length
if isequal(length(time),length(perf),length(resp),length(targetO))
    
    
    %% Bin time data 
    
if isnan(window)    
    % Using digit delays provided in experiment
    [trialNum, dataPoints] = select_sliding_average(NaN, timeline, time);
    overlap =0;
    
else
     % Using sliding window requires start and end
     [trialNum, dataPoints, overlap] = select_sliding_average(window, timeline, time);
   
end
    
    
    for i = 1:length(dataPoints)
        
        % Index into trial
        IDX{i} = trialNum{i};
        
        % Save average of data to create bin
        TimeBins(i) = mean(time(IDX{i}));
        TimeBins_std(i) = std(time(IDX{i}));
        
        if isnan(TimeBins(i))
            pause = 0;
        end
        
        % Save each averaged data point
        DataPoints{i} = time(IDX{i});
        n(i) = length(time(IDX{i}));
       
        
        % Percent correct
        PC(i) = sum(perf(IDX{i}))/length(IDX{i});
        p = 0.5; % probability of guessing correct/incorrect
        PC_se(i) = sqrt(p*(1-p)/(length(IDX{i})));
        PC_CI(i) = PC(i) + 1.96*PC_se(i);
        
        
        % Response Time
          RespTime(i) = mean(resp_time(IDX{i}));
          RespTime_se(i) = std(resp_time(IDX{i}))/length(IDX{i});
       
        % Sensitivity (dprime)
        [Dprime(i),~ ,Dprime_CI(i),~ ,~,HR(i), FA(i)] = CalculateDprime_2(resp(IDX{i}), targetO(IDX{i}));
        
    end

   
end
end