function  countingTrials(pptrials)
%COUNTING TRIALS Summary of this function goes here
%   Detailed explanation goes here

numTotalTrials = length(pptrials);

counter = buildCountingStruct; 
valid = buildValidStruct(pptrials);

%different counts for different events
for ii = 1:length(pptrials)
    timeOn = pptrials{ii}.TimeFixationOff;
    timeOff = pptrials{ii}.TimeResponseOn;


%     if length(pptrials{ii}.x.position) < 500 %%% TOO SHORT
%         valid.tooshort(ii) = true;
%         counter.TooShort = counter.TooShort+1;
%         continue;
%     end
    if (~isempty(pptrials{ii}.blinks.start))
        if (pptrials{ii}.blinks.start > timeOn) &  (pptrials{ii}.blinks.start<timeOff)%%%BLINKS
            valid.blink(ii) = true;
            counter.Blinks = counter.Blinks+1;
            continue;
        end
    end
    
    if (~isempty(pptrials{ii}.notracks.start))
        if (pptrials{ii}.notracks.start > timeOn) &  (pptrials{ii}.notracks.start<timeOff)%%%NO TRACK TRIALS
            valid.notrack(ii) = true;
            counter.Notracks = counter.NoTracks+1;
            continue;
        end
    end
%     if pptrials{ii}.Correct > 1 %%%NO RESPONSE
%        valid.noresponse(ii) = true;
%        counter.NoResponse = counter.NoResponse+1;
%        continue;
%     end
    if  ~isempty(pptrials{ii}.invalid.start)
        valid.manualdiscard(ii) = true;
        counter.manualDiscard = counter.manualDiscard+1;
        continue;
    end
%     if isIntersectedIn(timeOn,timeOff-timeOn,pptrials{ii}.saccades)%%%SACCADE TRIALS
%         valid.s(ii) = true;
%         counter.Saccades = counter.Saccades+1;
%         continue;
%     end
%     if pptrials{ii}.TimeFixationOFF > 0 %%%FIXATION TRIALS
%         valid.fixation(ii) = true;
%         counter.Fixation = counter.Fixation+1;
%         continue;
%     end

    if (~isempty(pptrials{ii}.microsaccades.start))
        if (pptrials{ii}.microsaccades.start > timeOn) &  (pptrials{ii}.microsaccades.start<timeOff)%%%MICROSACCADE TRIALS
            valid.ms(ii) = true;
            counter.Microsaccades = counter.Microsaccades+1;
            counter.TotalTask = counter.TotalTask + 1;
            continue;
%         else 
%             valid.d(ii) = true;
%             counter.Drifts = counter.Drifts+1;
%             counter.TotalTask = counter.TotalTask + 1;
        end
    end
    
      if (~isempty(pptrials{ii}.saccades.start))
        if (pptrials{ii}.saccades.start > timeOn) &  (pptrials{ii}.saccades.start<timeOff)%%%MICROSACCADE TRIALS
            valid.s(ii) = true;
            counter.Saccades = counter.Saccades+1;
            counter.TotalTask = counter.TotalTask + 1;
            continue;
        end
      end
    
    
    
end

fprintf('\nThere are %i in total trials prior to filter.\n\n', numTotalTrials)
% fprintf('There are %i trials that have no response.\n',counter.TooShort)
% fprintf('There are %i trials that have large offset.\n',counter.BigOffset)
fprintf('There are %i trials that have blinks.\n',counter.Blinks)
fprintf('There are %i trials do not track.\n',counter.NoTracks)
% fprintf('There are %i trials that have no response.\n',counter.NoResponse)
fprintf('There are %i trials that have saccades.\n',counter.Saccades)
% fprintf('There are %i trials that are fixation.\n',counter.Fixation)
fprintf('There are %i trials that do not fit within the span.\n',counter.Span)
fprintf('There are %i trials that have microsaccades.\n',counter.Microsaccades)
fprintf('There are %i trials that have drifts only.\n',counter.Drifts)
% fprintf('There are %i trials after filter.\n',  sum(cell2mat(struct2cell(counter))) - counter.TotalTask)

end

function [validStructure] = buildValidStruct (pptrials)
fprintf('%s: getting valid trials\n', datestr(now));
validStructure = struct(...;
    'tooshort', false(size(pptrials)),...;
    'blink', false(size(pptrials)),...;
    'notrack', false(size(pptrials)),...;
    'noresponse', false(size(pptrials)),...;
    'manualdiscard', false(size(pptrials)),...;
    's', false(size(pptrials)),...;
    'fixation', false(size(pptrials)),...;
    'span', false(size(pptrials)),...;
    'ms', false(size(pptrials)),...;
    'd', false(size(pptrials)));
end

function counter = buildCountingStruct()

counter.TooShort = 0;
counter.BigOffset = 0;
counter.Blinks = 0;
counter.NoTracks = 0;
counter.NoResponse = 0;
counter.manualDiscard = 0;
counter.Saccades = 0;
counter.Fixation = 0;
counter.Span = 0;
counter.Microsaccades = 0;
counter.Drifts = 0;
counter.TotalTask = 0; 

end
