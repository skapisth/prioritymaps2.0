close all;
clear all;
clc;
% initialize the random number generator
rng default
% the first number on each sequence in the StimList indicates the trial number
% the second number indicates whether the cue indicates left or right (-1= left 1= right)
% the third number indicats whether the cue is valid invalid or neutral (valid = 1 invalid = 0 neutral = 2 catch = 3)

% use the ratio typically used in a posner task 
% 75% of the trials are valid
% 8:2:4:3 from JongenEtAl06

BlockRepetition = 100;
% every BlockSize trials subjects will be exposed to the same number of
% cues types
BlockSize = 100; 
PValid = 45;%(8*100)/17;
PCatch = 18;%23;%(4*100)/17;
PNeutral = 18;%(3*100)/17;
PInvalid = 14;%(2*100)/17;
NumberValid = (PValid*BlockSize)/100;
NumberCatch = (PCatch*BlockSize)/100;
NumberNeutral = (PNeutral*BlockSize)/100;
NumberInvalid = (PInvalid*BlockSize)/100;

% minTime = 50;
% maxTime = 450;
trial = [50:1:450];

CueTargetTime = randperm(length(trial));


TrialType = [ones(1,NumberValid) zeros(1,NumberInvalid) ones(1,NumberNeutral)*2 ones(1,NumberCatch)*3];

C = [];
T = [];

r = randi([0 1],[BlockRepetition,1]);


for rr = 1: BlockRepetition
    if (r(rr)==1)
    Cue = [ones(1,ceil(NumberValid/2)) ones(1,floor(NumberValid/2))*-1 ...
    ones(1,NumberInvalid/2) ones(1,NumberInvalid/2)*-1 ...
    ones(1,NumberNeutral/2) ones(1,NumberNeutral/2)*-1 ...
    ones(1,floor(NumberCatch/2)) ones(1,ceil(NumberCatch/2))*-1];
    elseif (r(rr)==0)
    Cue = [ones(1,floor(NumberValid/2)) ones(1,ceil(NumberValid/2))*-1 ...
    ones(1,NumberInvalid/2) ones(1,NumberInvalid/2)*-1 ...
    ones(1,NumberNeutral/2) ones(1,NumberNeutral/2)*-1 ...
    ones(1,ceil(NumberCatch/2)) ones(1,floor(NumberCatch/2))*-1];           
    end
    [x idx] = shuffle(Cue);
    C = [C Cue(idx)];
    T = [T TrialType(idx)];
end

ct = randi([0 1],1,length(T));
% add variable cue-target interval
ct((ct==1)) = CueTargetTime(1);
ct((ct==0)) = CueTargetTime(2);


idCatch = find(T==3);
% insert catch trials with neutral cue
idCatch = shuffle(idCatch);
idx = [1:round(length(idCatch)/3)];
C(idCatch(idx)) = 3;

% write the file
fnameStim = 'C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\priorityMaps2.0\scriptsAndData\StimList.txt';
f = fopen(fnameStim, 'w');
fprintf(f,'*'); %Dummy trial number
fprintf(f, '\r\n');
for ii = 1:length(T)
    W = strrep(num2str(ii),'\','\\'); % trial number
    fprintf(f, W);
     fprintf(f, '\r\n');
    W = strrep(num2str(C(ii)),'\','\\'); % cue direction
    fprintf(f, W);
     fprintf(f, '\r\n');
    W = strrep(num2str(T(ii)),'\','\\'); % trial type
    fprintf(f, W);
    fprintf(f, '\r\n');
    W = strrep(num2str(ct(ii)),'\','\\'); % timing
    fprintf(f, W);
    fprintf(f, '\r\n');
    
    if(ii ~= length(T))
        fprintf(f,'*'); %Dummy trial number
        fprintf(f, '\r\n');
    end
end

fclose(f);

% check

% proportion of Valid
ValidP = length(find(T==1))/(length(find(T==1))+length(find(T==0)));
InvalidP =length(find(T==0))/(length(find(T==1))+length(find(T==0)));
fprintf('Proportion Valid over invalid: %.2f \n', ValidP)
fprintf('Proportion Invalid over valid: %.2f \n', InvalidP)
Valid = length(find(T==1))/length(T);
fprintf('Proportion Valid: %.2f \n', Valid)
Invalid = length(find(T==0))/length(T);
fprintf('Proportion Invalid: %.2f \n', Invalid)
Catch = length(find(T==3))/length(T);
fprintf('Proportion Catch: %.2f \n', Catch)
Neutral = length(find(T==2))/length(T);
fprintf('Proportion Neutral: %.2f \n', Neutral)

% timing 
Timing1v = length(find(T==1 & ct== CueTargetTime(1)))/(length(find(T==1)));
Timing2v = length(find(T==1 & ct==CueTargetTime(2)))/(length(find(T==1)));

Timing1i = length(find(T==0 & ct== CueTargetTime(1)))/(length(find(T==0)));
Timing2i = length(find(T==0 & ct== CueTargetTime(2)))/(length(find(T==0)));

Timing1n = length(find(T==2 & ct== CueTargetTime(1)))/(length(find(T==2)));
Timing2n = length(find(T==2 & ct==CueTargetTime(2)))/(length(find(T==2)));

fprintf('--------------------------\n')
ValidLeft = length(find(T==1 & C==-1))/length(find(T==1));
fprintf('Proportion Valid left cue: %.2f \n', ValidLeft)
InvalidLeft = length(find(T==0 & C==-1))/length(find(T==0));
fprintf('Proportion Invalid left cue: %.2f \n', InvalidLeft)
NeutralLeft = length(find(T==2 & C==-1))/ length(find(T==2));
fprintf('Proportion Neutral left cue: %.2f \n', NeutralLeft)
CatchLeft = length(find(T==3 & C==-1))/length(find(T==3));
fprintf('Proportion Catch left cue: %.3f \n', CatchLeft)
CatchRight = length(find(T==3 & C==1))/length(find(T==3));
fprintf('Proportion Catch right cue: %.3f \n', CatchRight)
CatchNeutral = length(find(T==3 & C==3))/length(find(T==3));
fprintf('Proportion Catch neutral cue: %.3f \n', CatchNeutral)

fprintf('timing valid T1: %.3f T2: %.3f \n', Timing1v, Timing2v)
fprintf('timing invalid T1: %.3f T2: %.3f \n', Timing1i, Timing2i)
fprintf('timing neutral T1: %.3f T2: %.3f \n', Timing1n, Timing2n)