all_RTs_0_150ms_B  = reactionTimes(id_baseline_ST_0_150ms);


all_RTs_150_300ms_B = reactionTimes(id_baseline_ST_150ms_300ms);


all_RTs_300_450ms_B = reactionTimes(id_baseline_ST_300ms_450ms);

C    = {};
C(1:length(all_RTs_0_150ms_B),1) = cellstr("0-150");
C(length(all_RTs_0_150ms_B)+1:length(all_RTs_0_150ms_B)+length(all_RTs_150_300ms_B),1) = cellstr("150-300");
C(length(all_RTs_0_150ms_B)+length(all_RTs_150_300ms_B)+1:length(all_RTs_0_150ms_B)+length(all_RTs_150_300ms_B)+length(all_RTs_300_450ms_B),1) = cellstr("300-450");
all_RTs = [];

for rt_b1 = 1:length(all_RTs_0_150ms_B)
    all_RTs(end+1,1) = all_RTs_0_150ms_B(rt_b1);
end
for rt_b2 = 1:length(all_RTs_150_300ms_B)
    all_RTs(end+1,1) = all_RTs_150_300ms_B(rt_b2);
end
for rt_b3 = 1:length(all_RTs_300_450ms_B)
    all_RTs(end+1,1) = all_RTs_300_450ms_B(rt_b3);
end

figure
vs = violinplot(all_RTs, C,'ViolinAlpha',0.05);
vs(1).ViolinColor = 'k';
vs(2).ViolinColor = 'k';
vs(3).ViolinColor = 'k';

hold on



all_RTs_0_150ms_S  = reactionTimes(id_salient_ST_0_150ms);


all_RTs_150_300ms_S = reactionTimes(id_salient_ST_150ms_300ms);


all_RTs_300_450ms_S = reactionTimes(id_salient_ST_300ms_450ms);

C_S    = {};
C_S(1:length(all_RTs_0_150ms_S),1) = cellstr("0-150");
C_S(length(all_RTs_0_150ms_S)+1:length(all_RTs_0_150ms_S)+length(all_RTs_150_300ms_S),1) = cellstr("150-300");
C_S(length(all_RTs_0_150ms_S)+length(all_RTs_150_300ms_S)+1:length(all_RTs_0_150ms_S)+length(all_RTs_150_300ms_S)+length(all_RTs_300_450ms_S),1) = cellstr("300-450");
all_RTs_S = [];

for rt_b1_S = 1:length(all_RTs_0_150ms_S)
    all_RTs_S(end+1,1) = all_RTs_0_150ms_S(rt_b1_S);
end
for rt_b2_S = 1:length(all_RTs_150_300ms_S)
    all_RTs_S(end+1,1) = all_RTs_150_300ms_S(rt_b2_S);
end
for rt_b3_S = 1:length(all_RTs_300_450ms_S)
    all_RTs_S(end+1,1) = all_RTs_300_450ms_S(rt_b3_S);
end


vs = violinplot(all_RTs_S, C_S,'ViolinAlpha',0.08);
vs(1).ViolinColor = 'g';
vs(1).EdgeColor = 'g';
vs(2).ViolinColor = 'g';
vs(2).EdgeColor = 'g';
vs(3).ViolinColor = 'g';
vs(3).EdgeColor = 'g';

ylabel('Reaction Times (ms)');
xlabel('Delay Times (ms)');