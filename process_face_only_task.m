clc;
close all
clear all;
eyeTrackingData = 1;

processAndPlot = 1;
plotProb_occ = 1;
processFaceResp = 1;
analyze_ms = 1;
respMatch = 0;

userFlags = struct(...
     'saveFileFlag', '0',...
     'combinePPTrials','0',...
      'manMark','0');

subject = 'SK';

date = '01302022';
session = '1';
combined = '0';
conversionFactor = 1000/340;

if eyeTrackingData
    userVariables = struct(...
    'folderName','1',...
    'pathToFile', strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\priorityMaps2.0\scriptsAndData\data\',subject,'\','eyeTracking','\disc\onlyFace\saccade','\'),...
    'pathToSave',  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\priorityMaps2.0\scriptsAndData\data\',subject,'\','eyeTracking','\disc\noFace','\','\processedMats\'),...
    'pathToSaveTaggedData',  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\priorityMaps2.0\scriptsAndData\data\',subject,'\','eyeTracking','\','\taggedData\'),...
    'allCombined_path', strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\priorityMaps2.0\scriptsAndData\data\',subject,'\','eyeTracking','\disc\noFace','\','allCombined\'));
   
else
    userVariables = struct(...
        'folderName','1',...
        'pathToFile', strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\priorityMaps2.0\scriptsAndData\data\',subject,'\'),...
        'pathToSave',  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\priorityMaps2.0\scriptsAndData\data\',subject,'\processedMats\'),...
        'allCombined_path', strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\priorityMaps2.0\scriptsAndData\data\',subject,'\','allCombined\'));
end

fullpath = strcat(userVariables.pathToFile,date,'\');

S = dir(fullpath);
numSessions = sum([S(~ismember({S.name},{'.','..'})).isdir]);

if (userFlags.saveFileFlag == '1' || userFlags.combinePPTrials == '1')
    for eachSess = 1%:length(numSessions)
        userVariables.folderName = 3;
        sessFolder = strcat(fullpath,string(eachSess),'\');
        load([strcat(sessFolder,'pptrials.mat')])
        if eyeTrackingData
            dataHolder = pptrials;
            pptrials = callToSaveCombineAndManuallyMark(userVariables, userFlags,dataHolder);
        else
            dataHolder = pptrials.user;
            pptrials = callToSaveCombineAndManuallyMark(userVariables, userFlags,dataHolder');
        end
        
    end
end

 if combined == '1'
        load([strcat(userVariables.allCombined_path,'pptrials.mat')])
        dataHolder = pptrials;
        dataHolder = convertSamplesToMs(dataHolder, conversionFactor);
 else
     load([strcat(userVariables.pathToFile,date,'\',session,'\','pptrials.mat')])
     if eyeTrackingData
         dataHolder = pptrials;
         dataHolder = convertSamplesToMs(dataHolder, conversionFactor);
     else
         dataHolder = pptrials.user;
     end
 end


if (userFlags.manMark == '1')
    pptrials = callToSaveCombineAndManuallyMark(userVariables, userFlags,dataHolder);
end
[responses,condition,delayTimeChoices,delayTimeChoicesPicked...
        delayTimesOccurence_B,delayTimesOccurence_S,reactionTimes,...
        timesBasedOnSaccade] = deal([]);
    
[recordNoTracks, recordBlinks, recordInvalid, recordMicroSaccades,recordSaccades] = deal([]);
[ematSL_time, delayOn] = deal([]);

[condition_for_prob, delayTime_for_prob] = deal([]); 

[validpptrialsMS,ms_trials] = deal({});
numFaultyTrials = 0;
num_discarded = 0;
num_more_than_450 = 0;
if processAndPlot
    %%discard trials below 0 and above 500 and keep track of how many
    %%trials
%     countingTrials(dataHolder);
    for all = 1:length(dataHolder)
         timeOn = dataHolder{all}.TimeStimulusOn;
         timeOff = dataHolder{all}.TimeStimulusOff;
         changeOff = dataHolder{all}.TimeChangeOff;
         [output, validTrials,...
          countNoTracks, countBlinks,countInvalid,...
          countMicroSaccades, countSaccades,ms_trials] = discardTrials(dataHolder, all, timeOn, timeOff,changeOff,ms_trials);
      
      
        if (output == 1)
            validpptrialsMS{1,end+1} = dataHolder{all};
        end
    end
end

[storeCorr_gender,storeCorr_emotion,storeCorr_dir,...
           idx_gender,idx_emotion,idx_dir] = deal([]);
[storeCorr_gender,storeCorr_emotion,storeCorr_dir,...
            idx_gender,idx_emotion,idx_dir] = processFacePerf(validpptrialsMS,...
                                                              storeCorr_gender,storeCorr_emotion,storeCorr_dir,...
                                                              idx_gender,idx_emotion,idx_dir);

allStoreCorr = [storeCorr_gender,storeCorr_emotion,storeCorr_dir];
                                                           
propC_gender = calcPerformanceCorrect(allStoreCorr,idx_gender);
ci_perf_gender = ciPerf(allStoreCorr(idx_gender));

propC_emotion = calcPerformanceCorrect(allStoreCorr,idx_emotion);
ci_perf_emotion = ciPerf(allStoreCorr(idx_emotion));

propC_dir = calcPerformanceCorrect(allStoreCorr,idx_dir);
ci_perf_dir = ciPerf(allStoreCorr(idx_dir));

figure();

errorbar([propC_gender propC_dir propC_emotion],...
    [ci_perf_gender ci_perf_dir ci_perf_emotion],...
    'o', 'LineStyle', '-','LineWidth',2,'Color','m');

set(gca, 'FontSize', 15, ...
    'XTick', [1:1:3], 'XTickLabel',{'gender','direction','emotion'});
xlim([0 4])
% ylim([0.4 1])
xlabel('Face Question')
ylabel('Proportion Correct for faces')
set(gca,'fontsize',27,'FontWeight','Bold')
legend('Baseline', 'Salient');
legend off

                                                          
                                                          
function [storeCorr_gender,storeCorr_emotion,storeCorr_dir,...
            idx_gender,idx_emotion,idx_dir] = processFacePerf(validTrials,...
                                                              storeCorr_gender,storeCorr_emotion,storeCorr_dir,...
                                                              idx_gender,idx_emotion,idx_dir)
    
    for all = 1:length(validTrials)
        
        if validTrials{all}.image_ques == 1
            val = contains(validTrials{all}.image,'m');
            idx_gender(1,end+1) = all;
            if val == 0
               val = contains(validTrials{all}.image,'f'); 
               if (validTrials{all}.ResponseFace == 1)
                   storeCorr_gender(1,end+1) = 1;
               else
                   storeCorr_gender(1,end+1) = 0;
               end

            else
                if (validTrials{all}.ResponseFace == 0)
                   storeCorr_gender(1,end+1) = 1;
               else
                   storeCorr_gender(1,end+1) = 0;
               end
               
            end
        elseif  validTrials{all}.image_ques == 2
            val = contains(validTrials{all}.image,'LA');
            idx_dir(1,end+1) = all;
            if val == 0
               if (validTrials{all}.ResponseFace == 1)
                   storeCorr_dir(1,end+1) = 1;
               else
                   storeCorr_dir(1,end+1) = 0;
               end
            else
                if (validTrials{all}.ResponseFace == 0)
                   storeCorr_dir(1,end+1) = 1;
               else
                   storeCorr_dir(1,end+1) = 0;
               end
               
            end
        elseif validTrials{all}.image_ques == 3
            val = contains(validTrials{all}.image,'Neutral');
            idx_emotion(1,end+1) = all;
            if val == 0
                val = contains(validTrials{all}.image,'Smile'); 
               if (validTrials{all}.ResponseFace == 1)
                   storeCorr_emotion(1,end+1) = 1;
               else
                   storeCorr_emotion(1,end+1) = 0;
               end
            else
                if (validTrials{all}.ResponseFace == 0)
                   storeCorr_emotion(1,end+1) = 1;
               else
                   storeCorr_emotion(1,end+1) = 0;
               end
               
            end
        end
            
        
        
    end
    
    
    
end