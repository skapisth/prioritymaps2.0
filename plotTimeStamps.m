directory = 'C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\priorityMaps2.0\scriptsAndData\data\SK\eyeTracking\disc\faceQ\';
folder = 'saccade\11022021\1\';

fullpath = strcat(directory,folder);

% load([fullpath sprintf('ms_data.mat')]);
load([strcat(fullpath,'pptrials.mat')])

data.user = pptrials;
% data.user = ms_data;
for all = 1:length(data.user)

    tagTimeStamps(data.user{all})
    
end


function tagTimeStamps(trial)
    
    
    x_offset = trial.xoffset * trial.pixelAngle;
    y_offset = trial.yoffset * trial.pixelAngle;
    x_pos_arcmin = trial.x.position;
    y_pos_arcmin = trial.y.position;
    x_trace = x_pos_arcmin(:) + x_offset; %x trace with offset in arcmin
    y_trace = y_pos_arcmin(:) + y_offset; %y trace with offset in arcmin
    
    tm = (1:(1000/330):length(x_trace)*1000/330); % (ms/sample) for DDPI

    a = plot(tm,x_trace,'r'); hold on; % in arcmin 
    b = plot(tm,y_trace,'b'); hold on;% in arcmin
    
    fixOn = plot([(trial.TimeFixationOn),(trial.TimeFixationOn)],ylim, 'k'); hold on;
    fixOff = plot([(trial.TimeFixationOff),(trial.TimeFixationOff)],ylim, '-k'); hold on;
    
    stimOn = plot([(trial.TimeStimulusOn),(trial.TimeStimulusOn)],ylim, 'r'); hold on;
    stimOff = plot([(trial.TimeStimulusOff),(trial.TimeStimulusOff)],ylim, '-r'); hold on;
    
    delayOn = plot([(trial.TimeDelayOn),(trial.TimeDelayOn)],ylim, 'm'); hold on;
    delayOff = plot([(trial.TimeDelayOff),(trial.TimeDelayOff)],ylim, '-m'); hold on;

    
    changeOn = plot([(trial.TimeChangeOn),(trial.TimeChangeOn)],ylim, 'b'); hold on;
    changeOff = plot([(trial.TimeChangeOff),(trial.TimeChangeOff)], ylim, '-b'); hold on;
    
    respOn = plot([(trial.TimeResponseOn),(trial.TimeResponseOn)],ylim, 'g'); hold on;
    respOff = plot([(trial.TimeResponseOff),(trial.TimeResponseOff)],ylim, '-g'); hold on;
    
    vals = [a, b, fixOn, fixOff, stimOn, stimOff,delayOn,delayOff, changeOn, changeOff, respOn, respOff];
%     'Delay On', 'Delay Off',
    legend(vals,{'xTrace', 'yTrace','Fixation On', 'Fixation Off', 'Stimulus On', 'Stimulus Off','DelayOn', 'Delay Off', 'Change On', 'Change Off', 'Response On', 'Response Off'});
%     xlim([0 1500]);
%     ylim([-30 30]);

    title(sprintf("Trial #: %i", trial.trialid));
    xlabel('Time (ms)')
    ylabel('Position in arc mins')
    input ''
    clf
    
end