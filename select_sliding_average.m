function [trialNum, dataPoints, overlap] = select_sliding_average(window, timeline, values)
    count.bins = 0;
    count.overlap =0;
    numtrials_thres = 3;
    
if isnan(window)
    for i = 1:length(timeline)-1
         
         if i < length(timeline)
             lower = timeline(i);
             upper = timeline(i + 1);
             count.bins = count.bins + 1;
         else
             lower = timeline(i);
             upper = lower + (max(values) - lower);
             count.bins = count.bins + 1;
         end
         
         trialIdx = find(values >= lower & values < upper);         
         
         if length(trialIdx) > numtrials_thres
             select_bins = values(trialIdx);
             dataPoints{count.bins} = select_bins;
             trialNum{count.bins} = trialIdx;  
         
            overlap(count.bins) = 0;
         else
             select_bins = [];
             dataPoints{count.bins} = select_bins;
             trialNum{count.bins} = [];  
         
            overlap(count.bins) = 0;
         end
     end
else    
    for i = 1:length(timeline)
        lower = timeline(i);
        upper = lower + window;
        count.bins = count.bins + 1;
        
        trialIdx = find(values >= lower & values < upper);
       
        if length(trialIdx) > numtrials_thres
        select_bins = values(trialIdx);
        dataPoints{count.bins} = select_bins;
        trialNum{count.bins} = trialIdx;
        else
         select_bins = [];
         dataPoints{count.bins} = select_bins;
         trialNum{count.bins} = [];  
         
         overlap(count.bins) = 0;
       end
        
        if i < length(timeline)
            overlap(count.bins) = window - abs((timeline(i)-timeline(i+1)));
        elseif i == length(timeline)
           overlap(count.bins) = 0;
        end
    
    end
end
end